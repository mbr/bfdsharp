# README #

BfdSharp is a .NET API for the [BFD library](https://en.wikipedia.org/wiki/Binary_File_Descriptor_library) (Binary File Descriptor) - part of the binutils software package. It provides a high-level, object-oriented interface to the underlying library. The DLL included in the project is built for x86 and targets mips-elf.

This project is likely not to encompass anything other than reading mips-elf object files/archives.

Example program reading a .a file from the N64 SDK (showing archive contents, symbols, and relocation data):

![bfd.png](https://bitbucket.org/repo/ynoGqB/images/1204315145-bfd.png)
