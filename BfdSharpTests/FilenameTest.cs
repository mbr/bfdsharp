﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BfdSharp.Backend;

namespace BfdSharpTests
{
    /// <summary>
    /// Checks that the filename stored internally in libbfd's context matches
    /// what is passed to bfd_openr() 
    /// </summary>
    [TestClass]
    public class FilenameTest
    {
        [TestMethod]
        public void CheckInternalFilename()
        {
            var path = Environment.CurrentDirectory + @"\assets\libc.a";
            var abfd = BfdBackend.OpenPath(path, true);

            Assert.AreEqual(path, abfd.Filename);
        }
    }
}
