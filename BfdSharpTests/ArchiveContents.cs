﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BfdSharp.Backend;


namespace BfdSharpTests
{
    [TestClass]
    public class ArchiveContents
    {
        [TestMethod]
        public void EnumerateArchiveContents()
        {
            //408, wsetup.o


            var path = Environment.CurrentDirectory + @"\assets\libc.a";
            var abfd = BfdBackend.OpenPath(path, true);

            Assert.AreEqual(
                abfd.ArchiveContents.Count(), 408
            );

            Assert.AreEqual(
                abfd.ArchiveContents.Last().Filename,
                "wsetup.o"
            );
        }
    }
}
