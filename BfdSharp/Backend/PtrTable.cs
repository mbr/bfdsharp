﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BfdSharp.Backend
{
    public class PtrTable<T>
        : IEnumerable<T>
    {
        internal T[]      _objects;
        internal IntPtr[] _pointers;
        internal byte[]   _raw_table;


        public T this[int i]
        {
            get
            {
                return _objects[i];
            }
        }


        public byte[] Raw
        {
            get
            {
                return _raw_table;
            }
        }

        #region IEnumerable implementation

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var obj in _objects)
                yield return obj;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var obj in _objects)
                yield return obj;
        }

        #endregion



        public PtrTable(byte[] data, int count, Func<IntPtr, T> creator)
        {
            _objects   = new T[count];
            _pointers  = new IntPtr[count];
            _raw_table = data;

            for( int i = 0; i < count; i++ )
            {
                _pointers[i] = new IntPtr(
                    BitConverter.ToInt32(data, i * 4)
                );

                _objects[i] = creator(_pointers[i]);
            }
        }
    }
}
