﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BfdSharp.Backend
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 4)]
    public struct bfd
    {
        [MarshalAs(UnmanagedType.I4)]
        public Int32 id;

        public IntPtr filename;


        /* A pointer to the target jump table.  
        const struct bfd_target *xvec;*/
        public IntPtr xvec;

        /* The IOSTREAM, and corresponding IO vector that provide access
            to the file backing the BFD.  
        void *iostream;
        const struct bfd_iovec *iovec;*/
        public IntPtr iostream;
        public IntPtr iovec;

        /* The caching routines use these to maintain a
            least-recently-used list of BFDs.  
        struct bfd *lru_prev, *lru_next;*/
        public IntPtr lru_prev;
        public IntPtr lru_next;

        /* When a file is closed by the caching routines, BFD retains
            state information on the file here...  
        ufile_ptr where*/
        public UInt64 where;

        /* File modified time, if mtime_set is TRUE.  
        long mtime;*/
        public Int32 mtime;

        /* Reserved for an unimplemented file locking extension.  
        int ifd;*/
        public Int32 ifd;

        /* The format which belongs to the BFD. (object, core, etc.)  
        bfd_format format;*/
        [MarshalAs(UnmanagedType.I4)]
        public bfd_format format;

        /* The direction with which the BFD was opened.  
        enum bfd_direction direction;*/
        [MarshalAs(UnmanagedType.I4)]
        public bfd_direction direction;

        /* Format_specific flags.  
        flagword flags;*/
        [MarshalAs(UnmanagedType.I4)]
        public bfd_flags flags;


        /* Currently my_archive is tested before adding origin to
           anything. I believe that this can become always an add of
           origin, with origin set to 0 for non archive files.  
        ufile_ptr origin;*/
        [MarshalAs(UnmanagedType.U8)]
        public UInt64 origin;

        /* The origin in the archive of the proxy entry.  This will
           normally be the same as origin, except for thin archives,
           when it will contain the current offset of the proxy in the
           thin archive rather than the offset of the bfd in its actual
           container.  
        ufile_ptr proxy_origin;*/
        [MarshalAs(UnmanagedType.U8)]
        public UInt64 proxy_origin;

        UInt64 padding;

        /*[MarshalAs(UnmanagedType.Struct)]*/
        public bfd_hash_table section_htab;
        //IntPtr section_htab;
        //#warning fix this loc

        /* Pointer to linked list of sections.  */
        //struct bfd_section *sections;
        //[FieldOffset(108)]
        public IntPtr sections;

        public IEnumerable<bfd_section> Sections
        {
            get
            {
                var sec = (bfd_section)Marshal.PtrToStructure(this.sections, typeof(bfd_section));

                do
                {
                    yield return sec;

                    if (sec.next == (IntPtr)0)
                        break;

                    sec = (bfd_section)Marshal.PtrToStructure(sec.next, typeof(bfd_section));
                }
                while (true);
            }
        }

        /* The last section on the section list.  */
        //struct bfd_section *section_last;
        IntPtr section_last;

        public bfd_section SectionLast
        {
            get
            {
                return (bfd_section)Marshal.PtrToStructure(this.section_last, typeof(bfd_section));
            }
        }

        /* The number of sections.  */
        //unsigned int section_count;
        UInt32 section_count;

        /* Stuff only useful for object files:
            The start address.  */
        //bfd_vma start_address;
        UInt64 start_address;

        /* Used for input and output.  */
        //unsigned int symcount;
        UInt32 symcount;

        /* Symbol table for output BFD (with symcount entries).
            Also used by the linker to cache input BFD symbols.  */
        //struct bfd_symbol  **outsymbols;
        IntPtr outsymbols;

        /* Used for slurped dynamic symbol tables.  */
        //unsigned int dynsymcount;
        UInt32 dynsymcount;

        /* Pointer to structure which contains architecture information.  */
        //const struct bfd_arch_info *arch_info;
        IntPtr arch_info;

        /* Stuff only useful for archives.  */
        //void *arelt_data;
        IntPtr arelt_data;
        //struct bfd *my_archive;      /* The containing archive BFD.  */
        IntPtr my_archive;
        //struct bfd *archive_next;    /* The next BFD in the archive.  */
        IntPtr archive_next;
        //struct bfd *archive_head;    /* The first BFD in the archive.  */
        IntPtr archive_head;
        //struct bfd *nested_archives; /* List of nested archive in a flattened thin archive.  */
        IntPtr nested_archives;

        /* A chain of BFD structures involved in a link.  */
        //struct bfd *link_next;
        IntPtr link_next;

        /* A field used by _bfd_generic_link_add_archive_symbols.  This will
            be used only for archive elements.  */
        Int32 archive_pass;

        /* Used by the back end to hold private data.  
        union
        {
            struct aout_data_struct *aout_data;
            struct artdata *aout_ar_data;
            struct _oasys_data *oasys_obj_data;
            struct _oasys_ar_data *oasys_ar_data;
            struct coff_tdata *coff_obj_data;
            struct pe_tdata *pe_obj_data;
            struct xcoff_tdata *xcoff_obj_data;
            struct ecoff_tdata *ecoff_obj_data;
            struct ieee_data_struct *ieee_data;
            struct ieee_ar_data_struct *ieee_ar_data;
            struct srec_data_struct *srec_data;
            struct verilog_data_struct *verilog_data;
            struct ihex_data_struct *ihex_data;
            struct tekhex_data_struct *tekhex_data;
            struct elf_obj_tdata *elf_obj_data;
            struct nlm_obj_tdata *nlm_obj_data;
            struct bout_data_struct *bout_data;
            struct mmo_data_struct *mmo_data;
            struct sun_core_struct *sun_core_data;
            struct sco5_core_struct *sco5_core_data;
            struct trad_core_struct *trad_core_data;
            struct som_data_struct *som_data;
            struct hpux_core_struct *hpux_core_data;
            struct hppabsd_core_struct *hppabsd_core_data;
            struct sgi_core_struct *sgi_core_data;
            struct lynx_core_struct *lynx_core_data;
            struct osf_core_struct *osf_core_data;
            struct cisco_core_struct *cisco_core_data;
            struct versados_data_struct *versados_data;
            struct netbsd_core_struct *netbsd_core_data;
            struct mach_o_data_struct *mach_o_data;
            struct mach_o_fat_data_struct *mach_o_fat_data;
            struct plugin_data_struct *plugin_data;
            struct bfd_pef_data_struct *pef_data;
            struct bfd_pef_xlib_data_struct *pef_xlib_data;
            struct bfd_sym_data_struct *sym_data;
            void *any;
        }
        tdata;*/
        IntPtr tdata;

        /* Used by the application to hold private data.  */
        //void *usrdata;
        IntPtr usrdata;

        /* Where all the allocated stuff under this BFD goes.  This is a
            struct objalloc *, but we use void * to avoid requiring the inclusion
            of objalloc.h.  */
        //void *memory;
        IntPtr memory;

#if false
        /* Is the file descriptor being cached?  That is, can it be closed as
            needed, and re-opened when accessed later?  */
        unsigned int cacheable : 1;

        /* Marks whether there was a default target specified when the
            BFD was opened. This is used to select which matching algorithm
            to use to choose the back end.  */
        unsigned int target_defaulted : 1;

        /* ... and here: (``once'' means at least once).  */
        unsigned int opened_once : 1;

        /* Set if we have a locally maintained mtime value, rather than
            getting it from the file each time.  */
        unsigned int mtime_set : 1;

        /* Flag set if symbols from this BFD should not be exported.  */
        unsigned int no_export : 1;

        /* Remember when output has begun, to stop strange things
            from happening.  */
        unsigned int output_has_begun : 1;

        /* Have archive map.  */
        unsigned int has_armap : 1;

        /* Set if this is a thin archive.  */
        unsigned int is_thin_archive : 1;

        /* Set if only required symbols should be added in the link hash table for
            this object.  Used by VMS linkers.  */
        unsigned int selective_search : 1;
#endif

        byte bitflags_a;
        byte bitflags_b;


        public int cacheable { get { return (bitflags_a >> 0) & 1; } }
        public int target_defaulted { get { return (bitflags_a >> 1) & 1; } }
        public int opened_once { get { return (bitflags_a >> 2) & 1; } }
        public int mtime_set { get { return (bitflags_a >> 3) & 1; } }
        public int no_export { get { return (bitflags_a >> 4) & 1; } }
        public int output_has_begun { get { return (bitflags_a >> 5) & 1; } }
        public int has_armap { get { return (bitflags_a >> 6) & 1; } }
        public int is_thin_archive { get { return (bitflags_a >> 7) & 1; } }
        public int selective_search { get { return (bitflags_b >> 0) & 1; } }
    }
}
