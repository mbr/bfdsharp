﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BfdSharp.Backend
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 4)]
    public struct bfd_reloc_howto_struct
    {
        /// <summary>
        /// The type field has mainly a documentary use - the back end can
        /// do what it wants with it, though normally the back end's
        /// external idea of what a reloc number is stored
        /// in this field.  For example, a PC relative word relocation
        /// in a coff environment has the type 023 - because that's
        /// what the outside world calls a R_PCRWORD reloc.
        /// </summary>
        public UInt32 type;

        /// <summary>
        /// The value the final relocation is shifted right by.  This drops
        /// unwanted data from the relocation.
        /// </summary>
        public UInt32 rightshift;

        /// <summary>
        /// The size of the item to be relocated.  This is *not* a
        /// power-of-two measure.  To get the number of bytes operated
        /// on by a type of relocation, use bfd_get_reloc_size.
        /// </summary>
        public Int32 size;

        /// <summary>
        /// The number of bits in the item to be relocated.  This is used
        /// when doing overflow checking.
        /// </summary>
        public UInt32 bitsize;

        /// <summary>
        /// The relocation is relative to the field being relocated.
        /// </summary>
        public bool pc_relative;

        /// <summary>
        /// The bit position of the reloc value in the destination.
        /// The relocated value is left shifted by this amount.
        /// </summary>
        public UInt32 bitpos;

        /// <summary>
        /// What type of overflow error should be checked for when
        /// relocating.
        /// </summary>
        public bfd_complain_overflow complain_on_overflow;

        /// <summary>
        /// If this field is non null, then the supplied function is
        /// called rather than the normal function.  This allows really
        /// strange relocation methods to be accommodated (e.g., i960 callj
        /// instructions).
        ///   bfd_reloc_status_type (*special_function)
        /// (bfd *, arelent *, struct bfd_symbol *, void *, asection *,
        ///  bfd *, char **);
        /// </summary>
        public IntPtr special_function;

        /// <summary>
        /// The textual name of the relocation type.
        /// </summary>
        public IntPtr name;

        /// <summary>
        /// Some formats record a relocation addend in the section contents
        /// rather than with the relocation.  For ELF formats this is the
        /// distinction between USE_REL and USE_RELA (though the code checks
        /// for USE_REL == 1/0).  The value of this field is TRUE if the
        /// addend is recorded with the section contents; when performing a
        /// partial link (ld -r) the section contents (the data) will be
        /// modified.  The value of this field is FALSE if addends are
        /// recorded with the relocation (in arelent.addend); when performing
        /// a partial link the relocation will be modified.
        /// All relocations for all ELF USE_RELA targets should set this field
        /// to FALSE (values of TRUE should be looked on with suspicion).
        /// However, the converse is not true: not all relocations of all ELF
        /// USE_REL targets set this field to TRUE.  Why this is so is peculiar
        /// to each particular target.  For relocs that aren't used in partial
        /// links (e.g. GOT stuff) it doesn't matter what this is set to.
        /// </summary>
        public bool partial_inplace;

        /// <summary>
        /// src_mask selects the part of the instruction (or data) to be used
        /// in the relocation sum.  If the target relocations don't have an
        /// addend in the reloc, eg. ELF USE_REL, src_mask will normally equal
        /// dst_mask to extract the addend from the section contents.  If
        /// relocations do have an addend in the reloc, eg. ELF USE_RELA, this
        /// field should be zero.  Non-zero values for ELF USE_RELA targets are
        /// bogus as in those cases the value in the dst_mask part of the
        /// section contents should be treated as garbage.
        /// </summary>
        public UInt64 src_mask;

        /// <summary>
        /// dst_mask selects which parts of the instruction (or data) are
        /// replaced with a relocated value.
        /// </summary>
        public UInt64 dst_mask;

        /// <summary>
        /// When some formats create PC relative instructions, they leave
        /// the value of the pc of the place being relocated in the offset
        /// slot of the instruction, so that a PC relative relocation can
        /// be made just by adding in an ordinary offset (e.g., sun3 a.out).
        /// Some formats leave the displacement part of an instruction
        /// empty (e.g., m88k bcs); this flag signals the fact.
        /// </summary>
        public bool pcrel_offset;


        public override string ToString()
        {
            return Marshal.PtrToStringAnsi(
                this.name
            );
        }
    }
}
