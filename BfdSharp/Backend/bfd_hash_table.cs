﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BfdSharp.Backend
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 4)]
    public struct bfd_hash_table
    {
        /*struct bfd_hash_entry **table;*/
        public IntPtr table;

        //[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        //public delegate IntPtr BfdHashAddElement(IntPtr entry, IntPtr table, string name);

        //[MarshalAs(UnmanagedType.FunctionPtr)]
        //public BfdHashAddElement newfunc;
        IntPtr newfunc;

        public IntPtr memory;

        [MarshalAs(UnmanagedType.U4)]
        public UInt32 size;

        [MarshalAs(UnmanagedType.U4)]
        public UInt32 count;

        [MarshalAs(UnmanagedType.U4)]
        public UInt32 entsize;

        [MarshalAs(UnmanagedType.U1)]
        public byte frozen;

    }
}
