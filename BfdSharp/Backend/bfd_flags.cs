﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BfdSharp.Backend
{
    [Flags]
    public enum bfd_flags
    {
        HAS_RELOC = 0x01,

        EXEC_P = 0x02,

        /* BFD has line number information (basically used for F_LNNO in a
           COFF header).  */
        HAS_LINENO = 0x04,

        /* BFD has debugging information.  */
        HAS_DEBUG = 0x08,

        /* BFD has symbols.  */
        HAS_SYMS = 0x10,

        /* BFD has local symbols (basically used for F_LSYMS in a COFF
           header).  */
        HAS_LOCALS = 0x20,

        /* BFD is a dynamic object.  */
        DYNAMIC = 0x40,

        /* Text section is write protected (if D_PAGED is not set, this is
           like an a.out NMAGIC file) (the linker sets this by default, but
           clears it for -r or -N).  */
        WP_TEXT = 0x80,

        /* BFD is dynamically paged (this is like an a.out ZMAGIC file) (the
           linker sets this by default, but clears it for -r or -n or -N).  */
        D_PAGED = 0x100,

        /* BFD is relaxable (this means that bfd_relax_section may be able to
           do something) (sometimes bfd_relax_section can do something even if
           this is not set).  */
        BFD_IS_RELAXABLE = 0x200,

        /* This may be set before writing out a BFD to request using a
           traditional format.  For example, this is used to request that when
           writing out an a.out object the symbols not be hashed to eliminate
           duplicates.  */
        BFD_TRADITIONAL_FORMAT = 0x400,

        /* This flag indicates that the BFD contents are actually cached
           in memory.  If this is set, iostream points to a bfd_in_memory
           struct.  */
        BFD_IN_MEMORY = 0x800,

        /* The sections in this BFD specify a memory page.  */
        HAS_LOAD_PAGE = 0x1000,

        /* This BFD has been created by the linker and doesn't correspond
           to any input file.  */
        BFD_LINKER_CREATED = 0x2000,

        /* This may be set before writing out a BFD to request that it
           be written using values for UIDs, GIDs, timestamps, etc. that
           will be consistent from run to run.  */
        BFD_DETERMINISTIC_OUTPUT = 0x4000,

        /* Compress sections in this BFD.  */
        BFD_COMPRESS = 0x8000,

        /* Decompress sections in this BFD.  */
        BFD_DECOMPRESS = 0x10000,

        /* BFD is a dummy, for plugins.  */
        BFD_PLUGIN = 0x20000
    }
}
