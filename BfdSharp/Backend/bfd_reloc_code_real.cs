﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BfdSharp.Backend
{
    public enum bfd_reloc_code_real 
    {
        _dummy_first_bfd_reloc_code_real,


        /* Basic absolute relocations of N bits.  */
        BFD_RELOC_64,
        BFD_RELOC_32,
        BFD_RELOC_26,
        BFD_RELOC_24,
        BFD_RELOC_16,
        BFD_RELOC_14,
        BFD_RELOC_8,

        /* PC-relative relocations.  Sometimes these are relative to the address
        of the relocation itself; sometimes they are relative to the start of
        the section containing the relocation.  It depends on the specific target.

        The 24-bit relocation is used in some Intel 960 configurations.  */
        BFD_RELOC_64_PCREL,
        BFD_RELOC_32_PCREL,
        BFD_RELOC_24_PCREL,
        BFD_RELOC_16_PCREL,
        BFD_RELOC_12_PCREL,
        BFD_RELOC_8_PCREL,

        /* Section relative relocations.  Some targets need this for DWARF2.  */
        BFD_RELOC_32_SECREL,

        /* For ELF.  */
        BFD_RELOC_32_GOT_PCREL,
        BFD_RELOC_16_GOT_PCREL,
        BFD_RELOC_8_GOT_PCREL,
        BFD_RELOC_32_GOTOFF,
        BFD_RELOC_16_GOTOFF,
        BFD_RELOC_LO16_GOTOFF,
        BFD_RELOC_HI16_GOTOFF,
        BFD_RELOC_HI16_S_GOTOFF,
        BFD_RELOC_8_GOTOFF,
        BFD_RELOC_64_PLT_PCREL,
        BFD_RELOC_32_PLT_PCREL,
        BFD_RELOC_24_PLT_PCREL,
        BFD_RELOC_16_PLT_PCREL,
        BFD_RELOC_8_PLT_PCREL,
        BFD_RELOC_64_PLTOFF,
        BFD_RELOC_32_PLTOFF,
        BFD_RELOC_16_PLTOFF,
        BFD_RELOC_LO16_PLTOFF,
        BFD_RELOC_HI16_PLTOFF,
        BFD_RELOC_HI16_S_PLTOFF,
        BFD_RELOC_8_PLTOFF,

        /* Size relocations.  */
        BFD_RELOC_SIZE32,
        BFD_RELOC_SIZE64,

        /* Relocations used by 68K ELF.  */
        BFD_RELOC_68K_GLOB_DAT,
        BFD_RELOC_68K_JMP_SLOT,
        BFD_RELOC_68K_RELATIVE,
        BFD_RELOC_68K_TLS_GD32,
        BFD_RELOC_68K_TLS_GD16,
        BFD_RELOC_68K_TLS_GD8,
        BFD_RELOC_68K_TLS_LDM32,
        BFD_RELOC_68K_TLS_LDM16,
        BFD_RELOC_68K_TLS_LDM8,
        BFD_RELOC_68K_TLS_LDO32,
        BFD_RELOC_68K_TLS_LDO16,
        BFD_RELOC_68K_TLS_LDO8,
        BFD_RELOC_68K_TLS_IE32,
        BFD_RELOC_68K_TLS_IE16,
        BFD_RELOC_68K_TLS_IE8,
        BFD_RELOC_68K_TLS_LE32,
        BFD_RELOC_68K_TLS_LE16,
        BFD_RELOC_68K_TLS_LE8,

        /* Linkage-table relative.  */
        BFD_RELOC_32_BASEREL,
        BFD_RELOC_16_BASEREL,
        BFD_RELOC_LO16_BASEREL,
        BFD_RELOC_HI16_BASEREL,
        BFD_RELOC_HI16_S_BASEREL,
        BFD_RELOC_8_BASEREL,
        BFD_RELOC_RVA,

        /* Absolute 8-bit relocation, but used to form an address like 0xFFnn.  */
        BFD_RELOC_8_FFnn,

        /* These PC-relative relocations are stored as word displacements --
        i.e., byte displacements shifted right two bits.  The 30-bit word
        displacement (<<32_PCREL_S2>> -- 32 bits, shifted 2) is used on the
        SPARC.  (SPARC tools generally refer to this as <<WDISP30>>.)  The
        signed 16-bit displacement is used on the MIPS, and the 23-bit
        displacement is used on the Alpha.  */
        BFD_RELOC_32_PCREL_S2,
        BFD_RELOC_16_PCREL_S2,
        BFD_RELOC_23_PCREL_S2,

        /* High 22 bits and low 10 bits of 32-bit value, placed into lower bits of
        the target word.  These are used on the SPARC.  */
        BFD_RELOC_HI22,
        BFD_RELOC_LO10,

        /* For systems that allocate a Global Pointer register, these are
        displacements off that register.  These relocation types are
        handled specially, because the value the register will have is
        decided relatively late.  */
        BFD_RELOC_GPREL16,
        BFD_RELOC_GPREL32,

        /* Reloc types used for i960/b.out.  */
        BFD_RELOC_I960_CALLJ,

        /* SPARC ELF relocations.  There is probably some overlap with other
        relocation types already defined.  */
        BFD_RELOC_NONE,
        BFD_RELOC_SPARC_WDISP22,
        BFD_RELOC_SPARC22,
        BFD_RELOC_SPARC13,
        BFD_RELOC_SPARC_GOT10,
        BFD_RELOC_SPARC_GOT13,
        BFD_RELOC_SPARC_GOT22,
        BFD_RELOC_SPARC_PC10,
        BFD_RELOC_SPARC_PC22,
        BFD_RELOC_SPARC_WPLT30,
        BFD_RELOC_SPARC_COPY,
        BFD_RELOC_SPARC_GLOB_DAT,
        BFD_RELOC_SPARC_JMP_SLOT,
        BFD_RELOC_SPARC_RELATIVE,
        BFD_RELOC_SPARC_UA16,
        BFD_RELOC_SPARC_UA32,
        BFD_RELOC_SPARC_UA64,
        BFD_RELOC_SPARC_GOTDATA_HIX22,
        BFD_RELOC_SPARC_GOTDATA_LOX10,
        BFD_RELOC_SPARC_GOTDATA_OP_HIX22,
        BFD_RELOC_SPARC_GOTDATA_OP_LOX10,
        BFD_RELOC_SPARC_GOTDATA_OP,
        BFD_RELOC_SPARC_JMP_IREL,
        BFD_RELOC_SPARC_IRELATIVE,

        /* I think these are specific to SPARC a.out (e.g., Sun 4).  */
        BFD_RELOC_SPARC_BASE13,
        BFD_RELOC_SPARC_BASE22,

        /* SPARC64 relocations  */
        BFD_RELOC_SPARC_10,
        BFD_RELOC_SPARC_11,
        BFD_RELOC_SPARC_OLO10,
        BFD_RELOC_SPARC_HH22,
        BFD_RELOC_SPARC_HM10,
        BFD_RELOC_SPARC_LM22,
        BFD_RELOC_SPARC_PC_HH22,
        BFD_RELOC_SPARC_PC_HM10,
        BFD_RELOC_SPARC_PC_LM22,
        BFD_RELOC_SPARC_WDISP16,
        BFD_RELOC_SPARC_WDISP19,
        BFD_RELOC_SPARC_7,
        BFD_RELOC_SPARC_6,
        BFD_RELOC_SPARC_5,
        BFD_RELOC_SPARC_PLT32,
        BFD_RELOC_SPARC_PLT64,
        BFD_RELOC_SPARC_HIX22,
        BFD_RELOC_SPARC_LOX10,
        BFD_RELOC_SPARC_H44,
        BFD_RELOC_SPARC_M44,
        BFD_RELOC_SPARC_L44,
        BFD_RELOC_SPARC_REGISTER,
        BFD_RELOC_SPARC_H34,
        BFD_RELOC_SPARC_SIZE32,
        BFD_RELOC_SPARC_SIZE64,
        BFD_RELOC_SPARC_WDISP10,

        /* SPARC little endian relocation  */
        BFD_RELOC_SPARC_REV32,

        /* SPARC TLS relocations  */
        BFD_RELOC_SPARC_TLS_GD_HI22,
        BFD_RELOC_SPARC_TLS_GD_LO10,
        BFD_RELOC_SPARC_TLS_GD_ADD,
        BFD_RELOC_SPARC_TLS_GD_CALL,
        BFD_RELOC_SPARC_TLS_LDM_HI22,
        BFD_RELOC_SPARC_TLS_LDM_LO10,
        BFD_RELOC_SPARC_TLS_LDM_ADD,
        BFD_RELOC_SPARC_TLS_LDM_CALL,
        BFD_RELOC_SPARC_TLS_LDO_HIX22,
        BFD_RELOC_SPARC_TLS_LDO_LOX10,
        BFD_RELOC_SPARC_TLS_LDO_ADD,
        BFD_RELOC_SPARC_TLS_IE_HI22,
        BFD_RELOC_SPARC_TLS_IE_LO10,
        BFD_RELOC_SPARC_TLS_IE_LD,
        BFD_RELOC_SPARC_TLS_IE_LDX,
        BFD_RELOC_SPARC_TLS_IE_ADD,
        BFD_RELOC_SPARC_TLS_LE_HIX22,
        BFD_RELOC_SPARC_TLS_LE_LOX10,
        BFD_RELOC_SPARC_TLS_DTPMOD32,
        BFD_RELOC_SPARC_TLS_DTPMOD64,
        BFD_RELOC_SPARC_TLS_DTPOFF32,
        BFD_RELOC_SPARC_TLS_DTPOFF64,
        BFD_RELOC_SPARC_TLS_TPOFF32,
        BFD_RELOC_SPARC_TLS_TPOFF64,

        /* SPU Relocations.  */
        BFD_RELOC_SPU_IMM7,
        BFD_RELOC_SPU_IMM8,
        BFD_RELOC_SPU_IMM10,
        BFD_RELOC_SPU_IMM10W,
        BFD_RELOC_SPU_IMM16,
        BFD_RELOC_SPU_IMM16W,
        BFD_RELOC_SPU_IMM18,
        BFD_RELOC_SPU_PCREL9a,
        BFD_RELOC_SPU_PCREL9b,
        BFD_RELOC_SPU_PCREL16,
        BFD_RELOC_SPU_LO16,
        BFD_RELOC_SPU_HI16,
        BFD_RELOC_SPU_PPU32,
        BFD_RELOC_SPU_PPU64,
        BFD_RELOC_SPU_ADD_PIC,

        /* Alpha ECOFF and ELF relocations.  Some of these treat the symbol or
        "addend" in some special way.
        For GPDISP_HI16 ("gpdisp") relocations, the symbol is ignored when
        writing; when reading, it will be the absolute section symbol.  The
        addend is the displacement in bytes of the "lda" instruction from
        the "ldah" instruction (which is at the address of this reloc).  */
        BFD_RELOC_ALPHA_GPDISP_HI16,

        /* For GPDISP_LO16 ("ignore") relocations, the symbol is handled as
        with GPDISP_HI16 relocs.  The addend is ignored when writing the
        relocations out, and is filled in with the file's GP value on
        reading, for convenience.  */
        BFD_RELOC_ALPHA_GPDISP_LO16,

        /* The ELF GPDISP relocation is exactly the same as the GPDISP_HI16
        relocation except that there is no accompanying GPDISP_LO16
        relocation.  */
        BFD_RELOC_ALPHA_GPDISP,

        /* The Alpha LITERAL/LITUSE relocs are produced by a symbol reference;
        the assembler turns it into a LDQ instruction to load the address of
        the symbol, and then fills in a register in the real instruction.

        The LITERAL reloc, at the LDQ instruction, refers to the .lita
        section symbol.  The addend is ignored when writing, but is filled
        in with the file's GP value on reading, for convenience, as with the
        GPDISP_LO16 reloc.

        The ELF_LITERAL reloc is somewhere between 16_GOTOFF and GPDISP_LO16.
        It should refer to the symbol to be referenced, as with 16_GOTOFF,
        but it generates output not based on the position within the .got
        section, but relative to the GP value chosen for the file during the
        final link stage.

        The LITUSE reloc, on the instruction using the loaded address, gives
        information to the linker that it might be able to use to optimize
        away some literal section references.  The symbol is ignored (read
        as the absolute section symbol), and the "addend" indicates the type
        of instruction using the register:
        1 - "memory" fmt insn
        2 - byte-manipulation (byte offset reg)
        3 - jsr (target of branch)  */
        BFD_RELOC_ALPHA_LITERAL,
        BFD_RELOC_ALPHA_ELF_LITERAL,
        BFD_RELOC_ALPHA_LITUSE,

        /* The HINT relocation indicates a value that should be filled into the
        "hint" field of a jmp/jsr/ret instruction, for possible branch-
        prediction logic which may be provided on some processors.  */
        BFD_RELOC_ALPHA_HINT,

        /* The LINKAGE relocation outputs a linkage pair in the object file,
        which is filled by the linker.  */
        BFD_RELOC_ALPHA_LINKAGE,

        /* The CODEADDR relocation outputs a STO_CA in the object file,
        which is filled by the linker.  */
        BFD_RELOC_ALPHA_CODEADDR,

        /* The GPREL_HI/LO relocations together form a 32-bit offset from the
        GP register.  */
        BFD_RELOC_ALPHA_GPREL_HI16,
        BFD_RELOC_ALPHA_GPREL_LO16,

        /* Like BFD_RELOC_23_PCREL_S2, except that the source and target must
        share a common GP, and the target address is adjusted for
        STO_ALPHA_STD_GPLOAD.  */
        BFD_RELOC_ALPHA_BRSGP,

        /* The NOP relocation outputs a NOP if the longword displacement
        between two procedure entry points is < 2^21.  */
        BFD_RELOC_ALPHA_NOP,

        /* The BSR relocation outputs a BSR if the longword displacement
        between two procedure entry points is < 2^21.  */
        BFD_RELOC_ALPHA_BSR,

        /* The LDA relocation outputs a LDA if the longword displacement
        between two procedure entry points is < 2^16.  */
        BFD_RELOC_ALPHA_LDA,

        /* The BOH relocation outputs a BSR if the longword displacement
        between two procedure entry points is < 2^21, or else a hint.  */
        BFD_RELOC_ALPHA_BOH,

        /* Alpha thread-local storage relocations.  */
        BFD_RELOC_ALPHA_TLSGD,
        BFD_RELOC_ALPHA_TLSLDM,
        BFD_RELOC_ALPHA_DTPMOD64,
        BFD_RELOC_ALPHA_GOTDTPREL16,
        BFD_RELOC_ALPHA_DTPREL64,
        BFD_RELOC_ALPHA_DTPREL_HI16,
        BFD_RELOC_ALPHA_DTPREL_LO16,
        BFD_RELOC_ALPHA_DTPREL16,
        BFD_RELOC_ALPHA_GOTTPREL16,
        BFD_RELOC_ALPHA_TPREL64,
        BFD_RELOC_ALPHA_TPREL_HI16,
        BFD_RELOC_ALPHA_TPREL_LO16,
        BFD_RELOC_ALPHA_TPREL16,

        /* The MIPS jump instruction.  */
        BFD_RELOC_MIPS_JMP,
        BFD_RELOC_MICROMIPS_JMP,

        /* The MIPS16 jump instruction.  */
        BFD_RELOC_MIPS16_JMP,

        /* MIPS16 GP relative reloc.  */
        BFD_RELOC_MIPS16_GPREL,

        /* High 16 bits of 32-bit value; simple reloc.  */
        BFD_RELOC_HI16,

        /* High 16 bits of 32-bit value but the low 16 bits will be sign
        extended and added to form the final result.  If the low 16
        bits form a negative number, we need to add one to the high value
        to compensate for the borrow when the low bits are added.  */
        BFD_RELOC_HI16_S,

        /* Low 16 bits.  */
        BFD_RELOC_LO16,

        /* High 16 bits of 32-bit pc-relative value  */
        BFD_RELOC_HI16_PCREL,

        /* High 16 bits of 32-bit pc-relative value, adjusted  */
        BFD_RELOC_HI16_S_PCREL,

        /* Low 16 bits of pc-relative value  */
        BFD_RELOC_LO16_PCREL,

        /* Equivalent of BFD_RELOC_MIPS_*, but with the MIPS16 layout of
        16-bit immediate fields  */
        BFD_RELOC_MIPS16_GOT16,
        BFD_RELOC_MIPS16_CALL16,

        /* MIPS16 high 16 bits of 32-bit value.  */
        BFD_RELOC_MIPS16_HI16,

        /* MIPS16 high 16 bits of 32-bit value but the low 16 bits will be sign
        extended and added to form the final result.  If the low 16
        bits form a negative number, we need to add one to the high value
        to compensate for the borrow when the low bits are added.  */
        BFD_RELOC_MIPS16_HI16_S,

        /* MIPS16 low 16 bits.  */
        BFD_RELOC_MIPS16_LO16,

        /* MIPS16 TLS relocations  */
        BFD_RELOC_MIPS16_TLS_GD,
        BFD_RELOC_MIPS16_TLS_LDM,
        BFD_RELOC_MIPS16_TLS_DTPREL_HI16,
        BFD_RELOC_MIPS16_TLS_DTPREL_LO16,
        BFD_RELOC_MIPS16_TLS_GOTTPREL,
        BFD_RELOC_MIPS16_TLS_TPREL_HI16,
        BFD_RELOC_MIPS16_TLS_TPREL_LO16,

        /* Relocation against a MIPS literal section.  */
        BFD_RELOC_MIPS_LITERAL,
        BFD_RELOC_MICROMIPS_LITERAL,

        /* microMIPS PC-relative relocations.  */
        BFD_RELOC_MICROMIPS_7_PCREL_S1,
        BFD_RELOC_MICROMIPS_10_PCREL_S1,
        BFD_RELOC_MICROMIPS_16_PCREL_S1,

        /* microMIPS versions of generic BFD relocs.  */
        BFD_RELOC_MICROMIPS_GPREL16,
        BFD_RELOC_MICROMIPS_HI16,
        BFD_RELOC_MICROMIPS_HI16_S,
        BFD_RELOC_MICROMIPS_LO16,

        /* MIPS ELF relocations.  */
        BFD_RELOC_MIPS_GOT16,
        BFD_RELOC_MICROMIPS_GOT16,
        BFD_RELOC_MIPS_CALL16,
        BFD_RELOC_MICROMIPS_CALL16,
        BFD_RELOC_MIPS_GOT_HI16,
        BFD_RELOC_MICROMIPS_GOT_HI16,
        BFD_RELOC_MIPS_GOT_LO16,
        BFD_RELOC_MICROMIPS_GOT_LO16,
        BFD_RELOC_MIPS_CALL_HI16,
        BFD_RELOC_MICROMIPS_CALL_HI16,
        BFD_RELOC_MIPS_CALL_LO16,
        BFD_RELOC_MICROMIPS_CALL_LO16,
        BFD_RELOC_MIPS_SUB,
        BFD_RELOC_MICROMIPS_SUB,
        BFD_RELOC_MIPS_GOT_PAGE,
        BFD_RELOC_MICROMIPS_GOT_PAGE,
        BFD_RELOC_MIPS_GOT_OFST,
        BFD_RELOC_MICROMIPS_GOT_OFST,
        BFD_RELOC_MIPS_GOT_DISP,
        BFD_RELOC_MICROMIPS_GOT_DISP,
        BFD_RELOC_MIPS_SHIFT5,
        BFD_RELOC_MIPS_SHIFT6,
        BFD_RELOC_MIPS_INSERT_A,
        BFD_RELOC_MIPS_INSERT_B,
        BFD_RELOC_MIPS_DELETE,
        BFD_RELOC_MIPS_HIGHEST,
        BFD_RELOC_MICROMIPS_HIGHEST,
        BFD_RELOC_MIPS_HIGHER,
        BFD_RELOC_MICROMIPS_HIGHER,
        BFD_RELOC_MIPS_SCN_DISP,
        BFD_RELOC_MICROMIPS_SCN_DISP,
        BFD_RELOC_MIPS_REL16,
        BFD_RELOC_MIPS_RELGOT,
        BFD_RELOC_MIPS_JALR,
        BFD_RELOC_MICROMIPS_JALR,
        BFD_RELOC_MIPS_TLS_DTPMOD32,
        BFD_RELOC_MIPS_TLS_DTPREL32,
        BFD_RELOC_MIPS_TLS_DTPMOD64,
        BFD_RELOC_MIPS_TLS_DTPREL64,
        BFD_RELOC_MIPS_TLS_GD,
        BFD_RELOC_MICROMIPS_TLS_GD,
        BFD_RELOC_MIPS_TLS_LDM,
        BFD_RELOC_MICROMIPS_TLS_LDM,
        BFD_RELOC_MIPS_TLS_DTPREL_HI16,
        BFD_RELOC_MICROMIPS_TLS_DTPREL_HI16,
        BFD_RELOC_MIPS_TLS_DTPREL_LO16,
        BFD_RELOC_MICROMIPS_TLS_DTPREL_LO16,
        BFD_RELOC_MIPS_TLS_GOTTPREL,
        BFD_RELOC_MICROMIPS_TLS_GOTTPREL,
        BFD_RELOC_MIPS_TLS_TPREL32,
        BFD_RELOC_MIPS_TLS_TPREL64,
        BFD_RELOC_MIPS_TLS_TPREL_HI16,
        BFD_RELOC_MICROMIPS_TLS_TPREL_HI16,
        BFD_RELOC_MIPS_TLS_TPREL_LO16,
        BFD_RELOC_MICROMIPS_TLS_TPREL_LO16,
        BFD_RELOC_MIPS_EH,


        /* MIPS ELF relocations (VxWorks and PLT extensions).  */
        BFD_RELOC_MIPS_COPY,
        BFD_RELOC_MIPS_JUMP_SLOT,


        /* Moxie ELF relocations.  */
        BFD_RELOC_MOXIE_10_PCREL,


        /* Fujitsu Frv Relocations.  */
        BFD_RELOC_FRV_LABEL16,
        BFD_RELOC_FRV_LABEL24,
        BFD_RELOC_FRV_LO16,
        BFD_RELOC_FRV_HI16,
        BFD_RELOC_FRV_GPREL12,
        BFD_RELOC_FRV_GPRELU12,
        BFD_RELOC_FRV_GPREL32,
        BFD_RELOC_FRV_GPRELHI,
        BFD_RELOC_FRV_GPRELLO,
        BFD_RELOC_FRV_GOT12,
        BFD_RELOC_FRV_GOTHI,
        BFD_RELOC_FRV_GOTLO,
        BFD_RELOC_FRV_FUNCDESC,
        BFD_RELOC_FRV_FUNCDESC_GOT12,
        BFD_RELOC_FRV_FUNCDESC_GOTHI,
        BFD_RELOC_FRV_FUNCDESC_GOTLO,
        BFD_RELOC_FRV_FUNCDESC_VALUE,
        BFD_RELOC_FRV_FUNCDESC_GOTOFF12,
        BFD_RELOC_FRV_FUNCDESC_GOTOFFHI,
        BFD_RELOC_FRV_FUNCDESC_GOTOFFLO,
        BFD_RELOC_FRV_GOTOFF12,
        BFD_RELOC_FRV_GOTOFFHI,
        BFD_RELOC_FRV_GOTOFFLO,
        BFD_RELOC_FRV_GETTLSOFF,
        BFD_RELOC_FRV_TLSDESC_VALUE,
        BFD_RELOC_FRV_GOTTLSDESC12,
        BFD_RELOC_FRV_GOTTLSDESCHI,
        BFD_RELOC_FRV_GOTTLSDESCLO,
        BFD_RELOC_FRV_TLSMOFF12,
        BFD_RELOC_FRV_TLSMOFFHI,
        BFD_RELOC_FRV_TLSMOFFLO,
        BFD_RELOC_FRV_GOTTLSOFF12,
        BFD_RELOC_FRV_GOTTLSOFFHI,
        BFD_RELOC_FRV_GOTTLSOFFLO,
        BFD_RELOC_FRV_TLSOFF,
        BFD_RELOC_FRV_TLSDESC_RELAX,
        BFD_RELOC_FRV_GETTLSOFF_RELAX,
        BFD_RELOC_FRV_TLSOFF_RELAX,
        BFD_RELOC_FRV_TLSMOFF,


        /* This is a 24bit GOT-relative reloc for the mn10300.  */
        BFD_RELOC_MN10300_GOTOFF24,

        /* This is a 32bit GOT-relative reloc for the mn10300, offset by two bytes
        in the instruction.  */
        BFD_RELOC_MN10300_GOT32,

        /* This is a 24bit GOT-relative reloc for the mn10300, offset by two bytes
        in the instruction.  */
        BFD_RELOC_MN10300_GOT24,

        /* This is a 16bit GOT-relative reloc for the mn10300, offset by two bytes
        in the instruction.  */
        BFD_RELOC_MN10300_GOT16,

        /* Copy symbol at runtime.  */
        BFD_RELOC_MN10300_COPY,

        /* Create GOT entry.  */
        BFD_RELOC_MN10300_GLOB_DAT,

        /* Create PLT entry.  */
        BFD_RELOC_MN10300_JMP_SLOT,

        /* Adjust by program base.  */
        BFD_RELOC_MN10300_RELATIVE,

        /* Together with another reloc targeted at the same location,
        allows for a value that is the difference of two symbols
        in the same section.  */
        BFD_RELOC_MN10300_SYM_DIFF,

        /* The addend of this reloc is an alignment power that must
        be honoured at the offset's location, regardless of linker
        relaxation.  */
        BFD_RELOC_MN10300_ALIGN,

        /* Various TLS-related relocations.  */
        BFD_RELOC_MN10300_TLS_GD,
        BFD_RELOC_MN10300_TLS_LD,
        BFD_RELOC_MN10300_TLS_LDO,
        BFD_RELOC_MN10300_TLS_GOTIE,
        BFD_RELOC_MN10300_TLS_IE,
        BFD_RELOC_MN10300_TLS_LE,
        BFD_RELOC_MN10300_TLS_DTPMOD,
        BFD_RELOC_MN10300_TLS_DTPOFF,
        BFD_RELOC_MN10300_TLS_TPOFF,

        /* This is a 32bit pcrel reloc for the mn10300, offset by two bytes in the
        instruction.  */
        BFD_RELOC_MN10300_32_PCREL,

        /* This is a 16bit pcrel reloc for the mn10300, offset by two bytes in the
        instruction.  */
        BFD_RELOC_MN10300_16_PCREL,


        /* i386/elf relocations  */
        BFD_RELOC_386_GOT32,
        BFD_RELOC_386_PLT32,
        BFD_RELOC_386_COPY,
        BFD_RELOC_386_GLOB_DAT,
        BFD_RELOC_386_JUMP_SLOT,
        BFD_RELOC_386_RELATIVE,
        BFD_RELOC_386_GOTOFF,
        BFD_RELOC_386_GOTPC,
        BFD_RELOC_386_TLS_TPOFF,
        BFD_RELOC_386_TLS_IE,
        BFD_RELOC_386_TLS_GOTIE,
        BFD_RELOC_386_TLS_LE,
        BFD_RELOC_386_TLS_GD,
        BFD_RELOC_386_TLS_LDM,
        BFD_RELOC_386_TLS_LDO_32,
        BFD_RELOC_386_TLS_IE_32,
        BFD_RELOC_386_TLS_LE_32,
        BFD_RELOC_386_TLS_DTPMOD32,
        BFD_RELOC_386_TLS_DTPOFF32,
        BFD_RELOC_386_TLS_TPOFF32,
        BFD_RELOC_386_TLS_GOTDESC,
        BFD_RELOC_386_TLS_DESC_CALL,
        BFD_RELOC_386_TLS_DESC,
        BFD_RELOC_386_IRELATIVE,

        /* x86-64/elf relocations  */
        BFD_RELOC_X86_64_GOT32,
        BFD_RELOC_X86_64_PLT32,
        BFD_RELOC_X86_64_COPY,
        BFD_RELOC_X86_64_GLOB_DAT,
        BFD_RELOC_X86_64_JUMP_SLOT,
        BFD_RELOC_X86_64_RELATIVE,
        BFD_RELOC_X86_64_GOTPCREL,
        BFD_RELOC_X86_64_32S,
        BFD_RELOC_X86_64_DTPMOD64,
        BFD_RELOC_X86_64_DTPOFF64,
        BFD_RELOC_X86_64_TPOFF64,
        BFD_RELOC_X86_64_TLSGD,
        BFD_RELOC_X86_64_TLSLD,
        BFD_RELOC_X86_64_DTPOFF32,
        BFD_RELOC_X86_64_GOTTPOFF,
        BFD_RELOC_X86_64_TPOFF32,
        BFD_RELOC_X86_64_GOTOFF64,
        BFD_RELOC_X86_64_GOTPC32,
        BFD_RELOC_X86_64_GOT64,
        BFD_RELOC_X86_64_GOTPCREL64,
        BFD_RELOC_X86_64_GOTPC64,
        BFD_RELOC_X86_64_GOTPLT64,
        BFD_RELOC_X86_64_PLTOFF64,
        BFD_RELOC_X86_64_GOTPC32_TLSDESC,
        BFD_RELOC_X86_64_TLSDESC_CALL,
        BFD_RELOC_X86_64_TLSDESC,
        BFD_RELOC_X86_64_IRELATIVE,
        BFD_RELOC_X86_64_PC32_BND,
        BFD_RELOC_X86_64_PLT32_BND,

        /* ns32k relocations  */
        BFD_RELOC_NS32K_IMM_8,
        BFD_RELOC_NS32K_IMM_16,
        BFD_RELOC_NS32K_IMM_32,
        BFD_RELOC_NS32K_IMM_8_PCREL,
        BFD_RELOC_NS32K_IMM_16_PCREL,
        BFD_RELOC_NS32K_IMM_32_PCREL,
        BFD_RELOC_NS32K_DISP_8,
        BFD_RELOC_NS32K_DISP_16,
        BFD_RELOC_NS32K_DISP_32,
        BFD_RELOC_NS32K_DISP_8_PCREL,
        BFD_RELOC_NS32K_DISP_16_PCREL,
        BFD_RELOC_NS32K_DISP_32_PCREL,

        /* PDP11 relocations  */
        BFD_RELOC_PDP11_DISP_8_PCREL,
        BFD_RELOC_PDP11_DISP_6_PCREL,

        /* Picojava relocs.  Not all of these appear in object files.  */
        BFD_RELOC_PJ_CODE_HI16,
        BFD_RELOC_PJ_CODE_LO16,
        BFD_RELOC_PJ_CODE_DIR16,
        BFD_RELOC_PJ_CODE_DIR32,
        BFD_RELOC_PJ_CODE_REL16,
        BFD_RELOC_PJ_CODE_REL32,

        /* Power(rs6000) and PowerPC relocations.  */
        BFD_RELOC_PPC_B26,
        BFD_RELOC_PPC_BA26,
        BFD_RELOC_PPC_TOC16,
        BFD_RELOC_PPC_B16,
        BFD_RELOC_PPC_B16_BRTAKEN,
        BFD_RELOC_PPC_B16_BRNTAKEN,
        BFD_RELOC_PPC_BA16,
        BFD_RELOC_PPC_BA16_BRTAKEN,
        BFD_RELOC_PPC_BA16_BRNTAKEN,
        BFD_RELOC_PPC_COPY,
        BFD_RELOC_PPC_GLOB_DAT,
        BFD_RELOC_PPC_JMP_SLOT,
        BFD_RELOC_PPC_RELATIVE,
        BFD_RELOC_PPC_LOCAL24PC,
        BFD_RELOC_PPC_EMB_NADDR32,
        BFD_RELOC_PPC_EMB_NADDR16,
        BFD_RELOC_PPC_EMB_NADDR16_LO,
        BFD_RELOC_PPC_EMB_NADDR16_HI,
        BFD_RELOC_PPC_EMB_NADDR16_HA,
        BFD_RELOC_PPC_EMB_SDAI16,
        BFD_RELOC_PPC_EMB_SDA2I16,
        BFD_RELOC_PPC_EMB_SDA2REL,
        BFD_RELOC_PPC_EMB_SDA21,
        BFD_RELOC_PPC_EMB_MRKREF,
        BFD_RELOC_PPC_EMB_RELSEC16,
        BFD_RELOC_PPC_EMB_RELST_LO,
        BFD_RELOC_PPC_EMB_RELST_HI,
        BFD_RELOC_PPC_EMB_RELST_HA,
        BFD_RELOC_PPC_EMB_BIT_FLD,
        BFD_RELOC_PPC_EMB_RELSDA,
        BFD_RELOC_PPC_VLE_REL8,
        BFD_RELOC_PPC_VLE_REL15,
        BFD_RELOC_PPC_VLE_REL24,
        BFD_RELOC_PPC_VLE_LO16A,
        BFD_RELOC_PPC_VLE_LO16D,
        BFD_RELOC_PPC_VLE_HI16A,
        BFD_RELOC_PPC_VLE_HI16D,
        BFD_RELOC_PPC_VLE_HA16A,
        BFD_RELOC_PPC_VLE_HA16D,
        BFD_RELOC_PPC_VLE_SDA21,
        BFD_RELOC_PPC_VLE_SDA21_LO,
        BFD_RELOC_PPC_VLE_SDAREL_LO16A,
        BFD_RELOC_PPC_VLE_SDAREL_LO16D,
        BFD_RELOC_PPC_VLE_SDAREL_HI16A,
        BFD_RELOC_PPC_VLE_SDAREL_HI16D,
        BFD_RELOC_PPC_VLE_SDAREL_HA16A,
        BFD_RELOC_PPC_VLE_SDAREL_HA16D,
        BFD_RELOC_PPC64_HIGHER,
        BFD_RELOC_PPC64_HIGHER_S,
        BFD_RELOC_PPC64_HIGHEST,
        BFD_RELOC_PPC64_HIGHEST_S,
        BFD_RELOC_PPC64_TOC16_LO,
        BFD_RELOC_PPC64_TOC16_HI,
        BFD_RELOC_PPC64_TOC16_HA,
        BFD_RELOC_PPC64_TOC,
        BFD_RELOC_PPC64_PLTGOT16,
        BFD_RELOC_PPC64_PLTGOT16_LO,
        BFD_RELOC_PPC64_PLTGOT16_HI,
        BFD_RELOC_PPC64_PLTGOT16_HA,
        BFD_RELOC_PPC64_ADDR16_DS,
        BFD_RELOC_PPC64_ADDR16_LO_DS,
        BFD_RELOC_PPC64_GOT16_DS,
        BFD_RELOC_PPC64_GOT16_LO_DS,
        BFD_RELOC_PPC64_PLT16_LO_DS,
        BFD_RELOC_PPC64_SECTOFF_DS,
        BFD_RELOC_PPC64_SECTOFF_LO_DS,
        BFD_RELOC_PPC64_TOC16_DS,
        BFD_RELOC_PPC64_TOC16_LO_DS,
        BFD_RELOC_PPC64_PLTGOT16_DS,
        BFD_RELOC_PPC64_PLTGOT16_LO_DS,
        BFD_RELOC_PPC64_ADDR16_HIGH,
        BFD_RELOC_PPC64_ADDR16_HIGHA,

        /* PowerPC and PowerPC64 thread-local storage relocations.  */
        BFD_RELOC_PPC_TLS,
        BFD_RELOC_PPC_TLSGD,
        BFD_RELOC_PPC_TLSLD,
        BFD_RELOC_PPC_DTPMOD,
        BFD_RELOC_PPC_TPREL16,
        BFD_RELOC_PPC_TPREL16_LO,
        BFD_RELOC_PPC_TPREL16_HI,
        BFD_RELOC_PPC_TPREL16_HA,
        BFD_RELOC_PPC_TPREL,
        BFD_RELOC_PPC_DTPREL16,
        BFD_RELOC_PPC_DTPREL16_LO,
        BFD_RELOC_PPC_DTPREL16_HI,
        BFD_RELOC_PPC_DTPREL16_HA,
        BFD_RELOC_PPC_DTPREL,
        BFD_RELOC_PPC_GOT_TLSGD16,
        BFD_RELOC_PPC_GOT_TLSGD16_LO,
        BFD_RELOC_PPC_GOT_TLSGD16_HI,
        BFD_RELOC_PPC_GOT_TLSGD16_HA,
        BFD_RELOC_PPC_GOT_TLSLD16,
        BFD_RELOC_PPC_GOT_TLSLD16_LO,
        BFD_RELOC_PPC_GOT_TLSLD16_HI,
        BFD_RELOC_PPC_GOT_TLSLD16_HA,
        BFD_RELOC_PPC_GOT_TPREL16,
        BFD_RELOC_PPC_GOT_TPREL16_LO,
        BFD_RELOC_PPC_GOT_TPREL16_HI,
        BFD_RELOC_PPC_GOT_TPREL16_HA,
        BFD_RELOC_PPC_GOT_DTPREL16,
        BFD_RELOC_PPC_GOT_DTPREL16_LO,
        BFD_RELOC_PPC_GOT_DTPREL16_HI,
        BFD_RELOC_PPC_GOT_DTPREL16_HA,
        BFD_RELOC_PPC64_TPREL16_DS,
        BFD_RELOC_PPC64_TPREL16_LO_DS,
        BFD_RELOC_PPC64_TPREL16_HIGHER,
        BFD_RELOC_PPC64_TPREL16_HIGHERA,
        BFD_RELOC_PPC64_TPREL16_HIGHEST,
        BFD_RELOC_PPC64_TPREL16_HIGHESTA,
        BFD_RELOC_PPC64_DTPREL16_DS,
        BFD_RELOC_PPC64_DTPREL16_LO_DS,
        BFD_RELOC_PPC64_DTPREL16_HIGHER,
        BFD_RELOC_PPC64_DTPREL16_HIGHERA,
        BFD_RELOC_PPC64_DTPREL16_HIGHEST,
        BFD_RELOC_PPC64_DTPREL16_HIGHESTA,
        BFD_RELOC_PPC64_TPREL16_HIGH,
        BFD_RELOC_PPC64_TPREL16_HIGHA,
        BFD_RELOC_PPC64_DTPREL16_HIGH,
        BFD_RELOC_PPC64_DTPREL16_HIGHA,

        /* IBM 370/390 relocations  */
        BFD_RELOC_I370_D12,

        /* The type of reloc used to build a constructor table - at the moment
        probably a 32 bit wide absolute relocation, but the target can choose.
        It generally does map to one of the other relocation types.  */
        BFD_RELOC_CTOR,

        /* ARM 26 bit pc-relative branch.  The lowest two bits must be zero and are
        not stored in the instruction.  */
        BFD_RELOC_ARM_PCREL_BRANCH,

        /* ARM 26 bit pc-relative branch.  The lowest bit must be zero and is
        not stored in the instruction.  The 2nd lowest bit comes from a 1 bit
        field in the instruction.  */
        BFD_RELOC_ARM_PCREL_BLX,

        /* Thumb 22 bit pc-relative branch.  The lowest bit must be zero and is
        not stored in the instruction.  The 2nd lowest bit comes from a 1 bit
        field in the instruction.  */
        BFD_RELOC_THUMB_PCREL_BLX,

        /* ARM 26-bit pc-relative branch for an unconditional BL or BLX instruction.  */
        BFD_RELOC_ARM_PCREL_CALL,

        /* ARM 26-bit pc-relative branch for B or conditional BL instruction.  */
        BFD_RELOC_ARM_PCREL_JUMP,

        /* Thumb 7-, 9-, 12-, 20-, 23-, and 25-bit pc-relative branches.
        The lowest bit must be zero and is not stored in the instruction.
        Note that the corresponding ELF R_ARM_THM_JUMPnn constant has an
        "nn" one smaller in all cases.  Note further that BRANCH23
        corresponds to R_ARM_THM_CALL.  */
        BFD_RELOC_THUMB_PCREL_BRANCH7,
        BFD_RELOC_THUMB_PCREL_BRANCH9,
        BFD_RELOC_THUMB_PCREL_BRANCH12,
        BFD_RELOC_THUMB_PCREL_BRANCH20,
        BFD_RELOC_THUMB_PCREL_BRANCH23,
        BFD_RELOC_THUMB_PCREL_BRANCH25,

        /* 12-bit immediate offset, used in ARM-format ldr and str instructions.  */
        BFD_RELOC_ARM_OFFSET_IMM,

        /* 5-bit immediate offset, used in Thumb-format ldr and str instructions.  */
        BFD_RELOC_ARM_THUMB_OFFSET,

        /* Pc-relative or absolute relocation depending on target.  Used for
        entries in .init_array sections.  */
        BFD_RELOC_ARM_TARGET1,

        /* Read-only segment base relative address.  */
        BFD_RELOC_ARM_ROSEGREL32,

        /* Data segment base relative address.  */
        BFD_RELOC_ARM_SBREL32,

        /* This reloc is used for references to RTTI data from exception handling
        tables.  The actual definition depends on the target.  It may be a
        pc-relative or some form of GOT-indirect relocation.  */
        BFD_RELOC_ARM_TARGET2,

        /* 31-bit PC relative address.  */
        BFD_RELOC_ARM_PREL31,

        /* Low and High halfword relocations for MOVW and MOVT instructions.  */
        BFD_RELOC_ARM_MOVW,
        BFD_RELOC_ARM_MOVT,
        BFD_RELOC_ARM_MOVW_PCREL,
        BFD_RELOC_ARM_MOVT_PCREL,
        BFD_RELOC_ARM_THUMB_MOVW,
        BFD_RELOC_ARM_THUMB_MOVT,
        BFD_RELOC_ARM_THUMB_MOVW_PCREL,
        BFD_RELOC_ARM_THUMB_MOVT_PCREL,

        /* Relocations for setting up GOTs and PLTs for shared libraries.  */
        BFD_RELOC_ARM_JUMP_SLOT,
        BFD_RELOC_ARM_GLOB_DAT,
        BFD_RELOC_ARM_GOT32,
        BFD_RELOC_ARM_PLT32,
        BFD_RELOC_ARM_RELATIVE,
        BFD_RELOC_ARM_GOTOFF,
        BFD_RELOC_ARM_GOTPC,
        BFD_RELOC_ARM_GOT_PREL,

        /* ARM thread-local storage relocations.  */
        BFD_RELOC_ARM_TLS_GD32,
        BFD_RELOC_ARM_TLS_LDO32,
        BFD_RELOC_ARM_TLS_LDM32,
        BFD_RELOC_ARM_TLS_DTPOFF32,
        BFD_RELOC_ARM_TLS_DTPMOD32,
        BFD_RELOC_ARM_TLS_TPOFF32,
        BFD_RELOC_ARM_TLS_IE32,
        BFD_RELOC_ARM_TLS_LE32,
        BFD_RELOC_ARM_TLS_GOTDESC,
        BFD_RELOC_ARM_TLS_CALL,
        BFD_RELOC_ARM_THM_TLS_CALL,
        BFD_RELOC_ARM_TLS_DESCSEQ,
        BFD_RELOC_ARM_THM_TLS_DESCSEQ,
        BFD_RELOC_ARM_TLS_DESC,

        /* ARM group relocations.  */
        BFD_RELOC_ARM_ALU_PC_G0_NC,
        BFD_RELOC_ARM_ALU_PC_G0,
        BFD_RELOC_ARM_ALU_PC_G1_NC,
        BFD_RELOC_ARM_ALU_PC_G1,
        BFD_RELOC_ARM_ALU_PC_G2,
        BFD_RELOC_ARM_LDR_PC_G0,
        BFD_RELOC_ARM_LDR_PC_G1,
        BFD_RELOC_ARM_LDR_PC_G2,
        BFD_RELOC_ARM_LDRS_PC_G0,
        BFD_RELOC_ARM_LDRS_PC_G1,
        BFD_RELOC_ARM_LDRS_PC_G2,
        BFD_RELOC_ARM_LDC_PC_G0,
        BFD_RELOC_ARM_LDC_PC_G1,
        BFD_RELOC_ARM_LDC_PC_G2,
        BFD_RELOC_ARM_ALU_SB_G0_NC,
        BFD_RELOC_ARM_ALU_SB_G0,
        BFD_RELOC_ARM_ALU_SB_G1_NC,
        BFD_RELOC_ARM_ALU_SB_G1,
        BFD_RELOC_ARM_ALU_SB_G2,
        BFD_RELOC_ARM_LDR_SB_G0,
        BFD_RELOC_ARM_LDR_SB_G1,
        BFD_RELOC_ARM_LDR_SB_G2,
        BFD_RELOC_ARM_LDRS_SB_G0,
        BFD_RELOC_ARM_LDRS_SB_G1,
        BFD_RELOC_ARM_LDRS_SB_G2,
        BFD_RELOC_ARM_LDC_SB_G0,
        BFD_RELOC_ARM_LDC_SB_G1,
        BFD_RELOC_ARM_LDC_SB_G2,

        /* Annotation of BX instructions.  */
        BFD_RELOC_ARM_V4BX,

        /* ARM support for STT_GNU_IFUNC.  */
        BFD_RELOC_ARM_IRELATIVE,

        /* These relocs are only used within the ARM assembler.  They are not
        (at present) written to any object files.  */
        BFD_RELOC_ARM_IMMEDIATE,
        BFD_RELOC_ARM_ADRL_IMMEDIATE,
        BFD_RELOC_ARM_T32_IMMEDIATE,
        BFD_RELOC_ARM_T32_ADD_IMM,
        BFD_RELOC_ARM_T32_IMM12,
        BFD_RELOC_ARM_T32_ADD_PC12,
        BFD_RELOC_ARM_SHIFT_IMM,
        BFD_RELOC_ARM_SMC,
        BFD_RELOC_ARM_HVC,
        BFD_RELOC_ARM_SWI,
        BFD_RELOC_ARM_MULTI,
        BFD_RELOC_ARM_CP_OFF_IMM,
        BFD_RELOC_ARM_CP_OFF_IMM_S2,
        BFD_RELOC_ARM_T32_CP_OFF_IMM,
        BFD_RELOC_ARM_T32_CP_OFF_IMM_S2,
        BFD_RELOC_ARM_ADR_IMM,
        BFD_RELOC_ARM_LDR_IMM,
        BFD_RELOC_ARM_LITERAL,
        BFD_RELOC_ARM_IN_POOL,
        BFD_RELOC_ARM_OFFSET_IMM8,
        BFD_RELOC_ARM_T32_OFFSET_U8,
        BFD_RELOC_ARM_T32_OFFSET_IMM,
        BFD_RELOC_ARM_HWLITERAL,
        BFD_RELOC_ARM_THUMB_ADD,
        BFD_RELOC_ARM_THUMB_IMM,
        BFD_RELOC_ARM_THUMB_SHIFT,

        /* Renesas / SuperH SH relocs.  Not all of these appear in object files.  */
        BFD_RELOC_SH_PCDISP8BY2,
        BFD_RELOC_SH_PCDISP12BY2,
        BFD_RELOC_SH_IMM3,
        BFD_RELOC_SH_IMM3U,
        BFD_RELOC_SH_DISP12,
        BFD_RELOC_SH_DISP12BY2,
        BFD_RELOC_SH_DISP12BY4,
        BFD_RELOC_SH_DISP12BY8,
        BFD_RELOC_SH_DISP20,
        BFD_RELOC_SH_DISP20BY8,
        BFD_RELOC_SH_IMM4,
        BFD_RELOC_SH_IMM4BY2,
        BFD_RELOC_SH_IMM4BY4,
        BFD_RELOC_SH_IMM8,
        BFD_RELOC_SH_IMM8BY2,
        BFD_RELOC_SH_IMM8BY4,
        BFD_RELOC_SH_PCRELIMM8BY2,
        BFD_RELOC_SH_PCRELIMM8BY4,
        BFD_RELOC_SH_SWITCH16,
        BFD_RELOC_SH_SWITCH32,
        BFD_RELOC_SH_USES,
        BFD_RELOC_SH_COUNT,
        BFD_RELOC_SH_ALIGN,
        BFD_RELOC_SH_CODE,
        BFD_RELOC_SH_DATA,
        BFD_RELOC_SH_LABEL,
        BFD_RELOC_SH_LOOP_START,
        BFD_RELOC_SH_LOOP_END,
        BFD_RELOC_SH_COPY,
        BFD_RELOC_SH_GLOB_DAT,
        BFD_RELOC_SH_JMP_SLOT,
        BFD_RELOC_SH_RELATIVE,
        BFD_RELOC_SH_GOTPC,
        BFD_RELOC_SH_GOT_LOW16,
        BFD_RELOC_SH_GOT_MEDLOW16,
        BFD_RELOC_SH_GOT_MEDHI16,
        BFD_RELOC_SH_GOT_HI16,
        BFD_RELOC_SH_GOTPLT_LOW16,
        BFD_RELOC_SH_GOTPLT_MEDLOW16,
        BFD_RELOC_SH_GOTPLT_MEDHI16,
        BFD_RELOC_SH_GOTPLT_HI16,
        BFD_RELOC_SH_PLT_LOW16,
        BFD_RELOC_SH_PLT_MEDLOW16,
        BFD_RELOC_SH_PLT_MEDHI16,
        BFD_RELOC_SH_PLT_HI16,
        BFD_RELOC_SH_GOTOFF_LOW16,
        BFD_RELOC_SH_GOTOFF_MEDLOW16,
        BFD_RELOC_SH_GOTOFF_MEDHI16,
        BFD_RELOC_SH_GOTOFF_HI16,
        BFD_RELOC_SH_GOTPC_LOW16,
        BFD_RELOC_SH_GOTPC_MEDLOW16,
        BFD_RELOC_SH_GOTPC_MEDHI16,
        BFD_RELOC_SH_GOTPC_HI16,
        BFD_RELOC_SH_COPY64,
        BFD_RELOC_SH_GLOB_DAT64,
        BFD_RELOC_SH_JMP_SLOT64,
        BFD_RELOC_SH_RELATIVE64,
        BFD_RELOC_SH_GOT10BY4,
        BFD_RELOC_SH_GOT10BY8,
        BFD_RELOC_SH_GOTPLT10BY4,
        BFD_RELOC_SH_GOTPLT10BY8,
        BFD_RELOC_SH_GOTPLT32,
        BFD_RELOC_SH_SHMEDIA_CODE,
        BFD_RELOC_SH_IMMU5,
        BFD_RELOC_SH_IMMS6,
        BFD_RELOC_SH_IMMS6BY32,
        BFD_RELOC_SH_IMMU6,
        BFD_RELOC_SH_IMMS10,
        BFD_RELOC_SH_IMMS10BY2,
        BFD_RELOC_SH_IMMS10BY4,
        BFD_RELOC_SH_IMMS10BY8,
        BFD_RELOC_SH_IMMS16,
        BFD_RELOC_SH_IMMU16,
        BFD_RELOC_SH_IMM_LOW16,
        BFD_RELOC_SH_IMM_LOW16_PCREL,
        BFD_RELOC_SH_IMM_MEDLOW16,
        BFD_RELOC_SH_IMM_MEDLOW16_PCREL,
        BFD_RELOC_SH_IMM_MEDHI16,
        BFD_RELOC_SH_IMM_MEDHI16_PCREL,
        BFD_RELOC_SH_IMM_HI16,
        BFD_RELOC_SH_IMM_HI16_PCREL,
        BFD_RELOC_SH_PT_16,
        BFD_RELOC_SH_TLS_GD_32,
        BFD_RELOC_SH_TLS_LD_32,
        BFD_RELOC_SH_TLS_LDO_32,
        BFD_RELOC_SH_TLS_IE_32,
        BFD_RELOC_SH_TLS_LE_32,
        BFD_RELOC_SH_TLS_DTPMOD32,
        BFD_RELOC_SH_TLS_DTPOFF32,
        BFD_RELOC_SH_TLS_TPOFF32,
        BFD_RELOC_SH_GOT20,
        BFD_RELOC_SH_GOTOFF20,
        BFD_RELOC_SH_GOTFUNCDESC,
        BFD_RELOC_SH_GOTFUNCDESC20,
        BFD_RELOC_SH_GOTOFFFUNCDESC,
        BFD_RELOC_SH_GOTOFFFUNCDESC20,
        BFD_RELOC_SH_FUNCDESC,

        /* ARC Cores relocs.
        ARC 22 bit pc-relative branch.  The lowest two bits must be zero and are
        not stored in the instruction.  The high 20 bits are installed in bits 26
        through 7 of the instruction.  */
        BFD_RELOC_ARC_B22_PCREL,

        /* ARC 26 bit absolute branch.  The lowest two bits must be zero and are not
        stored in the instruction.  The high 24 bits are installed in bits 23
        through 0.  */
        BFD_RELOC_ARC_B26,

        /* ADI Blackfin 16 bit immediate absolute reloc.  */
        BFD_RELOC_BFIN_16_IMM,

        /* ADI Blackfin 16 bit immediate absolute reloc higher 16 bits.  */
        BFD_RELOC_BFIN_16_HIGH,

        /* ADI Blackfin 'a' part of LSETUP.  */
        BFD_RELOC_BFIN_4_PCREL,

        /* ADI Blackfin.  */
        BFD_RELOC_BFIN_5_PCREL,

        /* ADI Blackfin 16 bit immediate absolute reloc lower 16 bits.  */
        BFD_RELOC_BFIN_16_LOW,

        /* ADI Blackfin.  */
        BFD_RELOC_BFIN_10_PCREL,

        /* ADI Blackfin 'b' part of LSETUP.  */
        BFD_RELOC_BFIN_11_PCREL,

        /* ADI Blackfin.  */
        BFD_RELOC_BFIN_12_PCREL_JUMP,

        /* ADI Blackfin Short jump, pcrel.  */
        BFD_RELOC_BFIN_12_PCREL_JUMP_S,

        /* ADI Blackfin Call.x not implemented.  */
        BFD_RELOC_BFIN_24_PCREL_CALL_X,

        /* ADI Blackfin Long Jump pcrel.  */
        BFD_RELOC_BFIN_24_PCREL_JUMP_L,

        /* ADI Blackfin FD-PIC relocations.  */
        BFD_RELOC_BFIN_GOT17M4,
        BFD_RELOC_BFIN_GOTHI,
        BFD_RELOC_BFIN_GOTLO,
        BFD_RELOC_BFIN_FUNCDESC,
        BFD_RELOC_BFIN_FUNCDESC_GOT17M4,
        BFD_RELOC_BFIN_FUNCDESC_GOTHI,
        BFD_RELOC_BFIN_FUNCDESC_GOTLO,
        BFD_RELOC_BFIN_FUNCDESC_VALUE,
        BFD_RELOC_BFIN_FUNCDESC_GOTOFF17M4,
        BFD_RELOC_BFIN_FUNCDESC_GOTOFFHI,
        BFD_RELOC_BFIN_FUNCDESC_GOTOFFLO,
        BFD_RELOC_BFIN_GOTOFF17M4,
        BFD_RELOC_BFIN_GOTOFFHI,
        BFD_RELOC_BFIN_GOTOFFLO,

        /* ADI Blackfin GOT relocation.  */
        BFD_RELOC_BFIN_GOT,

        /* ADI Blackfin PLTPC relocation.  */
        BFD_RELOC_BFIN_PLTPC,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_PUSH,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_CONST,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_ADD,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_SUB,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_MULT,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_DIV,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_MOD,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_LSHIFT,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_RSHIFT,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_AND,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_OR,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_XOR,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_LAND,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_LOR,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_LEN,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_NEG,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_COMP,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_PAGE,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_HWPAGE,

        /* ADI Blackfin arithmetic relocation.  */
        BFD_ARELOC_BFIN_ADDR,

        /* Mitsubishi D10V relocs.
        This is a 10-bit reloc with the right 2 bits
        assumed to be 0.  */
        BFD_RELOC_D10V_10_PCREL_R,

        /* Mitsubishi D10V relocs.
        This is a 10-bit reloc with the right 2 bits
        assumed to be 0.  This is the same as the previous reloc
        except it is in the left container, i.e.,
        shifted left 15 bits.  */
        BFD_RELOC_D10V_10_PCREL_L,

        /* This is an 18-bit reloc with the right 2 bits
        assumed to be 0.  */
        BFD_RELOC_D10V_18,

        /* This is an 18-bit reloc with the right 2 bits
        assumed to be 0.  */
        BFD_RELOC_D10V_18_PCREL,

        /* Mitsubishi D30V relocs.
        This is a 6-bit absolute reloc.  */
        BFD_RELOC_D30V_6,

        /* This is a 6-bit pc-relative reloc with
        the right 3 bits assumed to be 0.  */
        BFD_RELOC_D30V_9_PCREL,

        /* This is a 6-bit pc-relative reloc with
        the right 3 bits assumed to be 0. Same
        as the previous reloc but on the right side
        of the container.  */
        BFD_RELOC_D30V_9_PCREL_R,

        /* This is a 12-bit absolute reloc with the
        right 3 bitsassumed to be 0.  */
        BFD_RELOC_D30V_15,

        /* This is a 12-bit pc-relative reloc with
        the right 3 bits assumed to be 0.  */
        BFD_RELOC_D30V_15_PCREL,

        /* This is a 12-bit pc-relative reloc with
        the right 3 bits assumed to be 0. Same
        as the previous reloc but on the right side
        of the container.  */
        BFD_RELOC_D30V_15_PCREL_R,

        /* This is an 18-bit absolute reloc with
        the right 3 bits assumed to be 0.  */
        BFD_RELOC_D30V_21,

        /* This is an 18-bit pc-relative reloc with
        the right 3 bits assumed to be 0.  */
        BFD_RELOC_D30V_21_PCREL,

        /* This is an 18-bit pc-relative reloc with
        the right 3 bits assumed to be 0. Same
        as the previous reloc but on the right side
        of the container.  */
        BFD_RELOC_D30V_21_PCREL_R,

        /* This is a 32-bit absolute reloc.  */
        BFD_RELOC_D30V_32,

        /* This is a 32-bit pc-relative reloc.  */
        BFD_RELOC_D30V_32_PCREL,

        /* DLX relocs  */
        BFD_RELOC_DLX_HI16_S,

        /* DLX relocs  */
        BFD_RELOC_DLX_LO16,

        /* DLX relocs  */
        BFD_RELOC_DLX_JMP26,

        /* Renesas M16C/M32C Relocations.  */
        BFD_RELOC_M32C_HI8,
        BFD_RELOC_M32C_RL_JUMP,
        BFD_RELOC_M32C_RL_1ADDR,
        BFD_RELOC_M32C_RL_2ADDR,

        /* Renesas M32R (formerly Mitsubishi M32R) relocs.
        This is a 24 bit absolute address.  */
        BFD_RELOC_M32R_24,

        /* This is a 10-bit pc-relative reloc with the right 2 bits assumed to be 0.  */
        BFD_RELOC_M32R_10_PCREL,

        /* This is an 18-bit reloc with the right 2 bits assumed to be 0.  */
        BFD_RELOC_M32R_18_PCREL,

        /* This is a 26-bit reloc with the right 2 bits assumed to be 0.  */
        BFD_RELOC_M32R_26_PCREL,

        /* This is a 16-bit reloc containing the high 16 bits of an address
        used when the lower 16 bits are treated as unsigned.  */
        BFD_RELOC_M32R_HI16_ULO,

        /* This is a 16-bit reloc containing the high 16 bits of an address
        used when the lower 16 bits are treated as signed.  */
        BFD_RELOC_M32R_HI16_SLO,

        /* This is a 16-bit reloc containing the lower 16 bits of an address.  */
        BFD_RELOC_M32R_LO16,

        /* This is a 16-bit reloc containing the small data area offset for use in
        add3, load, and store instructions.  */
        BFD_RELOC_M32R_SDA16,

        /* For PIC.  */
        BFD_RELOC_M32R_GOT24,
        BFD_RELOC_M32R_26_PLTREL,
        BFD_RELOC_M32R_COPY,
        BFD_RELOC_M32R_GLOB_DAT,
        BFD_RELOC_M32R_JMP_SLOT,
        BFD_RELOC_M32R_RELATIVE,
        BFD_RELOC_M32R_GOTOFF,
        BFD_RELOC_M32R_GOTOFF_HI_ULO,
        BFD_RELOC_M32R_GOTOFF_HI_SLO,
        BFD_RELOC_M32R_GOTOFF_LO,
        BFD_RELOC_M32R_GOTPC24,
        BFD_RELOC_M32R_GOT16_HI_ULO,
        BFD_RELOC_M32R_GOT16_HI_SLO,
        BFD_RELOC_M32R_GOT16_LO,
        BFD_RELOC_M32R_GOTPC_HI_ULO,
        BFD_RELOC_M32R_GOTPC_HI_SLO,
        BFD_RELOC_M32R_GOTPC_LO,

        /* This is a 9-bit reloc  */
        BFD_RELOC_V850_9_PCREL,

        /* This is a 22-bit reloc  */
        BFD_RELOC_V850_22_PCREL,

        /* This is a 16 bit offset from the short data area pointer.  */
        BFD_RELOC_V850_SDA_16_16_OFFSET,

        /* This is a 16 bit offset (of which only 15 bits are used) from the
        short data area pointer.  */
        BFD_RELOC_V850_SDA_15_16_OFFSET,

        /* This is a 16 bit offset from the zero data area pointer.  */
        BFD_RELOC_V850_ZDA_16_16_OFFSET,

        /* This is a 16 bit offset (of which only 15 bits are used) from the
        zero data area pointer.  */
        BFD_RELOC_V850_ZDA_15_16_OFFSET,

        /* This is an 8 bit offset (of which only 6 bits are used) from the
        tiny data area pointer.  */
        BFD_RELOC_V850_TDA_6_8_OFFSET,

        /* This is an 8bit offset (of which only 7 bits are used) from the tiny
        data area pointer.  */
        BFD_RELOC_V850_TDA_7_8_OFFSET,

        /* This is a 7 bit offset from the tiny data area pointer.  */
        BFD_RELOC_V850_TDA_7_7_OFFSET,

        /* This is a 16 bit offset from the tiny data area pointer.  */
        BFD_RELOC_V850_TDA_16_16_OFFSET,

        /* This is a 5 bit offset (of which only 4 bits are used) from the tiny
        data area pointer.  */
        BFD_RELOC_V850_TDA_4_5_OFFSET,

        /* This is a 4 bit offset from the tiny data area pointer.  */
        BFD_RELOC_V850_TDA_4_4_OFFSET,

        /* This is a 16 bit offset from the short data area pointer, with the
        bits placed non-contiguously in the instruction.  */
        BFD_RELOC_V850_SDA_16_16_SPLIT_OFFSET,

        /* This is a 16 bit offset from the zero data area pointer, with the
        bits placed non-contiguously in the instruction.  */
        BFD_RELOC_V850_ZDA_16_16_SPLIT_OFFSET,

        /* This is a 6 bit offset from the call table base pointer.  */
        BFD_RELOC_V850_CALLT_6_7_OFFSET,

        /* This is a 16 bit offset from the call table base pointer.  */
        BFD_RELOC_V850_CALLT_16_16_OFFSET,

        /* Used for relaxing indirect function calls.  */
        BFD_RELOC_V850_LONGCALL,

        /* Used for relaxing indirect jumps.  */
        BFD_RELOC_V850_LONGJUMP,

        /* Used to maintain alignment whilst relaxing.  */
        BFD_RELOC_V850_ALIGN,

        /* This is a variation of BFD_RELOC_LO16 that can be used in v850e ld.bu
        instructions.  */
        BFD_RELOC_V850_LO16_SPLIT_OFFSET,

        /* This is a 16-bit reloc.  */
        BFD_RELOC_V850_16_PCREL,

        /* This is a 17-bit reloc.  */
        BFD_RELOC_V850_17_PCREL,

        /* This is a 23-bit reloc.  */
        BFD_RELOC_V850_23,

        /* This is a 32-bit reloc.  */
        BFD_RELOC_V850_32_PCREL,

        /* This is a 32-bit reloc.  */
        BFD_RELOC_V850_32_ABS,

        /* This is a 16-bit reloc.  */
        BFD_RELOC_V850_16_SPLIT_OFFSET,

        /* This is a 16-bit reloc.  */
        BFD_RELOC_V850_16_S1,

        /* Low 16 bits. 16 bit shifted by 1.  */
        BFD_RELOC_V850_LO16_S1,

        /* This is a 16 bit offset from the call table base pointer.  */
        BFD_RELOC_V850_CALLT_15_16_OFFSET,

        /* DSO relocations.  */
        BFD_RELOC_V850_32_GOTPCREL,

        /* DSO relocations.  */
        BFD_RELOC_V850_16_GOT,

        /* DSO relocations.  */
        BFD_RELOC_V850_32_GOT,

        /* DSO relocations.  */
        BFD_RELOC_V850_22_PLT_PCREL,

        /* DSO relocations.  */
        BFD_RELOC_V850_32_PLT_PCREL,

        /* DSO relocations.  */
        BFD_RELOC_V850_COPY,

        /* DSO relocations.  */
        BFD_RELOC_V850_GLOB_DAT,

        /* DSO relocations.  */
        BFD_RELOC_V850_JMP_SLOT,

        /* DSO relocations.  */
        BFD_RELOC_V850_RELATIVE,

        /* DSO relocations.  */
        BFD_RELOC_V850_16_GOTOFF,

        /* DSO relocations.  */
        BFD_RELOC_V850_32_GOTOFF,

        /* start code.  */
        BFD_RELOC_V850_CODE,

        /* start data in text.  */
        BFD_RELOC_V850_DATA,

        /* This is a 8bit DP reloc for the tms320c30, where the most
        significant 8 bits of a 24 bit word are placed into the least
        significant 8 bits of the opcode.  */
        BFD_RELOC_TIC30_LDP,

        /* This is a 7bit reloc for the tms320c54x, where the least
        significant 7 bits of a 16 bit word are placed into the least
        significant 7 bits of the opcode.  */
        BFD_RELOC_TIC54X_PARTLS7,

        /* This is a 9bit DP reloc for the tms320c54x, where the most
        significant 9 bits of a 16 bit word are placed into the least
        significant 9 bits of the opcode.  */
        BFD_RELOC_TIC54X_PARTMS9,

        /* This is an extended address 23-bit reloc for the tms320c54x.  */
        BFD_RELOC_TIC54X_23,

        /* This is a 16-bit reloc for the tms320c54x, where the least
        significant 16 bits of a 23-bit extended address are placed into
        the opcode.  */
        BFD_RELOC_TIC54X_16_OF_23,

        /* This is a reloc for the tms320c54x, where the most
        significant 7 bits of a 23-bit extended address are placed into
        the opcode.  */
        BFD_RELOC_TIC54X_MS7_OF_23,

        /* TMS320C6000 relocations.  */
        BFD_RELOC_C6000_PCR_S21,
        BFD_RELOC_C6000_PCR_S12,
        BFD_RELOC_C6000_PCR_S10,
        BFD_RELOC_C6000_PCR_S7,
        BFD_RELOC_C6000_ABS_S16,
        BFD_RELOC_C6000_ABS_L16,
        BFD_RELOC_C6000_ABS_H16,
        BFD_RELOC_C6000_SBR_U15_B,
        BFD_RELOC_C6000_SBR_U15_H,
        BFD_RELOC_C6000_SBR_U15_W,
        BFD_RELOC_C6000_SBR_S16,
        BFD_RELOC_C6000_SBR_L16_B,
        BFD_RELOC_C6000_SBR_L16_H,
        BFD_RELOC_C6000_SBR_L16_W,
        BFD_RELOC_C6000_SBR_H16_B,
        BFD_RELOC_C6000_SBR_H16_H,
        BFD_RELOC_C6000_SBR_H16_W,
        BFD_RELOC_C6000_SBR_GOT_U15_W,
        BFD_RELOC_C6000_SBR_GOT_L16_W,
        BFD_RELOC_C6000_SBR_GOT_H16_W,
        BFD_RELOC_C6000_DSBT_INDEX,
        BFD_RELOC_C6000_PREL31,
        BFD_RELOC_C6000_COPY,
        BFD_RELOC_C6000_JUMP_SLOT,
        BFD_RELOC_C6000_EHTYPE,
        BFD_RELOC_C6000_PCR_H16,
        BFD_RELOC_C6000_PCR_L16,
        BFD_RELOC_C6000_ALIGN,
        BFD_RELOC_C6000_FPHEAD,
        BFD_RELOC_C6000_NOCMP,

        /* This is a 48 bit reloc for the FR30 that stores 32 bits.  */
        BFD_RELOC_FR30_48,

        /* This is a 32 bit reloc for the FR30 that stores 20 bits split up into
        two sections.  */
        BFD_RELOC_FR30_20,

        /* This is a 16 bit reloc for the FR30 that stores a 6 bit word offset in
        4 bits.  */
        BFD_RELOC_FR30_6_IN_4,

        /* This is a 16 bit reloc for the FR30 that stores an 8 bit byte offset
        into 8 bits.  */
        BFD_RELOC_FR30_8_IN_8,

        /* This is a 16 bit reloc for the FR30 that stores a 9 bit short offset
        into 8 bits.  */
        BFD_RELOC_FR30_9_IN_8,

        /* This is a 16 bit reloc for the FR30 that stores a 10 bit word offset
        into 8 bits.  */
        BFD_RELOC_FR30_10_IN_8,

        /* This is a 16 bit reloc for the FR30 that stores a 9 bit pc relative
        short offset into 8 bits.  */
        BFD_RELOC_FR30_9_PCREL,

        /* This is a 16 bit reloc for the FR30 that stores a 12 bit pc relative
        short offset into 11 bits.  */
        BFD_RELOC_FR30_12_PCREL,

        /* Motorola Mcore relocations.  */
        BFD_RELOC_MCORE_PCREL_IMM8BY4,
        BFD_RELOC_MCORE_PCREL_IMM11BY2,
        BFD_RELOC_MCORE_PCREL_IMM4BY2,
        BFD_RELOC_MCORE_PCREL_32,
        BFD_RELOC_MCORE_PCREL_JSR_IMM11BY2,
        BFD_RELOC_MCORE_RVA,

        /* Toshiba Media Processor Relocations.  */
        BFD_RELOC_MEP_8,
        BFD_RELOC_MEP_16,
        BFD_RELOC_MEP_32,
        BFD_RELOC_MEP_PCREL8A2,
        BFD_RELOC_MEP_PCREL12A2,
        BFD_RELOC_MEP_PCREL17A2,
        BFD_RELOC_MEP_PCREL24A2,
        BFD_RELOC_MEP_PCABS24A2,
        BFD_RELOC_MEP_LOW16,
        BFD_RELOC_MEP_HI16U,
        BFD_RELOC_MEP_HI16S,
        BFD_RELOC_MEP_GPREL,
        BFD_RELOC_MEP_TPREL,
        BFD_RELOC_MEP_TPREL7,
        BFD_RELOC_MEP_TPREL7A2,
        BFD_RELOC_MEP_TPREL7A4,
        BFD_RELOC_MEP_UIMM24,
        BFD_RELOC_MEP_ADDR24A4,
        BFD_RELOC_MEP_GNU_VTINHERIT,
        BFD_RELOC_MEP_GNU_VTENTRY,


        /* Imagination Technologies Meta relocations.  */
        BFD_RELOC_METAG_HIADDR16,
        BFD_RELOC_METAG_LOADDR16,
        BFD_RELOC_METAG_RELBRANCH,
        BFD_RELOC_METAG_GETSETOFF,
        BFD_RELOC_METAG_HIOG,
        BFD_RELOC_METAG_LOOG,
        BFD_RELOC_METAG_REL8,
        BFD_RELOC_METAG_REL16,
        BFD_RELOC_METAG_HI16_GOTOFF,
        BFD_RELOC_METAG_LO16_GOTOFF,
        BFD_RELOC_METAG_GETSET_GOTOFF,
        BFD_RELOC_METAG_GETSET_GOT,
        BFD_RELOC_METAG_HI16_GOTPC,
        BFD_RELOC_METAG_LO16_GOTPC,
        BFD_RELOC_METAG_HI16_PLT,
        BFD_RELOC_METAG_LO16_PLT,
        BFD_RELOC_METAG_RELBRANCH_PLT,
        BFD_RELOC_METAG_GOTOFF,
        BFD_RELOC_METAG_PLT,
        BFD_RELOC_METAG_COPY,
        BFD_RELOC_METAG_JMP_SLOT,
        BFD_RELOC_METAG_RELATIVE,
        BFD_RELOC_METAG_GLOB_DAT,
        BFD_RELOC_METAG_TLS_GD,
        BFD_RELOC_METAG_TLS_LDM,
        BFD_RELOC_METAG_TLS_LDO_HI16,
        BFD_RELOC_METAG_TLS_LDO_LO16,
        BFD_RELOC_METAG_TLS_LDO,
        BFD_RELOC_METAG_TLS_IE,
        BFD_RELOC_METAG_TLS_IENONPIC,
        BFD_RELOC_METAG_TLS_IENONPIC_HI16,
        BFD_RELOC_METAG_TLS_IENONPIC_LO16,
        BFD_RELOC_METAG_TLS_TPOFF,
        BFD_RELOC_METAG_TLS_DTPMOD,
        BFD_RELOC_METAG_TLS_DTPOFF,
        BFD_RELOC_METAG_TLS_LE,
        BFD_RELOC_METAG_TLS_LE_HI16,
        BFD_RELOC_METAG_TLS_LE_LO16,

        /* These are relocations for the GETA instruction.  */
        BFD_RELOC_MMIX_GETA,
        BFD_RELOC_MMIX_GETA_1,
        BFD_RELOC_MMIX_GETA_2,
        BFD_RELOC_MMIX_GETA_3,

        /* These are relocations for a conditional branch instruction.  */
        BFD_RELOC_MMIX_CBRANCH,
        BFD_RELOC_MMIX_CBRANCH_J,
        BFD_RELOC_MMIX_CBRANCH_1,
        BFD_RELOC_MMIX_CBRANCH_2,
        BFD_RELOC_MMIX_CBRANCH_3,

        /* These are relocations for the PUSHJ instruction.  */
        BFD_RELOC_MMIX_PUSHJ,
        BFD_RELOC_MMIX_PUSHJ_1,
        BFD_RELOC_MMIX_PUSHJ_2,
        BFD_RELOC_MMIX_PUSHJ_3,
        BFD_RELOC_MMIX_PUSHJ_STUBBABLE,

        /* These are relocations for the JMP instruction.  */
        BFD_RELOC_MMIX_JMP,
        BFD_RELOC_MMIX_JMP_1,
        BFD_RELOC_MMIX_JMP_2,
        BFD_RELOC_MMIX_JMP_3,

        /* This is a relocation for a relative address as in a GETA instruction or
        a branch.  */
        BFD_RELOC_MMIX_ADDR19,

        /* This is a relocation for a relative address as in a JMP instruction.  */
        BFD_RELOC_MMIX_ADDR27,

        /* This is a relocation for an instruction field that may be a general
        register or a value 0..255.  */
        BFD_RELOC_MMIX_REG_OR_BYTE,

        /* This is a relocation for an instruction field that may be a general
        register.  */
        BFD_RELOC_MMIX_REG,

        /* This is a relocation for two instruction fields holding a register and
        an offset, the equivalent of the relocation.  */
        BFD_RELOC_MMIX_BASE_PLUS_OFFSET,

        /* This relocation is an assertion that the expression is not allocated as
        a global register.  It does not modify contents.  */
        BFD_RELOC_MMIX_LOCAL,

        /* This is a 16 bit reloc for the AVR that stores 8 bit pc relative
        short offset into 7 bits.  */
        BFD_RELOC_AVR_7_PCREL,

        /* This is a 16 bit reloc for the AVR that stores 13 bit pc relative
        short offset into 12 bits.  */
        BFD_RELOC_AVR_13_PCREL,

        /* This is a 16 bit reloc for the AVR that stores 17 bit value (usually
        program memory address) into 16 bits.  */
        BFD_RELOC_AVR_16_PM,

        /* This is a 16 bit reloc for the AVR that stores 8 bit value (usually
        data memory address) into 8 bit immediate value of LDI insn.  */
        BFD_RELOC_AVR_LO8_LDI,

        /* This is a 16 bit reloc for the AVR that stores 8 bit value (high 8 bit
        of data memory address) into 8 bit immediate value of LDI insn.  */
        BFD_RELOC_AVR_HI8_LDI,

        /* This is a 16 bit reloc for the AVR that stores 8 bit value (most high 8 bit
        of program memory address) into 8 bit immediate value of LDI insn.  */
        BFD_RELOC_AVR_HH8_LDI,

        /* This is a 16 bit reloc for the AVR that stores 8 bit value (most high 8 bit
        of 32 bit value) into 8 bit immediate value of LDI insn.  */
        BFD_RELOC_AVR_MS8_LDI,

        /* This is a 16 bit reloc for the AVR that stores negated 8 bit value
        (usually data memory address) into 8 bit immediate value of SUBI insn.  */
        BFD_RELOC_AVR_LO8_LDI_NEG,

        /* This is a 16 bit reloc for the AVR that stores negated 8 bit value
        (high 8 bit of data memory address) into 8 bit immediate value of
        SUBI insn.  */
        BFD_RELOC_AVR_HI8_LDI_NEG,

        /* This is a 16 bit reloc for the AVR that stores negated 8 bit value
        (most high 8 bit of program memory address) into 8 bit immediate value
        of LDI or SUBI insn.  */
        BFD_RELOC_AVR_HH8_LDI_NEG,

        /* This is a 16 bit reloc for the AVR that stores negated 8 bit value (msb
        of 32 bit value) into 8 bit immediate value of LDI insn.  */
        BFD_RELOC_AVR_MS8_LDI_NEG,

        /* This is a 16 bit reloc for the AVR that stores 8 bit value (usually
        command address) into 8 bit immediate value of LDI insn.  */
        BFD_RELOC_AVR_LO8_LDI_PM,

        /* This is a 16 bit reloc for the AVR that stores 8 bit value
        (command address) into 8 bit immediate value of LDI insn. If the address
        is beyond the 128k boundary, the linker inserts a jump stub for this reloc
        in the lower 128k.  */
        BFD_RELOC_AVR_LO8_LDI_GS,

        /* This is a 16 bit reloc for the AVR that stores 8 bit value (high 8 bit
        of command address) into 8 bit immediate value of LDI insn.  */
        BFD_RELOC_AVR_HI8_LDI_PM,

        /* This is a 16 bit reloc for the AVR that stores 8 bit value (high 8 bit
        of command address) into 8 bit immediate value of LDI insn.  If the address
        is beyond the 128k boundary, the linker inserts a jump stub for this reloc
        below 128k.  */
        BFD_RELOC_AVR_HI8_LDI_GS,

        /* This is a 16 bit reloc for the AVR that stores 8 bit value (most high 8 bit
        of command address) into 8 bit immediate value of LDI insn.  */
        BFD_RELOC_AVR_HH8_LDI_PM,

        /* This is a 16 bit reloc for the AVR that stores negated 8 bit value
        (usually command address) into 8 bit immediate value of SUBI insn.  */
        BFD_RELOC_AVR_LO8_LDI_PM_NEG,

        /* This is a 16 bit reloc for the AVR that stores negated 8 bit value
        (high 8 bit of 16 bit command address) into 8 bit immediate value
        of SUBI insn.  */
        BFD_RELOC_AVR_HI8_LDI_PM_NEG,

        /* This is a 16 bit reloc for the AVR that stores negated 8 bit value
        (high 6 bit of 22 bit command address) into 8 bit immediate
        value of SUBI insn.  */
        BFD_RELOC_AVR_HH8_LDI_PM_NEG,

        /* This is a 32 bit reloc for the AVR that stores 23 bit value
        into 22 bits.  */
        BFD_RELOC_AVR_CALL,

        /* This is a 16 bit reloc for the AVR that stores all needed bits
        for absolute addressing with ldi with overflow check to linktime  */
        BFD_RELOC_AVR_LDI,

        /* This is a 6 bit reloc for the AVR that stores offset for ldd/std
        instructions  */
        BFD_RELOC_AVR_6,

        /* This is a 6 bit reloc for the AVR that stores offset for adiw/sbiw
        instructions  */
        BFD_RELOC_AVR_6_ADIW,

        /* This is a 8 bit reloc for the AVR that stores bits 0..7 of a symbol
        in .byte lo8(symbol)  */
        BFD_RELOC_AVR_8_LO,

        /* This is a 8 bit reloc for the AVR that stores bits 8..15 of a symbol
        in .byte hi8(symbol)  */
        BFD_RELOC_AVR_8_HI,

        /* This is a 8 bit reloc for the AVR that stores bits 16..23 of a symbol
        in .byte hlo8(symbol)  */
        BFD_RELOC_AVR_8_HLO,

        /* Renesas RL78 Relocations.  */
        BFD_RELOC_RL78_NEG8,
        BFD_RELOC_RL78_NEG16,
        BFD_RELOC_RL78_NEG24,
        BFD_RELOC_RL78_NEG32,
        BFD_RELOC_RL78_16_OP,
        BFD_RELOC_RL78_24_OP,
        BFD_RELOC_RL78_32_OP,
        BFD_RELOC_RL78_8U,
        BFD_RELOC_RL78_16U,
        BFD_RELOC_RL78_24U,
        BFD_RELOC_RL78_DIR3U_PCREL,
        BFD_RELOC_RL78_DIFF,
        BFD_RELOC_RL78_GPRELB,
        BFD_RELOC_RL78_GPRELW,
        BFD_RELOC_RL78_GPRELL,
        BFD_RELOC_RL78_SYM,
        BFD_RELOC_RL78_OP_SUBTRACT,
        BFD_RELOC_RL78_OP_NEG,
        BFD_RELOC_RL78_OP_AND,
        BFD_RELOC_RL78_OP_SHRA,
        BFD_RELOC_RL78_ABS8,
        BFD_RELOC_RL78_ABS16,
        BFD_RELOC_RL78_ABS16_REV,
        BFD_RELOC_RL78_ABS32,
        BFD_RELOC_RL78_ABS32_REV,
        BFD_RELOC_RL78_ABS16U,
        BFD_RELOC_RL78_ABS16UW,
        BFD_RELOC_RL78_ABS16UL,
        BFD_RELOC_RL78_RELAX,
        BFD_RELOC_RL78_HI16,
        BFD_RELOC_RL78_HI8,
        BFD_RELOC_RL78_LO16,
        BFD_RELOC_RL78_CODE,

        /* Renesas RX Relocations.  */
        BFD_RELOC_RX_NEG8,
        BFD_RELOC_RX_NEG16,
        BFD_RELOC_RX_NEG24,
        BFD_RELOC_RX_NEG32,
        BFD_RELOC_RX_16_OP,
        BFD_RELOC_RX_24_OP,
        BFD_RELOC_RX_32_OP,
        BFD_RELOC_RX_8U,
        BFD_RELOC_RX_16U,
        BFD_RELOC_RX_24U,
        BFD_RELOC_RX_DIR3U_PCREL,
        BFD_RELOC_RX_DIFF,
        BFD_RELOC_RX_GPRELB,
        BFD_RELOC_RX_GPRELW,
        BFD_RELOC_RX_GPRELL,
        BFD_RELOC_RX_SYM,
        BFD_RELOC_RX_OP_SUBTRACT,
        BFD_RELOC_RX_OP_NEG,
        BFD_RELOC_RX_ABS8,
        BFD_RELOC_RX_ABS16,
        BFD_RELOC_RX_ABS16_REV,
        BFD_RELOC_RX_ABS32,
        BFD_RELOC_RX_ABS32_REV,
        BFD_RELOC_RX_ABS16U,
        BFD_RELOC_RX_ABS16UW,
        BFD_RELOC_RX_ABS16UL,
        BFD_RELOC_RX_RELAX,

        /* Direct 12 bit.  */
        BFD_RELOC_390_12,

        /* 12 bit GOT offset.  */
        BFD_RELOC_390_GOT12,

        /* 32 bit PC relative PLT address.  */
        BFD_RELOC_390_PLT32,

        /* Copy symbol at runtime.  */
        BFD_RELOC_390_COPY,

        /* Create GOT entry.  */
        BFD_RELOC_390_GLOB_DAT,

        /* Create PLT entry.  */
        BFD_RELOC_390_JMP_SLOT,

        /* Adjust by program base.  */
        BFD_RELOC_390_RELATIVE,

        /* 32 bit PC relative offset to GOT.  */
        BFD_RELOC_390_GOTPC,

        /* 16 bit GOT offset.  */
        BFD_RELOC_390_GOT16,

        /* PC relative 12 bit shifted by 1.  */
        BFD_RELOC_390_PC12DBL,

        /* 12 bit PC rel. PLT shifted by 1.  */
        BFD_RELOC_390_PLT12DBL,

        /* PC relative 16 bit shifted by 1.  */
        BFD_RELOC_390_PC16DBL,

        /* 16 bit PC rel. PLT shifted by 1.  */
        BFD_RELOC_390_PLT16DBL,

        /* PC relative 24 bit shifted by 1.  */
        BFD_RELOC_390_PC24DBL,

        /* 24 bit PC rel. PLT shifted by 1.  */
        BFD_RELOC_390_PLT24DBL,

        /* PC relative 32 bit shifted by 1.  */
        BFD_RELOC_390_PC32DBL,

        /* 32 bit PC rel. PLT shifted by 1.  */
        BFD_RELOC_390_PLT32DBL,

        /* 32 bit PC rel. GOT shifted by 1.  */
        BFD_RELOC_390_GOTPCDBL,

        /* 64 bit GOT offset.  */
        BFD_RELOC_390_GOT64,

        /* 64 bit PC relative PLT address.  */
        BFD_RELOC_390_PLT64,

        /* 32 bit rel. offset to GOT entry.  */
        BFD_RELOC_390_GOTENT,

        /* 64 bit offset to GOT.  */
        BFD_RELOC_390_GOTOFF64,

        /* 12-bit offset to symbol-entry within GOT, with PLT handling.  */
        BFD_RELOC_390_GOTPLT12,

        /* 16-bit offset to symbol-entry within GOT, with PLT handling.  */
        BFD_RELOC_390_GOTPLT16,

        /* 32-bit offset to symbol-entry within GOT, with PLT handling.  */
        BFD_RELOC_390_GOTPLT32,

        /* 64-bit offset to symbol-entry within GOT, with PLT handling.  */
        BFD_RELOC_390_GOTPLT64,

        /* 32-bit rel. offset to symbol-entry within GOT, with PLT handling.  */
        BFD_RELOC_390_GOTPLTENT,

        /* 16-bit rel. offset from the GOT to a PLT entry.  */
        BFD_RELOC_390_PLTOFF16,

        /* 32-bit rel. offset from the GOT to a PLT entry.  */
        BFD_RELOC_390_PLTOFF32,

        /* 64-bit rel. offset from the GOT to a PLT entry.  */
        BFD_RELOC_390_PLTOFF64,

        /* s390 tls relocations.  */
        BFD_RELOC_390_TLS_LOAD,
        BFD_RELOC_390_TLS_GDCALL,
        BFD_RELOC_390_TLS_LDCALL,
        BFD_RELOC_390_TLS_GD32,
        BFD_RELOC_390_TLS_GD64,
        BFD_RELOC_390_TLS_GOTIE12,
        BFD_RELOC_390_TLS_GOTIE32,
        BFD_RELOC_390_TLS_GOTIE64,
        BFD_RELOC_390_TLS_LDM32,
        BFD_RELOC_390_TLS_LDM64,
        BFD_RELOC_390_TLS_IE32,
        BFD_RELOC_390_TLS_IE64,
        BFD_RELOC_390_TLS_IEENT,
        BFD_RELOC_390_TLS_LE32,
        BFD_RELOC_390_TLS_LE64,
        BFD_RELOC_390_TLS_LDO32,
        BFD_RELOC_390_TLS_LDO64,
        BFD_RELOC_390_TLS_DTPMOD,
        BFD_RELOC_390_TLS_DTPOFF,
        BFD_RELOC_390_TLS_TPOFF,

        /* Long displacement extension.  */
        BFD_RELOC_390_20,
        BFD_RELOC_390_GOT20,
        BFD_RELOC_390_GOTPLT20,
        BFD_RELOC_390_TLS_GOTIE20,

        /* STT_GNU_IFUNC relocation.  */
        BFD_RELOC_390_IRELATIVE,

        /* Score relocations
        Low 16 bit for load/store  */
        BFD_RELOC_SCORE_GPREL15,

        /* This is a 24-bit reloc with the right 1 bit assumed to be 0  */
        BFD_RELOC_SCORE_DUMMY2,
        BFD_RELOC_SCORE_JMP,

        /* This is a 19-bit reloc with the right 1 bit assumed to be 0  */
        BFD_RELOC_SCORE_BRANCH,

        /* This is a 32-bit reloc for 48-bit instructions.  */
        BFD_RELOC_SCORE_IMM30,

        /* This is a 32-bit reloc for 48-bit instructions.  */
        BFD_RELOC_SCORE_IMM32,

        /* This is a 11-bit reloc with the right 1 bit assumed to be 0  */
        BFD_RELOC_SCORE16_JMP,

        /* This is a 8-bit reloc with the right 1 bit assumed to be 0  */
        BFD_RELOC_SCORE16_BRANCH,

        /* This is a 9-bit reloc with the right 1 bit assumed to be 0  */
        BFD_RELOC_SCORE_BCMP,

        /* Undocumented Score relocs  */
        BFD_RELOC_SCORE_GOT15,
        BFD_RELOC_SCORE_GOT_LO16,
        BFD_RELOC_SCORE_CALL15,
        BFD_RELOC_SCORE_DUMMY_HI16,

        /* Scenix IP2K - 9-bit register number / data address  */
        BFD_RELOC_IP2K_FR9,

        /* Scenix IP2K - 4-bit register/data bank number  */
        BFD_RELOC_IP2K_BANK,

        /* Scenix IP2K - low 13 bits of instruction word address  */
        BFD_RELOC_IP2K_ADDR16CJP,

        /* Scenix IP2K - high 3 bits of instruction word address  */
        BFD_RELOC_IP2K_PAGE3,

        /* Scenix IP2K - ext/low/high 8 bits of data address  */
        BFD_RELOC_IP2K_LO8DATA,
        BFD_RELOC_IP2K_HI8DATA,
        BFD_RELOC_IP2K_EX8DATA,

        /* Scenix IP2K - low/high 8 bits of instruction word address  */
        BFD_RELOC_IP2K_LO8INSN,
        BFD_RELOC_IP2K_HI8INSN,

        /* Scenix IP2K - even/odd PC modifier to modify snb pcl.0  */
        BFD_RELOC_IP2K_PC_SKIP,

        /* Scenix IP2K - 16 bit word address in text section.  */
        BFD_RELOC_IP2K_TEXT,

        /* Scenix IP2K - 7-bit sp or dp offset  */
        BFD_RELOC_IP2K_FR_OFFSET,

        /* Scenix VPE4K coprocessor - data/insn-space addressing  */
        BFD_RELOC_VPE4KMATH_DATA,
        BFD_RELOC_VPE4KMATH_INSN,

        /* These two relocations are used by the linker to determine which of
        the entries in a C++ virtual function table are actually used.  When
        the --gc-sections option is given, the linker will zero out the entries
        that are not used, so that the code for those functions need not be
        included in the output.

        VTABLE_INHERIT is a zero-space relocation used to describe to the
        linker the inheritance tree of a C++ virtual function table.  The
        relocation's symbol should be the parent class' vtable, and the
        relocation should be located at the child vtable.

        VTABLE_ENTRY is a zero-space relocation that describes the use of a
        virtual function table entry.  The reloc's symbol should refer to the
        table of the class mentioned in the code.  Off of that base, an offset
        describes the entry that is being used.  For Rela hosts, this offset
        is stored in the reloc's addend.  For Rel hosts, we are forced to put
        this offset in the reloc's section offset.  */
        BFD_RELOC_VTABLE_INHERIT,
        BFD_RELOC_VTABLE_ENTRY,

        /* Intel IA64 Relocations.  */
        BFD_RELOC_IA64_IMM14,
        BFD_RELOC_IA64_IMM22,
        BFD_RELOC_IA64_IMM64,
        BFD_RELOC_IA64_DIR32MSB,
        BFD_RELOC_IA64_DIR32LSB,
        BFD_RELOC_IA64_DIR64MSB,
        BFD_RELOC_IA64_DIR64LSB,
        BFD_RELOC_IA64_GPREL22,
        BFD_RELOC_IA64_GPREL64I,
        BFD_RELOC_IA64_GPREL32MSB,
        BFD_RELOC_IA64_GPREL32LSB,
        BFD_RELOC_IA64_GPREL64MSB,
        BFD_RELOC_IA64_GPREL64LSB,
        BFD_RELOC_IA64_LTOFF22,
        BFD_RELOC_IA64_LTOFF64I,
        BFD_RELOC_IA64_PLTOFF22,
        BFD_RELOC_IA64_PLTOFF64I,
        BFD_RELOC_IA64_PLTOFF64MSB,
        BFD_RELOC_IA64_PLTOFF64LSB,
        BFD_RELOC_IA64_FPTR64I,
        BFD_RELOC_IA64_FPTR32MSB,
        BFD_RELOC_IA64_FPTR32LSB,
        BFD_RELOC_IA64_FPTR64MSB,
        BFD_RELOC_IA64_FPTR64LSB,
        BFD_RELOC_IA64_PCREL21B,
        BFD_RELOC_IA64_PCREL21BI,
        BFD_RELOC_IA64_PCREL21M,
        BFD_RELOC_IA64_PCREL21F,
        BFD_RELOC_IA64_PCREL22,
        BFD_RELOC_IA64_PCREL60B,
        BFD_RELOC_IA64_PCREL64I,
        BFD_RELOC_IA64_PCREL32MSB,
        BFD_RELOC_IA64_PCREL32LSB,
        BFD_RELOC_IA64_PCREL64MSB,
        BFD_RELOC_IA64_PCREL64LSB,
        BFD_RELOC_IA64_LTOFF_FPTR22,
        BFD_RELOC_IA64_LTOFF_FPTR64I,
        BFD_RELOC_IA64_LTOFF_FPTR32MSB,
        BFD_RELOC_IA64_LTOFF_FPTR32LSB,
        BFD_RELOC_IA64_LTOFF_FPTR64MSB,
        BFD_RELOC_IA64_LTOFF_FPTR64LSB,
        BFD_RELOC_IA64_SEGREL32MSB,
        BFD_RELOC_IA64_SEGREL32LSB,
        BFD_RELOC_IA64_SEGREL64MSB,
        BFD_RELOC_IA64_SEGREL64LSB,
        BFD_RELOC_IA64_SECREL32MSB,
        BFD_RELOC_IA64_SECREL32LSB,
        BFD_RELOC_IA64_SECREL64MSB,
        BFD_RELOC_IA64_SECREL64LSB,
        BFD_RELOC_IA64_REL32MSB,
        BFD_RELOC_IA64_REL32LSB,
        BFD_RELOC_IA64_REL64MSB,
        BFD_RELOC_IA64_REL64LSB,
        BFD_RELOC_IA64_LTV32MSB,
        BFD_RELOC_IA64_LTV32LSB,
        BFD_RELOC_IA64_LTV64MSB,
        BFD_RELOC_IA64_LTV64LSB,
        BFD_RELOC_IA64_IPLTMSB,
        BFD_RELOC_IA64_IPLTLSB,
        BFD_RELOC_IA64_COPY,
        BFD_RELOC_IA64_LTOFF22X,
        BFD_RELOC_IA64_LDXMOV,
        BFD_RELOC_IA64_TPREL14,
        BFD_RELOC_IA64_TPREL22,
        BFD_RELOC_IA64_TPREL64I,
        BFD_RELOC_IA64_TPREL64MSB,
        BFD_RELOC_IA64_TPREL64LSB,
        BFD_RELOC_IA64_LTOFF_TPREL22,
        BFD_RELOC_IA64_DTPMOD64MSB,
        BFD_RELOC_IA64_DTPMOD64LSB,
        BFD_RELOC_IA64_LTOFF_DTPMOD22,
        BFD_RELOC_IA64_DTPREL14,
        BFD_RELOC_IA64_DTPREL22,
        BFD_RELOC_IA64_DTPREL64I,
        BFD_RELOC_IA64_DTPREL32MSB,
        BFD_RELOC_IA64_DTPREL32LSB,
        BFD_RELOC_IA64_DTPREL64MSB,
        BFD_RELOC_IA64_DTPREL64LSB,
        BFD_RELOC_IA64_LTOFF_DTPREL22,

        /* Motorola 68HC11 reloc.
        This is the 8 bit high part of an absolute address.  */
        BFD_RELOC_M68HC11_HI8,

        /* Motorola 68HC11 reloc.
        This is the 8 bit low part of an absolute address.  */
        BFD_RELOC_M68HC11_LO8,

        /* Motorola 68HC11 reloc.
        This is the 3 bit of a value.  */
        BFD_RELOC_M68HC11_3B,

        /* Motorola 68HC11 reloc.
        This reloc marks the beginning of a jump/call instruction.
        It is used for linker relaxation to correctly identify beginning
        of instruction and change some branches to use PC-relative
        addressing mode.  */
        BFD_RELOC_M68HC11_RL_JUMP,

        /* Motorola 68HC11 reloc.
        This reloc marks a group of several instructions that gcc generates
        and for which the linker relaxation pass can modify and/or remove
        some of them.  */
        BFD_RELOC_M68HC11_RL_GROUP,

        /* Motorola 68HC11 reloc.
        This is the 16-bit lower part of an address.  It is used for 'call'
        instruction to specify the symbol address without any special
        transformation (due to memory bank window).  */
        BFD_RELOC_M68HC11_LO16,

        /* Motorola 68HC11 reloc.
        This is a 8-bit reloc that specifies the page number of an address.
        It is used by 'call' instruction to specify the page number of
        the symbol.  */
        BFD_RELOC_M68HC11_PAGE,

        /* Motorola 68HC11 reloc.
        This is a 24-bit reloc that represents the address with a 16-bit
        value and a 8-bit page number.  The symbol address is transformed
        to follow the 16K memory bank of 68HC12 (seen as mapped in the window).  */
        BFD_RELOC_M68HC11_24,

        /* Motorola 68HC12 reloc.
        This is the 5 bits of a value.  */
        BFD_RELOC_M68HC12_5B,

        /* Freescale XGATE reloc.
        This reloc marks the beginning of a bra/jal instruction.  */
        BFD_RELOC_XGATE_RL_JUMP,

        /* Freescale XGATE reloc.
        This reloc marks a group of several instructions that gcc generates
        and for which the linker relaxation pass can modify and/or remove
        some of them.  */
        BFD_RELOC_XGATE_RL_GROUP,

        /* Freescale XGATE reloc.
        This is the 16-bit lower part of an address.  It is used for the '16-bit'
        instructions.  */
        BFD_RELOC_XGATE_LO16,

        /* Freescale XGATE reloc.  */
        BFD_RELOC_XGATE_GPAGE,

        /* Freescale XGATE reloc.  */
        BFD_RELOC_XGATE_24,

        /* Freescale XGATE reloc.
        This is a 9-bit pc-relative reloc.  */
        BFD_RELOC_XGATE_PCREL_9,

        /* Freescale XGATE reloc.
        This is a 10-bit pc-relative reloc.  */
        BFD_RELOC_XGATE_PCREL_10,

        /* Freescale XGATE reloc.
        This is the 16-bit lower part of an address.  It is used for the '16-bit'
        instructions.  */
        BFD_RELOC_XGATE_IMM8_LO,

        /* Freescale XGATE reloc.
        This is the 16-bit higher part of an address.  It is used for the '16-bit'
        instructions.  */
        BFD_RELOC_XGATE_IMM8_HI,

        /* Freescale XGATE reloc.
        This is a 3-bit pc-relative reloc.  */
        BFD_RELOC_XGATE_IMM3,

        /* Freescale XGATE reloc.
        This is a 4-bit pc-relative reloc.  */
        BFD_RELOC_XGATE_IMM4,

        /* Freescale XGATE reloc.
        This is a 5-bit pc-relative reloc.  */
        BFD_RELOC_XGATE_IMM5,

        /* Motorola 68HC12 reloc.
        This is the 9 bits of a value.  */
        BFD_RELOC_M68HC12_9B,

        /* Motorola 68HC12 reloc.
        This is the 16 bits of a value.  */
        BFD_RELOC_M68HC12_16B,

        /* Motorola 68HC12/XGATE reloc.
        This is a PCREL9 branch.  */
        BFD_RELOC_M68HC12_9_PCREL,

        /* Motorola 68HC12/XGATE reloc.
        This is a PCREL10 branch.  */
        BFD_RELOC_M68HC12_10_PCREL,

        /* Motorola 68HC12/XGATE reloc.
        This is the 8 bit low part of an absolute address and immediately precedes
        a matching HI8XG part.  */
        BFD_RELOC_M68HC12_LO8XG,

        /* Motorola 68HC12/XGATE reloc.
        This is the 8 bit high part of an absolute address and immediately follows
        a matching LO8XG part.  */
        BFD_RELOC_M68HC12_HI8XG,

        /* NS CR16C Relocations.  */
        BFD_RELOC_16C_NUM08,
        BFD_RELOC_16C_NUM08_C,
        BFD_RELOC_16C_NUM16,
        BFD_RELOC_16C_NUM16_C,
        BFD_RELOC_16C_NUM32,
        BFD_RELOC_16C_NUM32_C,
        BFD_RELOC_16C_DISP04,
        BFD_RELOC_16C_DISP04_C,
        BFD_RELOC_16C_DISP08,
        BFD_RELOC_16C_DISP08_C,
        BFD_RELOC_16C_DISP16,
        BFD_RELOC_16C_DISP16_C,
        BFD_RELOC_16C_DISP24,
        BFD_RELOC_16C_DISP24_C,
        BFD_RELOC_16C_DISP24a,
        BFD_RELOC_16C_DISP24a_C,
        BFD_RELOC_16C_REG04,
        BFD_RELOC_16C_REG04_C,
        BFD_RELOC_16C_REG04a,
        BFD_RELOC_16C_REG04a_C,
        BFD_RELOC_16C_REG14,
        BFD_RELOC_16C_REG14_C,
        BFD_RELOC_16C_REG16,
        BFD_RELOC_16C_REG16_C,
        BFD_RELOC_16C_REG20,
        BFD_RELOC_16C_REG20_C,
        BFD_RELOC_16C_ABS20,
        BFD_RELOC_16C_ABS20_C,
        BFD_RELOC_16C_ABS24,
        BFD_RELOC_16C_ABS24_C,
        BFD_RELOC_16C_IMM04,
        BFD_RELOC_16C_IMM04_C,
        BFD_RELOC_16C_IMM16,
        BFD_RELOC_16C_IMM16_C,
        BFD_RELOC_16C_IMM20,
        BFD_RELOC_16C_IMM20_C,
        BFD_RELOC_16C_IMM24,
        BFD_RELOC_16C_IMM24_C,
        BFD_RELOC_16C_IMM32,
        BFD_RELOC_16C_IMM32_C,

        /* NS CR16 Relocations.  */
        BFD_RELOC_CR16_NUM8,
        BFD_RELOC_CR16_NUM16,
        BFD_RELOC_CR16_NUM32,
        BFD_RELOC_CR16_NUM32a,
        BFD_RELOC_CR16_REGREL0,
        BFD_RELOC_CR16_REGREL4,
        BFD_RELOC_CR16_REGREL4a,
        BFD_RELOC_CR16_REGREL14,
        BFD_RELOC_CR16_REGREL14a,
        BFD_RELOC_CR16_REGREL16,
        BFD_RELOC_CR16_REGREL20,
        BFD_RELOC_CR16_REGREL20a,
        BFD_RELOC_CR16_ABS20,
        BFD_RELOC_CR16_ABS24,
        BFD_RELOC_CR16_IMM4,
        BFD_RELOC_CR16_IMM8,
        BFD_RELOC_CR16_IMM16,
        BFD_RELOC_CR16_IMM20,
        BFD_RELOC_CR16_IMM24,
        BFD_RELOC_CR16_IMM32,
        BFD_RELOC_CR16_IMM32a,
        BFD_RELOC_CR16_DISP4,
        BFD_RELOC_CR16_DISP8,
        BFD_RELOC_CR16_DISP16,
        BFD_RELOC_CR16_DISP20,
        BFD_RELOC_CR16_DISP24,
        BFD_RELOC_CR16_DISP24a,
        BFD_RELOC_CR16_SWITCH8,
        BFD_RELOC_CR16_SWITCH16,
        BFD_RELOC_CR16_SWITCH32,
        BFD_RELOC_CR16_GOT_REGREL20,
        BFD_RELOC_CR16_GOTC_REGREL20,
        BFD_RELOC_CR16_GLOB_DAT,

        /* NS CRX Relocations.  */
        BFD_RELOC_CRX_REL4,
        BFD_RELOC_CRX_REL8,
        BFD_RELOC_CRX_REL8_CMP,
        BFD_RELOC_CRX_REL16,
        BFD_RELOC_CRX_REL24,
        BFD_RELOC_CRX_REL32,
        BFD_RELOC_CRX_REGREL12,
        BFD_RELOC_CRX_REGREL22,
        BFD_RELOC_CRX_REGREL28,
        BFD_RELOC_CRX_REGREL32,
        BFD_RELOC_CRX_ABS16,
        BFD_RELOC_CRX_ABS32,
        BFD_RELOC_CRX_NUM8,
        BFD_RELOC_CRX_NUM16,
        BFD_RELOC_CRX_NUM32,
        BFD_RELOC_CRX_IMM16,
        BFD_RELOC_CRX_IMM32,
        BFD_RELOC_CRX_SWITCH8,
        BFD_RELOC_CRX_SWITCH16,
        BFD_RELOC_CRX_SWITCH32,

        /* These relocs are only used within the CRIS assembler.  They are not
        (at present) written to any object files.  */
        BFD_RELOC_CRIS_BDISP8,
        BFD_RELOC_CRIS_UNSIGNED_5,
        BFD_RELOC_CRIS_SIGNED_6,
        BFD_RELOC_CRIS_UNSIGNED_6,
        BFD_RELOC_CRIS_SIGNED_8,
        BFD_RELOC_CRIS_UNSIGNED_8,
        BFD_RELOC_CRIS_SIGNED_16,
        BFD_RELOC_CRIS_UNSIGNED_16,
        BFD_RELOC_CRIS_LAPCQ_OFFSET,
        BFD_RELOC_CRIS_UNSIGNED_4,

        /* Relocs used in ELF shared libraries for CRIS.  */
        BFD_RELOC_CRIS_COPY,
        BFD_RELOC_CRIS_GLOB_DAT,
        BFD_RELOC_CRIS_JUMP_SLOT,
        BFD_RELOC_CRIS_RELATIVE,

        /* 32-bit offset to symbol-entry within GOT.  */
        BFD_RELOC_CRIS_32_GOT,

        /* 16-bit offset to symbol-entry within GOT.  */
        BFD_RELOC_CRIS_16_GOT,

        /* 32-bit offset to symbol-entry within GOT, with PLT handling.  */
        BFD_RELOC_CRIS_32_GOTPLT,

        /* 16-bit offset to symbol-entry within GOT, with PLT handling.  */
        BFD_RELOC_CRIS_16_GOTPLT,

        /* 32-bit offset to symbol, relative to GOT.  */
        BFD_RELOC_CRIS_32_GOTREL,

        /* 32-bit offset to symbol with PLT entry, relative to GOT.  */
        BFD_RELOC_CRIS_32_PLT_GOTREL,

        /* 32-bit offset to symbol with PLT entry, relative to this relocation.  */
        BFD_RELOC_CRIS_32_PLT_PCREL,

        /* Relocs used in TLS code for CRIS.  */
        BFD_RELOC_CRIS_32_GOT_GD,
        BFD_RELOC_CRIS_16_GOT_GD,
        BFD_RELOC_CRIS_32_GD,
        BFD_RELOC_CRIS_DTP,
        BFD_RELOC_CRIS_32_DTPREL,
        BFD_RELOC_CRIS_16_DTPREL,
        BFD_RELOC_CRIS_32_GOT_TPREL,
        BFD_RELOC_CRIS_16_GOT_TPREL,
        BFD_RELOC_CRIS_32_TPREL,
        BFD_RELOC_CRIS_16_TPREL,
        BFD_RELOC_CRIS_DTPMOD,
        BFD_RELOC_CRIS_32_IE,

        /* Intel i860 Relocations.  */
        BFD_RELOC_860_COPY,
        BFD_RELOC_860_GLOB_DAT,
        BFD_RELOC_860_JUMP_SLOT,
        BFD_RELOC_860_RELATIVE,
        BFD_RELOC_860_PC26,
        BFD_RELOC_860_PLT26,
        BFD_RELOC_860_PC16,
        BFD_RELOC_860_LOW0,
        BFD_RELOC_860_SPLIT0,
        BFD_RELOC_860_LOW1,
        BFD_RELOC_860_SPLIT1,
        BFD_RELOC_860_LOW2,
        BFD_RELOC_860_SPLIT2,
        BFD_RELOC_860_LOW3,
        BFD_RELOC_860_LOGOT0,
        BFD_RELOC_860_SPGOT0,
        BFD_RELOC_860_LOGOT1,
        BFD_RELOC_860_SPGOT1,
        BFD_RELOC_860_LOGOTOFF0,
        BFD_RELOC_860_SPGOTOFF0,
        BFD_RELOC_860_LOGOTOFF1,
        BFD_RELOC_860_SPGOTOFF1,
        BFD_RELOC_860_LOGOTOFF2,
        BFD_RELOC_860_LOGOTOFF3,
        BFD_RELOC_860_LOPC,
        BFD_RELOC_860_HIGHADJ,
        BFD_RELOC_860_HAGOT,
        BFD_RELOC_860_HAGOTOFF,
        BFD_RELOC_860_HAPC,
        BFD_RELOC_860_HIGH,
        BFD_RELOC_860_HIGOT,
        BFD_RELOC_860_HIGOTOFF,

        /* OpenRISC Relocations.  */
        BFD_RELOC_OPENRISC_ABS_26,
        BFD_RELOC_OPENRISC_REL_26,

        /* H8 elf Relocations.  */
        BFD_RELOC_H8_DIR16A8,
        BFD_RELOC_H8_DIR16R8,
        BFD_RELOC_H8_DIR24A8,
        BFD_RELOC_H8_DIR24R8,
        BFD_RELOC_H8_DIR32A16,
        BFD_RELOC_H8_DISP32A16,

        /* Sony Xstormy16 Relocations.  */
        BFD_RELOC_XSTORMY16_REL_12,
        BFD_RELOC_XSTORMY16_12,
        BFD_RELOC_XSTORMY16_24,
        BFD_RELOC_XSTORMY16_FPTR16,

        /* Self-describing complex relocations.  */
        BFD_RELOC_RELC,


        /* Infineon Relocations.  */
        BFD_RELOC_XC16X_PAG,
        BFD_RELOC_XC16X_POF,
        BFD_RELOC_XC16X_SEG,
        BFD_RELOC_XC16X_SOF,

        /* Relocations used by VAX ELF.  */
        BFD_RELOC_VAX_GLOB_DAT,
        BFD_RELOC_VAX_JMP_SLOT,
        BFD_RELOC_VAX_RELATIVE,

        /* Morpho MT - 16 bit immediate relocation.  */
        BFD_RELOC_MT_PC16,

        /* Morpho MT - Hi 16 bits of an address.  */
        BFD_RELOC_MT_HI16,

        /* Morpho MT - Low 16 bits of an address.  */
        BFD_RELOC_MT_LO16,

        /* Morpho MT - Used to tell the linker which vtable entries are used.  */
        BFD_RELOC_MT_GNU_VTINHERIT,

        /* Morpho MT - Used to tell the linker which vtable entries are used.  */
        BFD_RELOC_MT_GNU_VTENTRY,

        /* Morpho MT - 8 bit immediate relocation.  */
        BFD_RELOC_MT_PCINSN8,

        /* msp430 specific relocation codes  */
        BFD_RELOC_MSP430_10_PCREL,
        BFD_RELOC_MSP430_16_PCREL,
        BFD_RELOC_MSP430_16,
        BFD_RELOC_MSP430_16_PCREL_BYTE,
        BFD_RELOC_MSP430_16_BYTE,
        BFD_RELOC_MSP430_2X_PCREL,
        BFD_RELOC_MSP430_RL_PCREL,
        BFD_RELOC_MSP430_ABS8,
        BFD_RELOC_MSP430X_PCR20_EXT_SRC,
        BFD_RELOC_MSP430X_PCR20_EXT_DST,
        BFD_RELOC_MSP430X_PCR20_EXT_ODST,
        BFD_RELOC_MSP430X_ABS20_EXT_SRC,
        BFD_RELOC_MSP430X_ABS20_EXT_DST,
        BFD_RELOC_MSP430X_ABS20_EXT_ODST,
        BFD_RELOC_MSP430X_ABS20_ADR_SRC,
        BFD_RELOC_MSP430X_ABS20_ADR_DST,
        BFD_RELOC_MSP430X_PCR16,
        BFD_RELOC_MSP430X_PCR20_CALL,
        BFD_RELOC_MSP430X_ABS16,
        BFD_RELOC_MSP430_ABS_HI16,
        BFD_RELOC_MSP430_PREL31,
        BFD_RELOC_MSP430_SYM_DIFF,

        /* Relocations used by the Altera Nios II core.  */
        BFD_RELOC_NIOS2_S16,
        BFD_RELOC_NIOS2_U16,
        BFD_RELOC_NIOS2_CALL26,
        BFD_RELOC_NIOS2_IMM5,
        BFD_RELOC_NIOS2_CACHE_OPX,
        BFD_RELOC_NIOS2_IMM6,
        BFD_RELOC_NIOS2_IMM8,
        BFD_RELOC_NIOS2_HI16,
        BFD_RELOC_NIOS2_LO16,
        BFD_RELOC_NIOS2_HIADJ16,
        BFD_RELOC_NIOS2_GPREL,
        BFD_RELOC_NIOS2_UJMP,
        BFD_RELOC_NIOS2_CJMP,
        BFD_RELOC_NIOS2_CALLR,
        BFD_RELOC_NIOS2_ALIGN,
        BFD_RELOC_NIOS2_GOT16,
        BFD_RELOC_NIOS2_CALL16,
        BFD_RELOC_NIOS2_GOTOFF_LO,
        BFD_RELOC_NIOS2_GOTOFF_HA,
        BFD_RELOC_NIOS2_PCREL_LO,
        BFD_RELOC_NIOS2_PCREL_HA,
        BFD_RELOC_NIOS2_TLS_GD16,
        BFD_RELOC_NIOS2_TLS_LDM16,
        BFD_RELOC_NIOS2_TLS_LDO16,
        BFD_RELOC_NIOS2_TLS_IE16,
        BFD_RELOC_NIOS2_TLS_LE16,
        BFD_RELOC_NIOS2_TLS_DTPMOD,
        BFD_RELOC_NIOS2_TLS_DTPREL,
        BFD_RELOC_NIOS2_TLS_TPREL,
        BFD_RELOC_NIOS2_COPY,
        BFD_RELOC_NIOS2_GLOB_DAT,
        BFD_RELOC_NIOS2_JUMP_SLOT,
        BFD_RELOC_NIOS2_RELATIVE,
        BFD_RELOC_NIOS2_GOTOFF,

        /* IQ2000 Relocations.  */
        BFD_RELOC_IQ2000_OFFSET_16,
        BFD_RELOC_IQ2000_OFFSET_21,
        BFD_RELOC_IQ2000_UHI16,

        /* Special Xtensa relocation used only by PLT entries in ELF shared
        objects to indicate that the runtime linker should set the value
        to one of its own internal functions or data structures.  */
        BFD_RELOC_XTENSA_RTLD,

        /* Xtensa relocations for ELF shared objects.  */
        BFD_RELOC_XTENSA_GLOB_DAT,
        BFD_RELOC_XTENSA_JMP_SLOT,
        BFD_RELOC_XTENSA_RELATIVE,

        /* Xtensa relocation used in ELF object files for symbols that may require
        PLT entries.  Otherwise, this is just a generic 32-bit relocation.  */
        BFD_RELOC_XTENSA_PLT,

        /* Xtensa relocations to mark the difference of two local symbols.
        These are only needed to support linker relaxation and can be ignored
        when not relaxing.  The field is set to the value of the difference
        assuming no relaxation.  The relocation encodes the position of the
        first symbol so the linker can determine whether to adjust the field
        value.  */
        BFD_RELOC_XTENSA_DIFF8,
        BFD_RELOC_XTENSA_DIFF16,
        BFD_RELOC_XTENSA_DIFF32,

        /* Generic Xtensa relocations for instruction operands.  Only the slot
        number is encoded in the relocation.  The relocation applies to the
        last PC-relative immediate operand, or if there are no PC-relative
        immediates, to the last immediate operand.  */
        BFD_RELOC_XTENSA_SLOT0_OP,
        BFD_RELOC_XTENSA_SLOT1_OP,
        BFD_RELOC_XTENSA_SLOT2_OP,
        BFD_RELOC_XTENSA_SLOT3_OP,
        BFD_RELOC_XTENSA_SLOT4_OP,
        BFD_RELOC_XTENSA_SLOT5_OP,
        BFD_RELOC_XTENSA_SLOT6_OP,
        BFD_RELOC_XTENSA_SLOT7_OP,
        BFD_RELOC_XTENSA_SLOT8_OP,
        BFD_RELOC_XTENSA_SLOT9_OP,
        BFD_RELOC_XTENSA_SLOT10_OP,
        BFD_RELOC_XTENSA_SLOT11_OP,
        BFD_RELOC_XTENSA_SLOT12_OP,
        BFD_RELOC_XTENSA_SLOT13_OP,
        BFD_RELOC_XTENSA_SLOT14_OP,

        /* Alternate Xtensa relocations.  Only the slot is encoded in the
        relocation.  The meaning of these relocations is opcode-specific.  */
        BFD_RELOC_XTENSA_SLOT0_ALT,
        BFD_RELOC_XTENSA_SLOT1_ALT,
        BFD_RELOC_XTENSA_SLOT2_ALT,
        BFD_RELOC_XTENSA_SLOT3_ALT,
        BFD_RELOC_XTENSA_SLOT4_ALT,
        BFD_RELOC_XTENSA_SLOT5_ALT,
        BFD_RELOC_XTENSA_SLOT6_ALT,
        BFD_RELOC_XTENSA_SLOT7_ALT,
        BFD_RELOC_XTENSA_SLOT8_ALT,
        BFD_RELOC_XTENSA_SLOT9_ALT,
        BFD_RELOC_XTENSA_SLOT10_ALT,
        BFD_RELOC_XTENSA_SLOT11_ALT,
        BFD_RELOC_XTENSA_SLOT12_ALT,
        BFD_RELOC_XTENSA_SLOT13_ALT,
        BFD_RELOC_XTENSA_SLOT14_ALT,

        /* Xtensa relocations for backward compatibility.  These have all been
        replaced by BFD_RELOC_XTENSA_SLOT0_OP.  */
        BFD_RELOC_XTENSA_OP0,
        BFD_RELOC_XTENSA_OP1,
        BFD_RELOC_XTENSA_OP2,

        /* Xtensa relocation to mark that the assembler expanded the
        instructions from an original target.  The expansion size is
        encoded in the reloc size.  */
        BFD_RELOC_XTENSA_ASM_EXPAND,

        /* Xtensa relocation to mark that the linker should simplify
        assembler-expanded instructions.  This is commonly used
        internally by the linker after analysis of a
        BFD_RELOC_XTENSA_ASM_EXPAND.  */
        BFD_RELOC_XTENSA_ASM_SIMPLIFY,

        /* Xtensa TLS relocations.  */
        BFD_RELOC_XTENSA_TLSDESC_FN,
        BFD_RELOC_XTENSA_TLSDESC_ARG,
        BFD_RELOC_XTENSA_TLS_DTPOFF,
        BFD_RELOC_XTENSA_TLS_TPOFF,
        BFD_RELOC_XTENSA_TLS_FUNC,
        BFD_RELOC_XTENSA_TLS_ARG,
        BFD_RELOC_XTENSA_TLS_CALL,

        /* 8 bit signed offset in (ix+d) or (iy+d).  */
        BFD_RELOC_Z80_DISP8,

        /* DJNZ offset.  */
        BFD_RELOC_Z8K_DISP7,

        /* CALR offset.  */
        BFD_RELOC_Z8K_CALLR,

        /* 4 bit value.  */
        BFD_RELOC_Z8K_IMM4L,

        /* Lattice Mico32 relocations.  */
        BFD_RELOC_LM32_CALL,
        BFD_RELOC_LM32_BRANCH,
        BFD_RELOC_LM32_16_GOT,
        BFD_RELOC_LM32_GOTOFF_HI16,
        BFD_RELOC_LM32_GOTOFF_LO16,
        BFD_RELOC_LM32_COPY,
        BFD_RELOC_LM32_GLOB_DAT,
        BFD_RELOC_LM32_JMP_SLOT,
        BFD_RELOC_LM32_RELATIVE,

        /* Difference between two section addreses.  Must be followed by a
        BFD_RELOC_MACH_O_PAIR.  */
        BFD_RELOC_MACH_O_SECTDIFF,

        /* Like BFD_RELOC_MACH_O_SECTDIFF but with a local symbol.  */
        BFD_RELOC_MACH_O_LOCAL_SECTDIFF,

        /* Pair of relocation.  Contains the first symbol.  */
        BFD_RELOC_MACH_O_PAIR,

        /* PCREL relocations.  They are marked as branch to create PLT entry if
        required.  */
        BFD_RELOC_MACH_O_X86_64_BRANCH32,
        BFD_RELOC_MACH_O_X86_64_BRANCH8,

        /* Used when referencing a GOT entry.  */
        BFD_RELOC_MACH_O_X86_64_GOT,

        /* Used when loading a GOT entry with movq.  It is specially marked so that
        the linker could optimize the movq to a leaq if possible.  */
        BFD_RELOC_MACH_O_X86_64_GOT_LOAD,

        /* Symbol will be substracted.  Must be followed by a BFD_RELOC_64.  */
        BFD_RELOC_MACH_O_X86_64_SUBTRACTOR32,

        /* Symbol will be substracted.  Must be followed by a BFD_RELOC_64.  */
        BFD_RELOC_MACH_O_X86_64_SUBTRACTOR64,

        /* Same as BFD_RELOC_32_PCREL but with an implicit -1 addend.  */
        BFD_RELOC_MACH_O_X86_64_PCREL32_1,

        /* Same as BFD_RELOC_32_PCREL but with an implicit -2 addend.  */
        BFD_RELOC_MACH_O_X86_64_PCREL32_2,

        /* Same as BFD_RELOC_32_PCREL but with an implicit -4 addend.  */
        BFD_RELOC_MACH_O_X86_64_PCREL32_4,

        /* This is a 32 bit reloc for the microblaze that stores the
        low 16 bits of a value  */
        BFD_RELOC_MICROBLAZE_32_LO,

        /* This is a 32 bit pc-relative reloc for the microblaze that
        stores the low 16 bits of a value  */
        BFD_RELOC_MICROBLAZE_32_LO_PCREL,

        /* This is a 32 bit reloc for the microblaze that stores a
        value relative to the read-only small data area anchor  */
        BFD_RELOC_MICROBLAZE_32_ROSDA,

        /* This is a 32 bit reloc for the microblaze that stores a
        value relative to the read-write small data area anchor  */
        BFD_RELOC_MICROBLAZE_32_RWSDA,

        /* This is a 32 bit reloc for the microblaze to handle
        expressions of the form "Symbol Op Symbol"  */
        BFD_RELOC_MICROBLAZE_32_SYM_OP_SYM,

        /* This is a 64 bit reloc that stores the 32 bit pc relative
        value in two words (with an imm instruction).  No relocation is
        done here - only used for relaxing  */
        BFD_RELOC_MICROBLAZE_64_NONE,

        /* This is a 64 bit reloc that stores the 32 bit pc relative
        value in two words (with an imm instruction).  The relocation is
        PC-relative GOT offset  */
        BFD_RELOC_MICROBLAZE_64_GOTPC,

        /* This is a 64 bit reloc that stores the 32 bit pc relative
        value in two words (with an imm instruction).  The relocation is
        GOT offset  */
        BFD_RELOC_MICROBLAZE_64_GOT,

        /* This is a 64 bit reloc that stores the 32 bit pc relative
        value in two words (with an imm instruction).  The relocation is
        PC-relative offset into PLT  */
        BFD_RELOC_MICROBLAZE_64_PLT,

        /* This is a 64 bit reloc that stores the 32 bit GOT relative
        value in two words (with an imm instruction).  The relocation is
        relative offset from _GLOBAL_OFFSET_TABLE_  */
        BFD_RELOC_MICROBLAZE_64_GOTOFF,

        /* This is a 32 bit reloc that stores the 32 bit GOT relative
        value in a word.  The relocation is relative offset from  */
        BFD_RELOC_MICROBLAZE_32_GOTOFF,

        /* This is used to tell the dynamic linker to copy the value out of
        the dynamic object into the runtime process image.  */
        BFD_RELOC_MICROBLAZE_COPY,

        /* Unused Reloc  */
        BFD_RELOC_MICROBLAZE_64_TLS,

        /* This is a 64 bit reloc that stores the 32 bit GOT relative value
        of the GOT TLS GD info entry in two words (with an imm instruction). The
        relocation is GOT offset.  */
        BFD_RELOC_MICROBLAZE_64_TLSGD,

        /* This is a 64 bit reloc that stores the 32 bit GOT relative value
        of the GOT TLS LD info entry in two words (with an imm instruction). The
        relocation is GOT offset.  */
        BFD_RELOC_MICROBLAZE_64_TLSLD,

        /* This is a 32 bit reloc that stores the Module ID to GOT(n).  */
        BFD_RELOC_MICROBLAZE_32_TLSDTPMOD,

        /* This is a 32 bit reloc that stores TLS offset to GOT(n+1).  */
        BFD_RELOC_MICROBLAZE_32_TLSDTPREL,

        /* This is a 32 bit reloc for storing TLS offset to two words (uses imm
        instruction)  */
        BFD_RELOC_MICROBLAZE_64_TLSDTPREL,

        /* This is a 64 bit reloc that stores 32-bit thread pointer relative offset
        to two words (uses imm instruction).  */
        BFD_RELOC_MICROBLAZE_64_TLSGOTTPREL,

        /* This is a 64 bit reloc that stores 32-bit thread pointer relative offset
        to two words (uses imm instruction).  */
        BFD_RELOC_MICROBLAZE_64_TLSTPREL,

        /* AArch64 pseudo relocation code to mark the start of the AArch64
        relocation enumerators.  N.B. the order of the enumerators is
        important as several tables in the AArch64 bfd backend are indexed
        by these enumerators; make sure they are all synced.  */
        BFD_RELOC_AARCH64_RELOC_START,

        /* AArch64 null relocation code.  */
        BFD_RELOC_AARCH64_NONE,

        /* Basic absolute relocations of N bits.  These are equivalent to
        BFD_RELOC_N and they were added to assist the indexing of the howto
        table.  */
        BFD_RELOC_AARCH64_64,
        BFD_RELOC_AARCH64_32,
        BFD_RELOC_AARCH64_16,

        /* PC-relative relocations.  These are equivalent to BFD_RELOC_N_PCREL
        and they were added to assist the indexing of the howto table.  */
        BFD_RELOC_AARCH64_64_PCREL,
        BFD_RELOC_AARCH64_32_PCREL,
        BFD_RELOC_AARCH64_16_PCREL,

        /* AArch64 MOV[NZK] instruction with most significant bits 0 to 15
        of an unsigned address/value.  */
        BFD_RELOC_AARCH64_MOVW_G0,

        /* AArch64 MOV[NZK] instruction with less significant bits 0 to 15 of
        an address/value.  No overflow checking.  */
        BFD_RELOC_AARCH64_MOVW_G0_NC,

        /* AArch64 MOV[NZK] instruction with most significant bits 16 to 31
        of an unsigned address/value.  */
        BFD_RELOC_AARCH64_MOVW_G1,

        /* AArch64 MOV[NZK] instruction with less significant bits 16 to 31
        of an address/value.  No overflow checking.  */
        BFD_RELOC_AARCH64_MOVW_G1_NC,

        /* AArch64 MOV[NZK] instruction with most significant bits 32 to 47
        of an unsigned address/value.  */
        BFD_RELOC_AARCH64_MOVW_G2,

        /* AArch64 MOV[NZK] instruction with less significant bits 32 to 47
        of an address/value.  No overflow checking.  */
        BFD_RELOC_AARCH64_MOVW_G2_NC,

        /* AArch64 MOV[NZK] instruction with most signficant bits 48 to 64
        of a signed or unsigned address/value.  */
        BFD_RELOC_AARCH64_MOVW_G3,

        /* AArch64 MOV[NZ] instruction with most significant bits 0 to 15
        of a signed value.  Changes instruction to MOVZ or MOVN depending on the
        value's sign.  */
        BFD_RELOC_AARCH64_MOVW_G0_S,

        /* AArch64 MOV[NZ] instruction with most significant bits 16 to 31
        of a signed value.  Changes instruction to MOVZ or MOVN depending on the
        value's sign.  */
        BFD_RELOC_AARCH64_MOVW_G1_S,

        /* AArch64 MOV[NZ] instruction with most significant bits 32 to 47
        of a signed value.  Changes instruction to MOVZ or MOVN depending on the
        value's sign.  */
        BFD_RELOC_AARCH64_MOVW_G2_S,

        /* AArch64 Load Literal instruction, holding a 19 bit pc-relative word
        offset.  The lowest two bits must be zero and are not stored in the
        instruction, giving a 21 bit signed byte offset.  */
        BFD_RELOC_AARCH64_LD_LO19_PCREL,

        /* AArch64 ADR instruction, holding a simple 21 bit pc-relative byte offset.  */
        BFD_RELOC_AARCH64_ADR_LO21_PCREL,

        /* AArch64 ADRP instruction, with bits 12 to 32 of a pc-relative page
        offset, giving a 4KB aligned page base address.  */
        BFD_RELOC_AARCH64_ADR_HI21_PCREL,

        /* AArch64 ADRP instruction, with bits 12 to 32 of a pc-relative page
        offset, giving a 4KB aligned page base address, but with no overflow
        checking.  */
        BFD_RELOC_AARCH64_ADR_HI21_NC_PCREL,

        /* AArch64 ADD immediate instruction, holding bits 0 to 11 of the address.
        Used in conjunction with BFD_RELOC_AARCH64_ADR_HI21_PCREL.  */
        BFD_RELOC_AARCH64_ADD_LO12,

        /* AArch64 8-bit load/store instruction, holding bits 0 to 11 of the
        address.  Used in conjunction with BFD_RELOC_AARCH64_ADR_HI21_PCREL.  */
        BFD_RELOC_AARCH64_LDST8_LO12,

        /* AArch64 14 bit pc-relative test bit and branch.
        The lowest two bits must be zero and are not stored in the instruction,
        giving a 16 bit signed byte offset.  */
        BFD_RELOC_AARCH64_TSTBR14,

        /* AArch64 19 bit pc-relative conditional branch and compare & branch.
        The lowest two bits must be zero and are not stored in the instruction,
        giving a 21 bit signed byte offset.  */
        BFD_RELOC_AARCH64_BRANCH19,

        /* AArch64 26 bit pc-relative unconditional branch.
        The lowest two bits must be zero and are not stored in the instruction,
        giving a 28 bit signed byte offset.  */
        BFD_RELOC_AARCH64_JUMP26,

        /* AArch64 26 bit pc-relative unconditional branch and link.
        The lowest two bits must be zero and are not stored in the instruction,
        giving a 28 bit signed byte offset.  */
        BFD_RELOC_AARCH64_CALL26,

        /* AArch64 16-bit load/store instruction, holding bits 0 to 11 of the
        address.  Used in conjunction with BFD_RELOC_AARCH64_ADR_HI21_PCREL.  */
        BFD_RELOC_AARCH64_LDST16_LO12,

        /* AArch64 32-bit load/store instruction, holding bits 0 to 11 of the
        address.  Used in conjunction with BFD_RELOC_AARCH64_ADR_HI21_PCREL.  */
        BFD_RELOC_AARCH64_LDST32_LO12,

        /* AArch64 64-bit load/store instruction, holding bits 0 to 11 of the
        address.  Used in conjunction with BFD_RELOC_AARCH64_ADR_HI21_PCREL.  */
        BFD_RELOC_AARCH64_LDST64_LO12,

        /* AArch64 128-bit load/store instruction, holding bits 0 to 11 of the
        address.  Used in conjunction with BFD_RELOC_AARCH64_ADR_HI21_PCREL.  */
        BFD_RELOC_AARCH64_LDST128_LO12,

        /* AArch64 Load Literal instruction, holding a 19 bit PC relative word
        offset of the global offset table entry for a symbol.  The lowest two
        bits must be zero and are not stored in the instruction, giving a 21
        bit signed byte offset.  This relocation type requires signed overflow
        checking.  */
        BFD_RELOC_AARCH64_GOT_LD_PREL19,

        /* Get to the page base of the global offset table entry for a symbol as
        part of an ADRP instruction using a 21 bit PC relative value.Used in
        conjunction with BFD_RELOC_AARCH64_LD64_GOT_LO12_NC.  */
        BFD_RELOC_AARCH64_ADR_GOT_PAGE,

        /* Unsigned 12 bit byte offset for 64 bit load/store from the page of
        the GOT entry for this symbol.  Used in conjunction with
        BFD_RELOC_AARCH64_ADR_GOTPAGE.  Valid in LP64 ABI only.  */
        BFD_RELOC_AARCH64_LD64_GOT_LO12_NC,

        /* Unsigned 12 bit byte offset for 32 bit load/store from the page of
        the GOT entry for this symbol.  Used in conjunction with
        BFD_RELOC_AARCH64_ADR_GOTPAGE.  Valid in ILP32 ABI only.  */
        BFD_RELOC_AARCH64_LD32_GOT_LO12_NC,

        /* Get to the page base of the global offset table entry for a symbols
        tls_index structure as part of an adrp instruction using a 21 bit PC
        relative value.  Used in conjunction with
        BFD_RELOC_AARCH64_TLSGD_ADD_LO12_NC.  */
        BFD_RELOC_AARCH64_TLSGD_ADR_PAGE21,

        /* Unsigned 12 bit byte offset to global offset table entry for a symbols
        tls_index structure.  Used in conjunction with
        BFD_RELOC_AARCH64_TLSGD_ADR_PAGE21.  */
        BFD_RELOC_AARCH64_TLSGD_ADD_LO12_NC,

        /* AArch64 TLS INITIAL EXEC relocation.  */
        BFD_RELOC_AARCH64_TLSIE_MOVW_GOTTPREL_G1,

        /* AArch64 TLS INITIAL EXEC relocation.  */
        BFD_RELOC_AARCH64_TLSIE_MOVW_GOTTPREL_G0_NC,

        /* AArch64 TLS INITIAL EXEC relocation.  */
        BFD_RELOC_AARCH64_TLSIE_ADR_GOTTPREL_PAGE21,

        /* AArch64 TLS INITIAL EXEC relocation.  */
        BFD_RELOC_AARCH64_TLSIE_LD64_GOTTPREL_LO12_NC,

        /* AArch64 TLS INITIAL EXEC relocation.  */
        BFD_RELOC_AARCH64_TLSIE_LD32_GOTTPREL_LO12_NC,

        /* AArch64 TLS INITIAL EXEC relocation.  */
        BFD_RELOC_AARCH64_TLSIE_LD_GOTTPREL_PREL19,

        /* AArch64 TLS LOCAL EXEC relocation.  */
        BFD_RELOC_AARCH64_TLSLE_MOVW_TPREL_G2,

        /* AArch64 TLS LOCAL EXEC relocation.  */
        BFD_RELOC_AARCH64_TLSLE_MOVW_TPREL_G1,

        /* AArch64 TLS LOCAL EXEC relocation.  */
        BFD_RELOC_AARCH64_TLSLE_MOVW_TPREL_G1_NC,

        /* AArch64 TLS LOCAL EXEC relocation.  */
        BFD_RELOC_AARCH64_TLSLE_MOVW_TPREL_G0,

        /* AArch64 TLS LOCAL EXEC relocation.  */
        BFD_RELOC_AARCH64_TLSLE_MOVW_TPREL_G0_NC,

        /* AArch64 TLS LOCAL EXEC relocation.  */
        BFD_RELOC_AARCH64_TLSLE_ADD_TPREL_HI12,

        /* AArch64 TLS LOCAL EXEC relocation.  */
        BFD_RELOC_AARCH64_TLSLE_ADD_TPREL_LO12,

        /* AArch64 TLS LOCAL EXEC relocation.  */
        BFD_RELOC_AARCH64_TLSLE_ADD_TPREL_LO12_NC,

        /* AArch64 TLS DESC relocation.  */
        BFD_RELOC_AARCH64_TLSDESC_LD_PREL19,

        /* AArch64 TLS DESC relocation.  */
        BFD_RELOC_AARCH64_TLSDESC_ADR_PREL21,

        /* AArch64 TLS DESC relocation.  */
        BFD_RELOC_AARCH64_TLSDESC_ADR_PAGE21,

        /* AArch64 TLS DESC relocation.  */
        BFD_RELOC_AARCH64_TLSDESC_LD64_LO12_NC,

        /* AArch64 TLS DESC relocation.  */
        BFD_RELOC_AARCH64_TLSDESC_LD32_LO12_NC,

        /* AArch64 TLS DESC relocation.  */
        BFD_RELOC_AARCH64_TLSDESC_ADD_LO12_NC,

        /* AArch64 TLS DESC relocation.  */
        BFD_RELOC_AARCH64_TLSDESC_OFF_G1,

        /* AArch64 TLS DESC relocation.  */
        BFD_RELOC_AARCH64_TLSDESC_OFF_G0_NC,

        /* AArch64 TLS DESC relocation.  */
        BFD_RELOC_AARCH64_TLSDESC_LDR,

        /* AArch64 TLS DESC relocation.  */
        BFD_RELOC_AARCH64_TLSDESC_ADD,

        /* AArch64 TLS DESC relocation.  */
        BFD_RELOC_AARCH64_TLSDESC_CALL,

        /* AArch64 TLS relocation.  */
        BFD_RELOC_AARCH64_COPY,

        /* AArch64 TLS relocation.  */
        BFD_RELOC_AARCH64_GLOB_DAT,

        /* AArch64 TLS relocation.  */
        BFD_RELOC_AARCH64_JUMP_SLOT,

        /* AArch64 TLS relocation.  */
        BFD_RELOC_AARCH64_RELATIVE,

        /* AArch64 TLS relocation.  */
        BFD_RELOC_AARCH64_TLS_DTPMOD,

        /* AArch64 TLS relocation.  */
        BFD_RELOC_AARCH64_TLS_DTPREL,

        /* AArch64 TLS relocation.  */
        BFD_RELOC_AARCH64_TLS_TPREL,

        /* AArch64 TLS relocation.  */
        BFD_RELOC_AARCH64_TLSDESC,

        /* AArch64 support for STT_GNU_IFUNC.  */
        BFD_RELOC_AARCH64_IRELATIVE,

        /* AArch64 pseudo relocation code to mark the end of the AArch64
        relocation enumerators that have direct mapping to ELF reloc codes.
        There are a few more enumerators after this one; those are mainly
        used by the AArch64 assembler for the internal fixup or to select
        one of the above enumerators.  */
        BFD_RELOC_AARCH64_RELOC_END,

        /* AArch64 pseudo relocation code to be used internally by the AArch64
        assembler and not (currently) written to any object files.  */
        BFD_RELOC_AARCH64_GAS_INTERNAL_FIXUP,

        /* AArch64 unspecified load/store instruction, holding bits 0 to 11 of the
        address.  Used in conjunction with BFD_RELOC_AARCH64_ADR_HI21_PCREL.  */
        BFD_RELOC_AARCH64_LDST_LO12,

        /* AArch64 pseudo relocation code to be used internally by the AArch64
        assembler and not (currently) written to any object files.  */
        BFD_RELOC_AARCH64_LD_GOT_LO12_NC,

        /* AArch64 pseudo relocation code to be used internally by the AArch64
        assembler and not (currently) written to any object files.  */
        BFD_RELOC_AARCH64_TLSIE_LD_GOTTPREL_LO12_NC,

        /* AArch64 pseudo relocation code to be used internally by the AArch64
        assembler and not (currently) written to any object files.  */
        BFD_RELOC_AARCH64_TLSDESC_LD_LO12_NC,

        /* Tilera TILEPro Relocations.  */
        BFD_RELOC_TILEPRO_COPY,
        BFD_RELOC_TILEPRO_GLOB_DAT,
        BFD_RELOC_TILEPRO_JMP_SLOT,
        BFD_RELOC_TILEPRO_RELATIVE,
        BFD_RELOC_TILEPRO_BROFF_X1,
        BFD_RELOC_TILEPRO_JOFFLONG_X1,
        BFD_RELOC_TILEPRO_JOFFLONG_X1_PLT,
        BFD_RELOC_TILEPRO_IMM8_X0,
        BFD_RELOC_TILEPRO_IMM8_Y0,
        BFD_RELOC_TILEPRO_IMM8_X1,
        BFD_RELOC_TILEPRO_IMM8_Y1,
        BFD_RELOC_TILEPRO_DEST_IMM8_X1,
        BFD_RELOC_TILEPRO_MT_IMM15_X1,
        BFD_RELOC_TILEPRO_MF_IMM15_X1,
        BFD_RELOC_TILEPRO_IMM16_X0,
        BFD_RELOC_TILEPRO_IMM16_X1,
        BFD_RELOC_TILEPRO_IMM16_X0_LO,
        BFD_RELOC_TILEPRO_IMM16_X1_LO,
        BFD_RELOC_TILEPRO_IMM16_X0_HI,
        BFD_RELOC_TILEPRO_IMM16_X1_HI,
        BFD_RELOC_TILEPRO_IMM16_X0_HA,
        BFD_RELOC_TILEPRO_IMM16_X1_HA,
        BFD_RELOC_TILEPRO_IMM16_X0_PCREL,
        BFD_RELOC_TILEPRO_IMM16_X1_PCREL,
        BFD_RELOC_TILEPRO_IMM16_X0_LO_PCREL,
        BFD_RELOC_TILEPRO_IMM16_X1_LO_PCREL,
        BFD_RELOC_TILEPRO_IMM16_X0_HI_PCREL,
        BFD_RELOC_TILEPRO_IMM16_X1_HI_PCREL,
        BFD_RELOC_TILEPRO_IMM16_X0_HA_PCREL,
        BFD_RELOC_TILEPRO_IMM16_X1_HA_PCREL,
        BFD_RELOC_TILEPRO_IMM16_X0_GOT,
        BFD_RELOC_TILEPRO_IMM16_X1_GOT,
        BFD_RELOC_TILEPRO_IMM16_X0_GOT_LO,
        BFD_RELOC_TILEPRO_IMM16_X1_GOT_LO,
        BFD_RELOC_TILEPRO_IMM16_X0_GOT_HI,
        BFD_RELOC_TILEPRO_IMM16_X1_GOT_HI,
        BFD_RELOC_TILEPRO_IMM16_X0_GOT_HA,
        BFD_RELOC_TILEPRO_IMM16_X1_GOT_HA,
        BFD_RELOC_TILEPRO_MMSTART_X0,
        BFD_RELOC_TILEPRO_MMEND_X0,
        BFD_RELOC_TILEPRO_MMSTART_X1,
        BFD_RELOC_TILEPRO_MMEND_X1,
        BFD_RELOC_TILEPRO_SHAMT_X0,
        BFD_RELOC_TILEPRO_SHAMT_X1,
        BFD_RELOC_TILEPRO_SHAMT_Y0,
        BFD_RELOC_TILEPRO_SHAMT_Y1,
        BFD_RELOC_TILEPRO_TLS_GD_CALL,
        BFD_RELOC_TILEPRO_IMM8_X0_TLS_GD_ADD,
        BFD_RELOC_TILEPRO_IMM8_X1_TLS_GD_ADD,
        BFD_RELOC_TILEPRO_IMM8_Y0_TLS_GD_ADD,
        BFD_RELOC_TILEPRO_IMM8_Y1_TLS_GD_ADD,
        BFD_RELOC_TILEPRO_TLS_IE_LOAD,
        BFD_RELOC_TILEPRO_IMM16_X0_TLS_GD,
        BFD_RELOC_TILEPRO_IMM16_X1_TLS_GD,
        BFD_RELOC_TILEPRO_IMM16_X0_TLS_GD_LO,
        BFD_RELOC_TILEPRO_IMM16_X1_TLS_GD_LO,
        BFD_RELOC_TILEPRO_IMM16_X0_TLS_GD_HI,
        BFD_RELOC_TILEPRO_IMM16_X1_TLS_GD_HI,
        BFD_RELOC_TILEPRO_IMM16_X0_TLS_GD_HA,
        BFD_RELOC_TILEPRO_IMM16_X1_TLS_GD_HA,
        BFD_RELOC_TILEPRO_IMM16_X0_TLS_IE,
        BFD_RELOC_TILEPRO_IMM16_X1_TLS_IE,
        BFD_RELOC_TILEPRO_IMM16_X0_TLS_IE_LO,
        BFD_RELOC_TILEPRO_IMM16_X1_TLS_IE_LO,
        BFD_RELOC_TILEPRO_IMM16_X0_TLS_IE_HI,
        BFD_RELOC_TILEPRO_IMM16_X1_TLS_IE_HI,
        BFD_RELOC_TILEPRO_IMM16_X0_TLS_IE_HA,
        BFD_RELOC_TILEPRO_IMM16_X1_TLS_IE_HA,
        BFD_RELOC_TILEPRO_TLS_DTPMOD32,
        BFD_RELOC_TILEPRO_TLS_DTPOFF32,
        BFD_RELOC_TILEPRO_TLS_TPOFF32,
        BFD_RELOC_TILEPRO_IMM16_X0_TLS_LE,
        BFD_RELOC_TILEPRO_IMM16_X1_TLS_LE,
        BFD_RELOC_TILEPRO_IMM16_X0_TLS_LE_LO,
        BFD_RELOC_TILEPRO_IMM16_X1_TLS_LE_LO,
        BFD_RELOC_TILEPRO_IMM16_X0_TLS_LE_HI,
        BFD_RELOC_TILEPRO_IMM16_X1_TLS_LE_HI,
        BFD_RELOC_TILEPRO_IMM16_X0_TLS_LE_HA,
        BFD_RELOC_TILEPRO_IMM16_X1_TLS_LE_HA,

        /* Tilera TILE-Gx Relocations.  */
        BFD_RELOC_TILEGX_HW0,
        BFD_RELOC_TILEGX_HW1,
        BFD_RELOC_TILEGX_HW2,
        BFD_RELOC_TILEGX_HW3,
        BFD_RELOC_TILEGX_HW0_LAST,
        BFD_RELOC_TILEGX_HW1_LAST,
        BFD_RELOC_TILEGX_HW2_LAST,
        BFD_RELOC_TILEGX_COPY,
        BFD_RELOC_TILEGX_GLOB_DAT,
        BFD_RELOC_TILEGX_JMP_SLOT,
        BFD_RELOC_TILEGX_RELATIVE,
        BFD_RELOC_TILEGX_BROFF_X1,
        BFD_RELOC_TILEGX_JUMPOFF_X1,
        BFD_RELOC_TILEGX_JUMPOFF_X1_PLT,
        BFD_RELOC_TILEGX_IMM8_X0,
        BFD_RELOC_TILEGX_IMM8_Y0,
        BFD_RELOC_TILEGX_IMM8_X1,
        BFD_RELOC_TILEGX_IMM8_Y1,
        BFD_RELOC_TILEGX_DEST_IMM8_X1,
        BFD_RELOC_TILEGX_MT_IMM14_X1,
        BFD_RELOC_TILEGX_MF_IMM14_X1,
        BFD_RELOC_TILEGX_MMSTART_X0,
        BFD_RELOC_TILEGX_MMEND_X0,
        BFD_RELOC_TILEGX_SHAMT_X0,
        BFD_RELOC_TILEGX_SHAMT_X1,
        BFD_RELOC_TILEGX_SHAMT_Y0,
        BFD_RELOC_TILEGX_SHAMT_Y1,
        BFD_RELOC_TILEGX_IMM16_X0_HW0,
        BFD_RELOC_TILEGX_IMM16_X1_HW0,
        BFD_RELOC_TILEGX_IMM16_X0_HW1,
        BFD_RELOC_TILEGX_IMM16_X1_HW1,
        BFD_RELOC_TILEGX_IMM16_X0_HW2,
        BFD_RELOC_TILEGX_IMM16_X1_HW2,
        BFD_RELOC_TILEGX_IMM16_X0_HW3,
        BFD_RELOC_TILEGX_IMM16_X1_HW3,
        BFD_RELOC_TILEGX_IMM16_X0_HW0_LAST,
        BFD_RELOC_TILEGX_IMM16_X1_HW0_LAST,
        BFD_RELOC_TILEGX_IMM16_X0_HW1_LAST,
        BFD_RELOC_TILEGX_IMM16_X1_HW1_LAST,
        BFD_RELOC_TILEGX_IMM16_X0_HW2_LAST,
        BFD_RELOC_TILEGX_IMM16_X1_HW2_LAST,
        BFD_RELOC_TILEGX_IMM16_X0_HW0_PCREL,
        BFD_RELOC_TILEGX_IMM16_X1_HW0_PCREL,
        BFD_RELOC_TILEGX_IMM16_X0_HW1_PCREL,
        BFD_RELOC_TILEGX_IMM16_X1_HW1_PCREL,
        BFD_RELOC_TILEGX_IMM16_X0_HW2_PCREL,
        BFD_RELOC_TILEGX_IMM16_X1_HW2_PCREL,
        BFD_RELOC_TILEGX_IMM16_X0_HW3_PCREL,
        BFD_RELOC_TILEGX_IMM16_X1_HW3_PCREL,
        BFD_RELOC_TILEGX_IMM16_X0_HW0_LAST_PCREL,
        BFD_RELOC_TILEGX_IMM16_X1_HW0_LAST_PCREL,
        BFD_RELOC_TILEGX_IMM16_X0_HW1_LAST_PCREL,
        BFD_RELOC_TILEGX_IMM16_X1_HW1_LAST_PCREL,
        BFD_RELOC_TILEGX_IMM16_X0_HW2_LAST_PCREL,
        BFD_RELOC_TILEGX_IMM16_X1_HW2_LAST_PCREL,
        BFD_RELOC_TILEGX_IMM16_X0_HW0_GOT,
        BFD_RELOC_TILEGX_IMM16_X1_HW0_GOT,
        BFD_RELOC_TILEGX_IMM16_X0_HW0_PLT_PCREL,
        BFD_RELOC_TILEGX_IMM16_X1_HW0_PLT_PCREL,
        BFD_RELOC_TILEGX_IMM16_X0_HW1_PLT_PCREL,
        BFD_RELOC_TILEGX_IMM16_X1_HW1_PLT_PCREL,
        BFD_RELOC_TILEGX_IMM16_X0_HW2_PLT_PCREL,
        BFD_RELOC_TILEGX_IMM16_X1_HW2_PLT_PCREL,
        BFD_RELOC_TILEGX_IMM16_X0_HW0_LAST_GOT,
        BFD_RELOC_TILEGX_IMM16_X1_HW0_LAST_GOT,
        BFD_RELOC_TILEGX_IMM16_X0_HW1_LAST_GOT,
        BFD_RELOC_TILEGX_IMM16_X1_HW1_LAST_GOT,
        BFD_RELOC_TILEGX_IMM16_X0_HW3_PLT_PCREL,
        BFD_RELOC_TILEGX_IMM16_X1_HW3_PLT_PCREL,
        BFD_RELOC_TILEGX_IMM16_X0_HW0_TLS_GD,
        BFD_RELOC_TILEGX_IMM16_X1_HW0_TLS_GD,
        BFD_RELOC_TILEGX_IMM16_X0_HW0_TLS_LE,
        BFD_RELOC_TILEGX_IMM16_X1_HW0_TLS_LE,
        BFD_RELOC_TILEGX_IMM16_X0_HW0_LAST_TLS_LE,
        BFD_RELOC_TILEGX_IMM16_X1_HW0_LAST_TLS_LE,
        BFD_RELOC_TILEGX_IMM16_X0_HW1_LAST_TLS_LE,
        BFD_RELOC_TILEGX_IMM16_X1_HW1_LAST_TLS_LE,
        BFD_RELOC_TILEGX_IMM16_X0_HW0_LAST_TLS_GD,
        BFD_RELOC_TILEGX_IMM16_X1_HW0_LAST_TLS_GD,
        BFD_RELOC_TILEGX_IMM16_X0_HW1_LAST_TLS_GD,
        BFD_RELOC_TILEGX_IMM16_X1_HW1_LAST_TLS_GD,
        BFD_RELOC_TILEGX_IMM16_X0_HW0_TLS_IE,
        BFD_RELOC_TILEGX_IMM16_X1_HW0_TLS_IE,
        BFD_RELOC_TILEGX_IMM16_X0_HW0_LAST_PLT_PCREL,
        BFD_RELOC_TILEGX_IMM16_X1_HW0_LAST_PLT_PCREL,
        BFD_RELOC_TILEGX_IMM16_X0_HW1_LAST_PLT_PCREL,
        BFD_RELOC_TILEGX_IMM16_X1_HW1_LAST_PLT_PCREL,
        BFD_RELOC_TILEGX_IMM16_X0_HW2_LAST_PLT_PCREL,
        BFD_RELOC_TILEGX_IMM16_X1_HW2_LAST_PLT_PCREL,
        BFD_RELOC_TILEGX_IMM16_X0_HW0_LAST_TLS_IE,
        BFD_RELOC_TILEGX_IMM16_X1_HW0_LAST_TLS_IE,
        BFD_RELOC_TILEGX_IMM16_X0_HW1_LAST_TLS_IE,
        BFD_RELOC_TILEGX_IMM16_X1_HW1_LAST_TLS_IE,
        BFD_RELOC_TILEGX_TLS_DTPMOD64,
        BFD_RELOC_TILEGX_TLS_DTPOFF64,
        BFD_RELOC_TILEGX_TLS_TPOFF64,
        BFD_RELOC_TILEGX_TLS_DTPMOD32,
        BFD_RELOC_TILEGX_TLS_DTPOFF32,
        BFD_RELOC_TILEGX_TLS_TPOFF32,
        BFD_RELOC_TILEGX_TLS_GD_CALL,
        BFD_RELOC_TILEGX_IMM8_X0_TLS_GD_ADD,
        BFD_RELOC_TILEGX_IMM8_X1_TLS_GD_ADD,
        BFD_RELOC_TILEGX_IMM8_Y0_TLS_GD_ADD,
        BFD_RELOC_TILEGX_IMM8_Y1_TLS_GD_ADD,
        BFD_RELOC_TILEGX_TLS_IE_LOAD,
        BFD_RELOC_TILEGX_IMM8_X0_TLS_ADD,
        BFD_RELOC_TILEGX_IMM8_X1_TLS_ADD,
        BFD_RELOC_TILEGX_IMM8_Y0_TLS_ADD,
        BFD_RELOC_TILEGX_IMM8_Y1_TLS_ADD,

        /* Adapteva EPIPHANY - 8 bit signed pc-relative displacement  */
        BFD_RELOC_EPIPHANY_SIMM8,

        /* Adapteva EPIPHANY - 24 bit signed pc-relative displacement  */
        BFD_RELOC_EPIPHANY_SIMM24,

        /* Adapteva EPIPHANY - 16 most-significant bits of absolute address  */
        BFD_RELOC_EPIPHANY_HIGH,

        /* Adapteva EPIPHANY - 16 least-significant bits of absolute address  */
        BFD_RELOC_EPIPHANY_LOW,

        /* Adapteva EPIPHANY - 11 bit signed number - add/sub immediate  */
        BFD_RELOC_EPIPHANY_SIMM11,

        /* Adapteva EPIPHANY - 11 bit sign-magnitude number (ld/st displacement)  */
        BFD_RELOC_EPIPHANY_IMM11,

        /* Adapteva EPIPHANY - 8 bit immediate for 16 bit mov instruction.  */
        BFD_RELOC_EPIPHANY_IMM8,
        BFD_RELOC_UNUSED ,

        /* #defines */
        BFD_RELOC_SPARC_64 = BFD_RELOC_64,
        BFD_RELOC_SPARC_DISP64 = BFD_RELOC_64_PCREL
    }
}
