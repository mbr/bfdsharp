﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BfdSharp.Backend
{
    public class BfdRelocHowto
        : BackendObject<bfd_reloc_howto_struct>
    {
        public string Name
        {
            get
            {
                return Marshal.PtrToStringAnsi(Backend.name);
            }
        }


        public override string ToString()
        {
            return Name;
        }


        public BfdRelocHowto(BfdBackend owner, IntPtr p)
            : base(owner, p)
        {

        }
    }
}
