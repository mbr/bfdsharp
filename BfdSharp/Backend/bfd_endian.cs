﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BfdSharp.Backend
{
    public enum bfd_endian 
    { 
        BFD_ENDIAN_BIG,
        BFD_ENDIAN_LITTLE, 
        BFD_ENDIAN_UNKNOWN 
    };
}
