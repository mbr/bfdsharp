﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BfdSharp.Backend
{
    public enum bfd_error
    {
        bfd_error_no_error = 0,
        bfd_error_system_call,
        bfd_error_invalid_target,
        bfd_error_wrong_format,
        bfd_error_wrong_object_format,
        bfd_error_invalid_operation,
        bfd_error_no_memory,
        bfd_error_no_symbols,
        bfd_error_no_armap,
        bfd_error_no_more_archived_files,
        bfd_error_malformed_archive,
        bfd_error_missing_dso,
        bfd_error_file_not_recognized,
        bfd_error_file_ambiguously_recognized,
        bfd_error_no_contents,
        bfd_error_nonrepresentable_section,
        bfd_error_no_debug_section,
        bfd_error_bad_value,
        bfd_error_file_truncated,
        bfd_error_file_too_big,
        bfd_error_on_input,
        bfd_error_invalid_error_code
    };
}
