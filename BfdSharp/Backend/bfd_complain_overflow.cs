﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BfdSharp.Backend
{
    public enum bfd_complain_overflow
    {
        /// <summary>
        /// Do not complain on overflow. 
        /// </summary>
        complain_overflow_dont,

        /// <summary>
        /// Complain if the value overflows when considered as a signed
        /// number one bit larger than the field.  ie. A bitfield of N bits
        /// is allowed to represent -2**n to 2**n-1. 
        /// </summary>
        complain_overflow_bitfield,

        /// <summary>
        /// Complain if the value overflows when considered as a signed
        /// number.
        /// </summary>
        complain_overflow_signed,

        /// <summary>
        /// Complain if the value overflows when considered as an
        /// unsigned number
        /// </summary>
        complain_overflow_unsigned
    };
}
