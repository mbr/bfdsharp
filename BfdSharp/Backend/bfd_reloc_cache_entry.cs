﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BfdSharp.Backend
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 8)]
    public struct bfd_reloc_cache_entry
    {
        /// <summary>
        /// A pointer into the canonical table of pointers.
        /// struct bfd_symbol **sym_ptr_ptr
        /// </summary>
        public IntPtr sym_ptr_ptr;

        /// <summary>
        /// Offset in section.
        /// </summary>
        public UInt64 address;

        /// <summary>
        /// Addend for relocation value.
        /// </summary>
        public UInt64 addend;

        /// <summary>
        /// Pointer to how to perform the required relocation.
        /// struct reloc_howto_struct *howto
        /// </summary>
        public IntPtr howto;
    }
}
