﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;


namespace BfdSharp.Backend
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 4)]
    public struct bfd_target
    {

        /* Identifies the kind of target, e.g., SunOS4, Ultrix, etc.  */
        public IntPtr name;

        /* The "flavour" of a back end is a general indication about
        the contents of a file.  */
        public bfd_flavour flavour;

        /* The order of bytes within the data area of a file.  */
        public bfd_endian byteorder;

        /* The order of bytes within the header parts of a file.  */
        public bfd_endian header_byteorder;

        /* A mask of all the flags which an executable may have set -
            from the set <<BFD_NO_FLAGS>>, <<HAS_RELOC>>, ...<<D_PAGED>>.  */
        public bfd_flags object_flags;

        /* A mask of all the flags which a section may have set - from
        the set <<SEC_NO_FLAGS>>, <<SEC_ALLOC>>, ...<<SET_NEVER_LOAD>>.  */
        public bfd_sec_flags section_flags;


        /* The character normally found at the front of a symbol.
        (if any), perhaps `_'.  */
        public byte symbol_leading_char;

        /* The pad character for file names within an archive header.  */
        public byte ar_pad_char;

        /* The maximum number of characters in an archive header.  */
        public byte ar_max_namelen;

        /* How well this target matches, used to select between various
            possible targets when more than one target matches.  */
        public byte match_priority;

        /*
        bfd_uint64_t    (*bfd_getx64) (const void *);
        bfd_int64_t     (*bfd_getx_signed_64) (const void *);
        void            (*bfd_putx64) (bfd_uint64_t, void *);
            * 
        bfd_vma         (*bfd_getx32) (const void *);
        bfd_signed_vma  (*bfd_getx_signed_32) (const void *);
        void            (*bfd_putx32) (bfd_vma, void *);
            * 
        bfd_vma         (*bfd_getx16) (const void *);
        bfd_signed_vma  (*bfd_getx_signed_16) (const void *);
        void            (*bfd_putx16) (bfd_vma, void *);
        */

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate UInt64 BfdGetx64Delegate(IntPtr data);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate Int64 BfdGetxSigned64Delegate(IntPtr data);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void BfdPutx64Delegate(UInt64 w, IntPtr data);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate UInt32 BfdGetx32Delegate(IntPtr data);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate Int32 BfdGetxSigned32Delegate(IntPtr data);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void BfdPutx32Delegate(UInt32 w, IntPtr data);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate UInt32 BfdGetx16Delegate(IntPtr data);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate Int32 BfdGetxSigned16Delegate(IntPtr data);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void BfdPutx16Delegate(UInt32 w, IntPtr data);

        public BfdGetx64Delegate bfd_getx64;
        public BfdGetxSigned64Delegate bfd_getx_signed_64;
        public BfdPutx64Delegate bfd_putx64;

        public BfdGetx32Delegate bfd_getx32;
        public BfdGetxSigned32Delegate bfd_getx_signed_32;
        public BfdPutx32Delegate bfd_putx32;

        public BfdGetx16Delegate bfd_getx16;
        public BfdGetxSigned16Delegate bfd_getx_signed_16;
        public BfdPutx16Delegate bfd_putx16;

        /*
        bfd_uint64_t (*bfd_h_getx64) (const void *);
        bfd_int64_t (*bfd_h_getx_signed_64) (const void *);
        void (*bfd_h_putx64) (bfd_uint64_t, void *);
            * 
        bfd_vma (*bfd_h_getx32) (const void *);
        bfd_signed_vma (*bfd_h_getx_signed_32) (const void *);
        void (*bfd_h_putx32) (bfd_vma, void *);
            * 
        bfd_vma (*bfd_h_getx16) (const void *);
        bfd_signed_vma (*bfd_h_getx_signed_16) (const void *);
        void (*bfd_h_putx16) (bfd_vma, void *);
            * 
            * Reusing earlier delegates.
            * */

        public BfdGetx64Delegate bfd_h_getx64;
        public BfdGetxSigned64Delegate bfd_h_getx_signed_64;
        public BfdPutx64Delegate bfd_h_putx64;

        public BfdGetx32Delegate bfd_h_getx32;
        public BfdGetxSigned32Delegate bfd_h_getx_signed_32;
        public BfdPutx32Delegate bfd_h_putx32;

        public BfdGetx16Delegate bfd_h_getx16;
        public BfdGetxSigned16Delegate bfd_h_getx_signed_16;
        public BfdPutx16Delegate bfd_h_putx16;



        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate IntPtr BfdPtrRetPtrArgDelegate(IntPtr data);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate bool BfdBoolRetPtrArgDelegate(IntPtr data);

        /*const struct bfd_target *(*_bfd_check_format[bfd_type_end]) (bfd *);*/
        //public fixed IntPtr _bfd_check_format[(int)bfd_format.bfd_type_end];
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst=((int)bfd_format.bfd_type_end*4))]
        public BfdPtrRetPtrArgDelegate _bfd_check_format_unknown;
        public BfdPtrRetPtrArgDelegate _bfd_check_format_object;
        public BfdPtrRetPtrArgDelegate _bfd_check_format_archive;
        public BfdPtrRetPtrArgDelegate _bfd_check_format_core;


        /*bfd_boolean (*_bfd_set_format[bfd_type_end]) (bfd *);*/
        //public fixed BfdBoolRetPtrArgDelegate _bfd_set_format[(int)bfd_format.bfd_type_end];
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = (int)bfd_format.bfd_type_end)]
        //public BfdBoolRetPtrArgDelegate[] _bfd_set_format;
        public BfdBoolRetPtrArgDelegate _bfd_set_format_unknown;
        public BfdBoolRetPtrArgDelegate _bfd_set_format_object;
        public BfdBoolRetPtrArgDelegate _bfd_set_format_archive;
        public BfdBoolRetPtrArgDelegate _bfd_set_format_core;


        /*bfd_boolean (*_bfd_write_contents[bfd_type_end]) (bfd *);*/
        //public fixed BfdBoolRetPtrArgDelegate _bfd_write_contents[(int)bfd_format.bfd_type_end];
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = (int)bfd_format.bfd_type_end)]
        //public BfdBoolRetPtrArgDelegate[] _bfd_write_contents;
        public BfdBoolRetPtrArgDelegate _bfd_write_contents_unknown;
        public BfdBoolRetPtrArgDelegate _bfd_write_contents_object;
        public BfdBoolRetPtrArgDelegate _bfd_write_contents_archive;
        public BfdBoolRetPtrArgDelegate _bfd_write_contents_core;

        /*
        bfd_boolean (*_close_and_cleanup) (bfd *);

        bfd_boolean (*_bfd_free_cached_info) (bfd *);*/
        public BfdBoolRetPtrArgDelegate _close_and_cleanup;
        public BfdBoolRetPtrArgDelegate _bfd_free_cached_info;

        //bfd_boolean (*_new_section_hook) (bfd *, sec_ptr);
        public IntPtr _new_section_hook;

        //bfd_boolean (*_bfd_get_section_contents) (bfd *, sec_ptr, void *, file_ptr, bfd_size_type);
        public IntPtr _bfd_get_section_contents;


        //bfd_boolean (*_bfd_get_section_contents_in_window)  (bfd *, sec_ptr, bfd_window *, file_ptr, bfd_size_type);
        public IntPtr _bfd_get_section_contents_in_window;

        //bfd_boolean (*_bfd_copy_private_bfd_data) (bfd *, bfd *);
        public IntPtr _bfd_copy_private_bfd_data;


        //bfd_boolean (*_bfd_merge_private_bfd_data) (bfd *, bfd *);
        public IntPtr _bfd_merge_private_bfd_data;

        //bfd_boolean (*_bfd_init_private_section_data) (bfd *, sec_ptr, bfd *, sec_ptr, struct bfd_link_info *);
        public IntPtr _bfd_init_private_section_data;


        //bfd_boolean (*_bfd_copy_private_section_data) (bfd *, sec_ptr, bfd *, sec_ptr);
        public IntPtr _bfd_copy_private_section_data;

        //bfd_boolean (*_bfd_copy_private_symbol_data)  (bfd *, asymbol *, bfd *, asymbol *);
        public IntPtr _bfd_copy_private_symbol_data;

        //bfd_boolean (*_bfd_copy_private_header_data)  (bfd *, bfd *);
        public IntPtr _bfd_copy_private_header_data;

        //bfd_boolean (*_bfd_set_private_flags) (bfd *, flagword);
        public IntPtr _bfd_set_private_flags;

        //bfd_boolean (*_bfd_print_private_bfd_data) (bfd *, void *);
        public IntPtr _bfd_print_private_bfd_data;

        //char * (*_core_file_failing_command) (bfd *);
        public IntPtr _core_file_failing_command;

        //int (*_core_file_failing_signal) (bfd *);
        public IntPtr _core_file_failing_signal;

        //bfd_boolean (*_core_file_matches_executable_p) (bfd *, bfd *);
        public IntPtr _core_file_matches_executable_p;

        //int (*_core_file_pid) (bfd *);
        public IntPtr _core_file_pid;

        //bfd_boolean (*_bfd_slurp_armap) (bfd *);
        public IntPtr _bfd_slurp_armap;

        //bfd_boolean (*_bfd_slurp_extended_name_table) (bfd *);
        public IntPtr _bfd_slurp_extended_name_table;

        //bfd_boolean (*_bfd_construct_extended_name_table)   (bfd *, char **, bfd_size_type *, const char **);
        public IntPtr _bfd_construct_extended_name_table;

        //void (*_bfd_truncate_arname) (bfd *, const char *, char *);
        public IntPtr _bfd_truncate_arname;

        //bfd_boolean (*write_armap)   (bfd *, unsigned int, struct orl *, unsigned int, int);
        public IntPtr write_armap;

        //void * (*_bfd_read_ar_hdr_fn) (bfd *);
        public IntPtr _bfd_read_ar_hdr_fn;

        //bfd_boolean (*_bfd_write_ar_hdr_fn) (bfd *, bfd *);
        public IntPtr _bfd_write_ar_hdr_fn;

        //bfd * (*openr_next_archived_file) (bfd *, bfd *);
        public IntPtr openr_next_archived_file;

        //bfd * (*_bfd_get_elt_at_index) (bfd *, symindex);
        public IntPtr _bfd_get_elt_at_index;

        //int (*_bfd_stat_arch_elt) (bfd *, struct stat *);
        public IntPtr _bfd_stat_arch_elt;

        //bfd_boolean (*_bfd_update_armap_timestamp) (bfd *);
        public IntPtr _bfd_update_armap_timestamp;

        //long (*_bfd_get_symtab_upper_bound) (bfd *);
        //public IntPtr _bfd_get_symtab_upper_bound;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate Int32 BfdLongRetPtrArg(IntPtr arg);
        public BfdLongRetPtrArg _bfd_get_symtab_upper_bound;

        //long (*_bfd_canonicalize_symtab)  (bfd *, struct bfd_symbol **);
        //public IntPtr _bfd_canonicalize_symtab;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate Int32 BfdLongRetPtrByteArg(IntPtr arg, byte[] d);
        public BfdLongRetPtrByteArg _bfd_canonicalize_symtab;

        //struct bfd_symbol * (*_bfd_make_empty_symbol) (bfd *);
        public IntPtr _bfd_make_empty_symbol;

        //void (*_bfd_print_symbol) (bfd *, void *, struct bfd_symbol *, bfd_print_symbol_type);
        public IntPtr _bfd_print_symbol;

        //void (*_bfd_get_symbol_info) (bfd *, struct bfd_symbol *, symbol_info *);
        public IntPtr _bfd_get_symbol_info;

        //bfd_boolean (*_bfd_is_local_label_name) (bfd *, const char *);
        public IntPtr _bfd_is_local_label_name;

        //bfd_boolean (*_bfd_is_target_special_symbol) (bfd *, asymbol *);
        public IntPtr _bfd_is_target_special_symbol;

        //alent * (*_get_lineno) (bfd *, struct bfd_symbol *);
        public IntPtr _get_lineno;

        //bfd_boolean (*_bfd_find_nearest_line)(bfd *, struct bfd_section *, struct bfd_symbol **, bfd_vma, const char **, const char **, unsigned int *);
        public IntPtr _bfd_find_nearest_line;

        //bfd_boolean (*_bfd_find_nearest_line_discriminator)(bfd *, struct bfd_section *, struct bfd_symbol **, bfd_vma, const char **, const char **, unsigned int *, unsigned int *);
        public IntPtr _bfd_find_nearest_line_discriminator;

        //bfd_boolean (*_bfd_find_line)      (bfd *, struct bfd_symbol **, struct bfd_symbol *, const char **, unsigned int *);
        public IntPtr _bfd_find_line;

        //bfd_boolean (*_bfd_find_inliner_info)  (bfd *, const char **, const char **, unsigned int *);
        public IntPtr _bfd_find_inliner_info;



        //asymbol * (*_bfd_make_debug_symbol) (bfd *, void *, unsigned long size);
        public IntPtr _bfd_make_debug_symbol;

        //long (*_read_minisymbols) (bfd *, bfd_boolean, void **, unsigned int *);
        public IntPtr _read_minisymbols;


        //asymbol * (*_minisymbol_to_symbol)(bfd *, bfd_boolean, const void *, asymbol *);
        public IntPtr _minisymbol_to_symbol;

        //long (*_get_reloc_upper_bound) (bfd *, sec_ptr);
        public IntPtr _get_reloc_upper_bound;

        //long (*_bfd_canonicalize_reloc)  (bfd *, sec_ptr, arelent **, struct bfd_symbol **);
        public IntPtr _bfd_canonicalize_reloc;

        //reloc_howto_type *  (*reloc_type_lookup) (bfd *, bfd_reloc_code_real_type);
        public IntPtr reloc_type_lookup;

        //reloc_howto_type *  (*reloc_name_lookup) (bfd *, const char *);
        public IntPtr reloc_name_lookup;







        //bfd_boolean (*_bfd_set_arch_mach) (bfd *, enum bfd_architecture, unsigned long);
        public IntPtr _bfd_set_arch_mach;

        //bfd_boolean (*_bfd_set_section_contents) (bfd *, sec_ptr, const void *, file_ptr, bfd_size_type);
        public IntPtr _bfd_set_section_contents;

        //int (*_bfd_sizeof_headers) (bfd *, struct bfd_link_info *);
        public IntPtr _bfd_sizeof_headers;

        //bfd_byte * (*_bfd_get_relocated_section_contents) (bfd *, struct bfd_link_info *, struct bfd_link_order *, bfd_byte *, bfd_boolean, struct bfd_symbol **);
        public IntPtr _bfd_get_relocated_section_contents;

        //bfd_boolean (*_bfd_relax_section) (bfd *, struct bfd_section *, struct bfd_link_info *, bfd_boolean *);
        public IntPtr _bfd_relax_section;



        //struct bfd_link_hash_table * (*_bfd_link_hash_table_create) (bfd *);
        public IntPtr _bfd_link_hash_table_create;


        //void (*_bfd_link_hash_table_free) (struct bfd_link_hash_table *);
        public IntPtr _bfd_link_hash_table_free;


        //bfd_boolean (*_bfd_link_add_symbols) (bfd *, struct bfd_link_info *);
        public IntPtr _bfd_link_add_symbols;


        //void (*_bfd_link_just_syms) (asection *, struct bfd_link_info *);
        public IntPtr _bfd_link_just_syms;




        //void (*_bfd_copy_link_hash_symbol_type) (bfd *, struct bfd_link_hash_entry *, struct bfd_link_hash_entry *);
        public IntPtr _bfd_copy_link_hash_symbol_type;



        //bfd_boolean (*_bfd_final_link) (bfd *, struct bfd_link_info *);
        public IntPtr _bfd_final_link;


        //bfd_boolean (*_bfd_link_split_section) (bfd *, struct bfd_section *);
        public IntPtr _bfd_link_split_section;


        //bfd_boolean (*_bfd_gc_sections) (bfd *, struct bfd_link_info *);
        public IntPtr _bfd_gc_sections;


        //bfd_boolean (*_bfd_lookup_section_flags) (struct bfd_link_info *, struct flag_info *,  asection *);
        public IntPtr _bfd_lookup_section_flags;


        //bfd_boolean (*_bfd_merge_sections) (bfd *, struct bfd_link_info *);
        public IntPtr _bfd_merge_sections;


        //bfd_boolean (*_bfd_is_group_section) (bfd *, const struct bfd_section *);
        public IntPtr _bfd_is_group_section;


        //bfd_boolean (*_bfd_discard_group) (bfd *, struct bfd_section *);
        public IntPtr _bfd_discard_group;



        //bfd_boolean (*_section_already_linked) (bfd *, asection *, struct bfd_link_info *);
        public IntPtr _section_already_linked;


        //bfd_boolean (*_bfd_define_common_symbol) (bfd *, struct bfd_link_info *,  struct bfd_link_hash_entry *);
        public IntPtr _bfd_define_common_symbol;

        //long (*_bfd_get_dynamic_symtab_upper_bound) (bfd *);
        public IntPtr _bfd_get_dynamic_symtab_upper_bound;

        //long (*_bfd_canonicalize_dynamic_symtab)  (bfd *, struct bfd_symbol **);
        public IntPtr _bfd_canonicalize_dynamic_symtab;

        //long (*_bfd_get_synthetic_symtab) (bfd *, long, struct bfd_symbol **, long, struct bfd_symbol **, struct bfd_symbol **);
        public IntPtr _bfd_get_synthetic_symtab;

        //long (*_bfd_get_dynamic_reloc_upper_bound) (bfd *);
        public IntPtr _bfd_get_dynamic_reloc_upper_bound;

        //long (*_bfd_canonicalize_dynamic_reloc) (bfd *, arelent **, struct bfd_symbol **);
        public IntPtr _bfd_canonicalize_dynamic_reloc;


        //const struct bfd_target * alternative_target;
        public IntPtr alternative_target;



        //const void *backend_data;
        public IntPtr backend_data;


        public override string ToString()
        {
            return Marshal.PtrToStringAnsi(name);
        }
    }
}
