﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BfdSharp.Backend
{
    public class BfdSymbol
        : BackendObject<bfd_symbol>
    {
        #region Properties

        /// <summary>
        /// A pointer to the BFD which owns the symbol.
        /// </summary>
        public BfdBackend TheBfd
        {
            get
            {
                return new BfdBackend(null, _backend_object.the_bfd);
            }
        }

        /// <summary>
        /// The text of the symbol. The name is left alone, and not copied;
        /// the application may not alter it.
        /// </summary>
        public string Name
        {
            get
            {
                return Marshal.PtrToStringAnsi(
                    _backend_object.name
                );
            }
        }

        /// <summary>
        /// The value of the symbol.
        /// </summary>
        public UInt64 Value
        {
            get
            {
                return _backend_object.Value;
            }
        }

        /// <summary>
        /// Flags
        /// </summary>
        public bfd_symbol_flags Flags
        {
            get
            {
                return _backend_object.Flags;
            }
        }

        /// <summary>
        /// A pointer to the section to which this symbol is relative. This will always be non
        /// NULL, there are special sections for undefined and absolute symbols.
        /// </summary>
        public BfdSection Section
        {
            get
            {
                return new BfdSection(
                    Owner,
                    _backend_object.section
                );
            }
        }

        #endregion


        #region Overrides

        public override string ToString()
        {
            return String.Format("0x{0:X8} {1}", Value, Name);
        }

        #endregion



        public BfdSymbol(BfdBackend owner, IntPtr p)
            : base(owner, p)
        {

        }
    }
}
