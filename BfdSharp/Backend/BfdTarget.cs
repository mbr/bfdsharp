﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BfdSharp.Backend
{
    public class BfdTarget
        : BackendObject<bfd_target>
    {
        public string Name
        {
            get
            {
                return Marshal.PtrToStringAnsi(Backend.name);
            }
        }


        public override string ToString()
        {
            return Name;
        }


        public BfdTarget ( BfdBackend owner, IntPtr p )
            : base(owner, p)
        {

        }
    }
}
