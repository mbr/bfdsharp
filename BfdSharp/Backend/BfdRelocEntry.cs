﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BfdSharp.Backend
{
    public class BfdRelocEntry
        : BackendObject<bfd_reloc_cache_entry>
    {
        public BfdRelocHowto Howto
        {
            get
            {
                return new BfdRelocHowto(
                    this.Owner, Backend.howto
                );
            }
        }

        public BfdSymbol Symbol
        {
            get
            {
                // Pointer to pointer
                var ptr = Marshal.ReadIntPtr(Backend.sym_ptr_ptr);

                return new BfdSymbol(
                    this.Owner, ptr
                );
            }
        }

        /// <summary>
        /// Offset in section.
        /// </summary>
        public UInt64 Address
        {
            get
            {
                return Backend.address;
            }
        }

        /// <summary>
        /// Addend for relocation value.
        /// </summary>
        public UInt64 Addend
        {
            get
            {
                return Backend.addend;
            }
        }


        public override string ToString()
        {
            return String.Format("0x{0:X8} {1}", Address, Howto.Name);
        }


        public BfdRelocEntry(BfdBackend owner, IntPtr p)
            : base(owner, p)
        {

        }
    }
}
