﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BfdSharp.Backend
{
    public enum bfd_direction
    {
        no_direction = 0,
        read_direction = 1,
        write_direction = 2,
        both_direction = 3
    };
}
