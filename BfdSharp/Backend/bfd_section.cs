﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;


namespace BfdSharp.Backend
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 4)]
    public struct bfd_section
    {
        public IntPtr name;
        public int id;
        public int index;

        public IntPtr next;
        public IntPtr prev;

        public bfd_sec_flags flags;

        public UInt16 PackedBoolFields;

        Int32 padding;

        public UInt64 vma;
        public UInt64 lma;
        public UInt64 size;
        public UInt64 rawsize;
        public UInt64 compressed_size;

        public IntPtr relax;
        public int relax_count;

        public UInt64 output_offset;
        public IntPtr output_section;

        public UInt32 alignment_power;

        public IntPtr relocation;
        public IntPtr orelocation;

        public UInt32 reloc_count;

        public UInt64 filepos;
        public UInt64 rel_filepos;
        public UInt64 line_filepos;

        public IntPtr userdata;

        public IntPtr contents;

        public IntPtr lineno;
        public UInt32 lineno_count;

        public UInt32 entsize;

        public IntPtr kept_section;

        public UInt64 moving_line_filepos;

        public UInt32 target_index;
        public IntPtr used_by_bfd;

        public IntPtr constructor_chain;
        public IntPtr owner;

        public IntPtr symbol;
        public IntPtr symbol_ptr_tr;

        public IntPtr map_link_order;
        public IntPtr map_s;


        public override string ToString()
        {
            return String.Format("{0} (0x{1:X8} - 0x{2:X8})", Marshal.PtrToStringAnsi(name), vma, vma + size);
        }
    }
}
