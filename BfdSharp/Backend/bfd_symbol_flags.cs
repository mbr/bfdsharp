﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BfdSharp.Backend
{
    [Flags]
    public enum bfd_symbol_flags
    {
        BSF_LOCAL = (1 << 0),

        /* The symbol has global scope; initialized data in <<C>>. The
           value is the offset into the section of the data.  */
        BSF_GLOBAL = (1 << 1),

        /* The symbol has global scope and is exported. The value is
           the offset into the section of the data.  */
        BSF_EXPORT = BSF_GLOBAL, /* No real difference.  */

        /* A normal C symbol would be one of:
           <<BSF_LOCAL>>, <<BSF_COMMON>>,  <<BSF_UNDEFINED>> or
           <<BSF_GLOBAL>>.  */

        /* The symbol is a debugging record. The value has an arbitrary
           meaning, unless BSF_DEBUGGING_RELOC is also set.  */
        BSF_DEBUGGING = (1 << 2),

        /* The symbol denotes a function entry point.  Used in ELF,
           perhaps others someday.  */
        BSF_FUNCTION = (1 << 3),

        /* Used by the linker.  */
        BSF_KEEP = (1 << 5),
        BSF_KEEP_G = (1 << 6),

        /* A weak global symbol, overridable without warnings by
           a regular global symbol of the same name.  */
        BSF_WEAK = (1 << 7),

        /* This symbol was created to point to a section, e.g. ELF's
           STT_SECTION symbols.  */
        BSF_SECTION_SYM = (1 << 8),

        /* The symbol used to be a common symbol, but now it is
           allocated.  */
        BSF_OLD_COMMON = (1 << 9),

        /* In some files the type of a symbol sometimes alters its
           location in an output file - ie in coff a <<ISFCN>> symbol
           which is also <<C_EXT>> symbol appears where it was
           declared and not at the end of a section.  This bit is set
           by the target BFD part to convey this information.  */
        BSF_NOT_AT_END = (1 << 10),

        /* Signal that the symbol is the label of constructor section.  */
        BSF_CONSTRUCTOR = (1 << 11),

        /* Signal that the symbol is a warning symbol.  The name is a
           warning.  The name of the next symbol is the one to warn about;
           if a reference is made to a symbol with the same name as the next
           symbol, a warning is issued by the linker.  */
        BSF_WARNING = (1 << 12),

        /* Signal that the symbol is indirect.  This symbol is an indirect
           pointer to the symbol with the same name as the next symbol.  */
        BSF_INDIRECT = (1 << 13),

        /* BSF_FILE marks symbols that contain a file name.  This is used
           for ELF STT_FILE symbols.  */
        BSF_FILE = (1 << 14),

        /* Symbol is from dynamic linking information.  */
        BSF_DYNAMIC = (1 << 15),

        /* The symbol denotes a data object.  Used in ELF, and perhaps
           others someday.  */
        BSF_OBJECT = (1 << 16),

        /* This symbol is a debugging symbol.  The value is the offset
           into the section of the data.  BSF_DEBUGGING should be set
           as well.  */
        BSF_DEBUGGING_RELOC = (1 << 17),

        /* This symbol is thread local.  Used in ELF.  */
        BSF_THREAD_LOCAL = (1 << 18),

        /* This symbol represents a complex relocation expression,
           with the expression tree serialized in the symbol name.  */
        BSF_RELC = (1 << 19),

        /* This symbol represents a signed complex relocation expression,
           with the expression tree serialized in the symbol name.  */
        BSF_SRELC = (1 << 20),

        /* This symbol was created by bfd_get_synthetic_symtab.  */
        BSF_SYNTHETIC = (1 << 21),

        /* This symbol is an indirect code object.  Unrelated to BSF_INDIRECT.
           The dynamic linker will compute the value of this symbol by
           calling the function that it points to.  BSF_FUNCTION must
           also be also set.  */
        BSF_GNU_INDIRECT_FUNCTION = (1 << 22),
        /* This symbol is a globally unique data object.  The dynamic linker
           will make sure that in the entire process there is just one symbol
           with this name and type in use.  BSF_OBJECT must also be set.  */
        BSF_GNU_UNIQUE = (1 << 23)
    }
}
