﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BfdSharp.Backend
{
    public class BfdException
        : ApplicationException
    {
        public BfdException()
            : base(
                Marshal.PtrToStringAnsi(
                    BfdApi.bfd_errmsg(BfdApi.bfd_get_error())
                )
            )
        {

        }
    }
}
