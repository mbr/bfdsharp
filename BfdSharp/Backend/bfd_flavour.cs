﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BfdSharp.Backend
{
    public enum bfd_flavour
    {
        bfd_target_unknown_flavour,
        bfd_target_aout_flavour,
        bfd_target_coff_flavour,
        bfd_target_ecoff_flavour,
        bfd_target_xcoff_flavour,
        bfd_target_elf_flavour,
        bfd_target_ieee_flavour,
        bfd_target_nlm_flavour,
        bfd_target_oasys_flavour,
        bfd_target_tekhex_flavour,
        bfd_target_srec_flavour,
        bfd_target_verilog_flavour,
        bfd_target_ihex_flavour,
        bfd_target_som_flavour,
        bfd_target_os9k_flavour,
        bfd_target_versados_flavour,
        bfd_target_msdos_flavour,
        bfd_target_ovax_flavour,
        bfd_target_evax_flavour,
        bfd_target_mmo_flavour,
        bfd_target_mach_o_flavour,
        bfd_target_pef_flavour,
        bfd_target_pef_xlib_flavour,
        bfd_target_sym_flavour
    }
}
