﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BfdSharp.Backend
{
    public class BfdBackend
        : BackendObject<bfd>
    {
        private bool _autoclose;
        private IntPtr _fn;


        #region Static methods

        /// <summary>
        /// Calls bfd_openr() on the supplied back, and creates a new BfdBackend object supporting
        /// the returned context.
        /// </summary>
        /// <param name="path">Path to the archive/object file.</param>
        /// <returns></returns>
        public static BfdBackend OpenPath ( string path, bool autoclose = false )
        {
			BfdApi.InitializeBfd ();

            var dup = BfdApi.StrDup(path);
            var ctx = BfdApi.bfd_openr(dup, null);

            if (ctx == (IntPtr)0)
                throw new BfdException();

            var ret = new BfdBackend(null, ctx);
            ret._autoclose = autoclose;
            ret._fn = dup;

            return ret;
        }

        #endregion


        #region Enumerable properties

        public IEnumerable<BfdSection> Sections
        {
            get
            {
                var sec = new BfdSection(this, this._backend_object.sections);

                do
                {
                    yield return sec;

                    if (sec._backend_object.next == (IntPtr)0)
                        break;

                    sec = new BfdSection(this, sec._backend_object.next);
                }
                while (true);
            }
        }


        public IEnumerable<BfdSymbol> Symbols
        {
            get
            {
                if (_symbol_table == null)
                    LoadSymbolTable();

                foreach (var sym in this._symbol_table)
                    yield return sym;
            }
        }


        public IEnumerable<BfdBackend> ArchiveContents
        {
            get
            {
                if (!IsArchive)
                    throw new ApplicationException("Can only enumerate contents of archives.");

                IntPtr obj, prev = (IntPtr)0;

                do
                {
                    obj = BfdApi.bfd_openr_next_archived_file(this.Ptr, prev);

                    if (obj == (IntPtr)0)
                        break;

                    if (BfdApi.bfd_check_format(obj, bfd_format.bfd_object))
                        yield return new BfdBackend(this, obj);

                    prev = obj;
                }
                while (true);
            }
        }

        #endregion


        #region Various properties

        public BfdTarget Target
        {
            get
            {
                return new BfdTarget(this, _backend_object.xvec);
            }
        }

        public bool IsArchive
        {
            get
            {
                return BfdApi.bfd_check_format(this.Ptr, bfd_format.bfd_archive);
            }
        }

        public string Filename
        {
            get
            {
                return Marshal.PtrToStringAnsi(
                    Backend.filename
                );
            }
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return Filename;
        }

        #endregion


        #region Symbol table

		internal PtrTable<BfdSymbol> _symbol_table = null;


		public PtrTable<BfdSymbol> SymbolTable
		{
			get
			{
				if (_symbol_table == null)
					LoadSymbolTable();

				return _symbol_table;
			}
		}



        private void LoadSymbolTable()
        {
            var storageNeeded = Target.Backend._bfd_get_symtab_upper_bound(
                this.Ptr
            );
            var datum = new byte[storageNeeded];
            var numSym = Target.Backend._bfd_canonicalize_symtab(this.Ptr, datum);

			_symbol_table = new PtrTable<BfdSymbol>(
				datum, 
				numSym, 
				(ptr) => { 
					return new BfdSymbol(this, ptr); 
				}
			);
        }

        #endregion



        public BfdBackend(BfdBackend owner, IntPtr p)
            : base(owner, p)
        {
            
        }


        ~BfdBackend()
        {
            if( _autoclose )
            {
                if (!BfdApi.bfd_close(_backend_ptr))
                    throw new BfdException();

                BfdApi.LocalFree(_fn);
            }
        }
    }
}
