﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BfdSharp.Backend
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 8)]
    public struct bfd_symbol
    {

        /* A pointer to the BFD which owns the symbol. This information
           is necessary so that a back end can work out what additional
           information (invisible to the application writer) is carried
           with the symbol.

           This field is *almost* redundant, since you can use section->owner
           instead, except that some symbols point to the global sections
           bfd_{abs,com,und}_section.  This could be fixed by making
           these globals be per-bfd (or per-target-flavor).  FIXME.  */
        //struct bfd *the_bfd; /* Use bfd_asymbol_bfd(sym) to access this field.  */
        public IntPtr the_bfd;

        /* The text of the symbol. The name is left alone, and not copied; the
           application may not alter it.  */
        //const char *name;
        public IntPtr name;



        /* The value of the symbol.  This really should be a union of a
           numeric value with a pointer, since some flags indicate that
           a pointer to another symbol is stored here.  */
        //symvalue value;
        public UInt64 Value;


        //flagword flags;
        public bfd_symbol_flags Flags;

        /* A pointer to the section to which this symbol is
           relative.  This will always be non NULL, there are special
           sections for undefined and absolute symbols.  */
        //struct bfd_section *section;
        public IntPtr section;

        /* Back end special data.  
        union
          {
            void *p;
            bfd_vma i;
          }
        udata;*/
        UInt64 udata;
    }
}
