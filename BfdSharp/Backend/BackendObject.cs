﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;


namespace BfdSharp.Backend
{
    public class BackendObject<T>
    {
        internal BfdBackend _owner;
        internal IntPtr     _backend_ptr;
        internal T          _backend_object;


        public T Backend
        {
            get
            {
                return _backend_object;
            }
        }

        public IntPtr Ptr
        {
            get
            {
                return _backend_ptr;
            }
        }

        public BfdBackend Owner
        {
            get
            {
                return _owner;
            }
        }


        public override string ToString()
        {
            return String.Format("{0}@0x{1:x8}", typeof(T).Name, Ptr.ToInt32());
        }


        public BackendObject(BfdBackend owner, IntPtr p)
        {
            if (p == (IntPtr)0)
                throw new ArgumentException("Pointer to backend object cannot be null");

            this._owner = owner;

            this._backend_object = (T)Marshal.PtrToStructure(
                p, typeof(T)
            );

            this._backend_ptr = p;
        }
    }
}
