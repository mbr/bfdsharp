﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BfdSharp.Backend
{
    public class BfdApi
    {
        const string BFDLIB = @"assets\libbfd.dll";

		private static bool   initialized = false;
		private static object initLock    = new object ();


		/// <summary>
		/// Marshals the call to `bfd_init`, which must only be done once.
		/// </summary>
		public static void InitializeBfd ( )
		{
			lock (initLock) {

				if (!initialized)
					bfd_init ();
				else
					return;

				initialized = true;
			}
		}



		/// <summary>
		/// This routine must be called before any other BFD function to initialize magical internal data structures.
		/// </summary>
		[DllImport(BFDLIB, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
		public static extern void bfd_init();

        [DllImport(BFDLIB, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr bfd_openr( IntPtr filename, string target);

        [DllImport(BFDLIB, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool bfd_close(IntPtr handle);

        [DllImport(BFDLIB, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool bfd_check_format(IntPtr bfd, bfd_format format);

        [DllImport(BFDLIB, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern bfd_error bfd_get_error();

        [DllImport(BFDLIB, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void bfd_set_error(bfd_error error, __arglist);

        [DllImport(BFDLIB, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr bfd_errmsg(bfd_error error);

        [DllImport(BFDLIB, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr bfd_openr_next_archived_file(IntPtr archive, IntPtr prev);

        [DllImport(BFDLIB, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void bfd_perror(string message);

        [DllImport(BFDLIB, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool bfd_get_section_contents ( IntPtr abfd, IntPtr section, byte[] data, UInt64 offset, UInt64 count );


        /// <summary>
        /// Return the number of bytes required to store the relocation information associated with section sect 
        /// attached to bfd abfd. If an error occurs, return -1.
        /// </summary>
        /// <param name="abfd">bfd *abfd</param>
        /// <param name="sect">asection *sect</param>
        [DllImport(BFDLIB, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 bfd_get_reloc_upper_bound(IntPtr abfd, IntPtr sect);


        /// <summary>
        /// Call the back end associated with the open BFD abfd and translate the external form of the relocation 
        /// information attached to sec into the internal canonical form. Place the table into memory at loc, which has
        /// been preallocated, usually by a call to bfd_get_reloc_upper_bound. Returns the number of relocs, or -1 on 
        /// error.
        /// 
        /// The syms table is also needed for horrible internal magic reasons.
        /// </summary>
        /// <param name="archive">bfd *abfd</param>
        /// <param name="section">asection *sec</param>
        /// <param name="relentries">arelent **loc</param>
        /// <param name="symbols">asymbol **syms</param>
        /// <returns></returns>
        [DllImport(BFDLIB, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 bfd_canonicalize_reloc(IntPtr abfd, IntPtr sec, byte[] loc, byte[] syms);
        //void bfd_perror (const char *message);

        [DllImport("Shlwapi.dll", SetLastError = true)]
        public static extern IntPtr StrDup(string str);

        [DllImport("Kernel32.dll", SetLastError = true)]
        public static extern void LocalFree(IntPtr data);

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr calloc(int num, int size);

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void free(IntPtr p);
    }
}
