﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BfdSharp.Backend
{
    public class BfdSection 
        : BackendObject<bfd_section>
    {

        #region Enumerable properties


        #endregion

        private PtrTable<BfdRelocEntry> _relocation_table = null;

        private void LoadRelocationTable ()
        {
            var storage = BfdApi.bfd_get_reloc_upper_bound(Owner.Ptr, this.Ptr);
            var data = new byte[storage];
            var numrel = BfdApi.bfd_canonicalize_reloc(
                Owner.Ptr, 
                this.Ptr, 
                data,
                Owner.SymbolTable.Raw
            );

            _relocation_table = new PtrTable<BfdRelocEntry>(
                data, 
                numrel,
                (ptr) => {
                    return new BfdRelocEntry(this.Owner, ptr);
                }
            );
        }


        private byte[] _contents;

        public byte[] Contents
        {
            get
            {
                if( _contents == null )
                {
                    _contents = new byte[Backend.size];
                    if (!BfdApi.bfd_get_section_contents(Owner.Ptr, this.Ptr, _contents, 0, (ulong)_contents.Length))
                        throw new BfdException();
                }

                return _contents;
            }
        }



        public IEnumerable<BfdRelocEntry> Relocations
        {
            get
            {
                if (_relocation_table == null)
                    LoadRelocationTable();

                foreach (var rel in _relocation_table)
                    yield return rel;
            }
        }
        

        public string Name
        {
            get
            {
                return Marshal.PtrToStringAnsi(
                    Backend.name
                );
            }
        }


        public override string ToString()
        {
            return Name;
        }

        public BfdSection ( BfdBackend owner, IntPtr p )
            : base(owner, p)
        {

        }
    }
}
