﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BfdSharp.Backend;


namespace BfdGuiTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void LoadFile ( string path )
        {
            var abfd = BfdBackend.OpenPath(path, true);

            var root = treeView1.Nodes.Add(Path.GetFileName(abfd.Filename));

            foreach( var obj in abfd.ArchiveContents )
            {
                var rootObj = root.Nodes.Add(obj.Filename);

                foreach( var sec in obj.Sections )
                {
                    var rootSec = rootObj.Nodes.Add(sec.Name);

                    foreach (var reloc in sec.Relocations)
                    {
                        rootSec.Nodes.Add(
                            String.Format(
                                "0x{0:X8}  {1,-12}  ->  {2}", // Padding to 15 chars because MIPS_RELOC_HI16 is the longest we'll encounter
                                reloc.Address,
                                reloc.Howto.Name,
                                reloc.Symbol.Name
                            )
                        );
                    }
                }
            }
        }


        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fc = new OpenFileDialog();
            fc.Title = "Please choose an archive";
            fc.Multiselect = false;

            switch( fc.ShowDialog() )
            {
                case DialogResult.OK:
                    try
                    {
                        LoadFile(fc.FileName);
                    }
                    catch( Exception error )
                    {
                        MessageBox.Show(
                            error.Message + Environment.NewLine + Environment.NewLine + error.StackTrace,
                            "Error while enumerating archive"
                        );
                    }
                break;
            }

            fc.Dispose();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
